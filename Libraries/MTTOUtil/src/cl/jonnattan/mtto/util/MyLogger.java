package cl.jonnattan.mtto.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class MyLogger extends Formatter
{
  static final String DT_FORMAT = "[HH:mm:ss]";
  final DateFormat    DFMT      = new SimpleDateFormat(DT_FORMAT);
  
  static public void initialize(Level level, String logFilePrefix, String add,
      int port)
  {
    // locate log root
    Logger root = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    while (root.getParent() != null)
      root = root.getParent();
    if (level != null)
      root.setLevel(level);
    
    Handler[] handlers = root.getHandlers();
    for (Handler handler : handlers)
      root.removeHandler(handler);
    
    if (logFilePrefix != null)
    {
      // add file handler
      try
      {
        String fileHandlerPattern = String.format("%s_%%u%%g.log", logFilePrefix);
        Handler fileHandler = new FileHandler(fileHandlerPattern, (5 * 1024 * 1024), 10, true);
        fileHandler.setFormatter(new MyLogger());
        fileHandler.setLevel(level != null ? level : Level.ALL);
        root.addHandler(fileHandler);
      }
      catch (SecurityException | IOException e)
      {
        e.printStackTrace(System.err);
      }
    }
    
    try
    {
      // add single line formatter handler
      Handler handler = new FlushStreamHandler(System.out, new MyLogger());
      handler.setLevel(level != null ? level : Level.ALL);
      root.addHandler(handler);
    }
    catch (SecurityException e)
    {
      e.printStackTrace(System.err);
    }
    
    try
    {
      // add single line formatter handler
      Handler updHandler = new HandlerUDP(add, port);
      updHandler.setFormatter(new MyLogger());
      updHandler.setLevel(level != null ? level : Level.ALL);
      root.addHandler(updHandler);
    }
    catch (SecurityException e)
    {
      e.printStackTrace(System.err);
    }
  }
  
  private static class HandlerUDP extends Handler
  {
    private InetAddress    server  = null;
    private DatagramSocket socket  = null;
    private int            port    = 4515;
    private String         address = "192.168.255.255";
    private DateFormat     DF      = new SimpleDateFormat(DT_FORMAT);
    
    public HandlerUDP(String ip, int port)
    {
      super();
      this.port = port;
      this.address = ip;
      initialize();
    }
    
    private void initialize()
    {
      if( this.port == 0) return;
      try
      {
        this.socket = new DatagramSocket();
        this.server = InetAddress.getByName(address);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
    
    /**
     * Escribe la data en el puerto
     * 
     * @param bArray
     */
    private void writer(final byte[] bArray)
    {
      if( this.port == 0) return;
      DatagramPacket output = new DatagramPacket(bArray, bArray.length, server,
          port);
      try
      {
        socket.send(output);
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }
    
    @Override
    public void flush()
    {
      
    }
    
    @Override
    public void close() throws SecurityException
    {
      
    }
    
    @Override
    public synchronized void publish(LogRecord record)
    {
      StringBuilder sb = new StringBuilder();
      sb.append(DF.format(new Date(record.getMillis())));
      sb.append(getClaseEmisora(record.getSourceClassName()));
      sb.append("[" + record.getLevel().getName() + "] ");
      sb.append(record.getMessage());
      writer(sb.toString().getBytes());
      flush();
    }
  }
  
  /**
   * Utility class to force message flushing (instead all messages appear at the
   * end).
   * 
   * @author ayachan
   */
  static class FlushStreamHandler extends StreamHandler
  {
    public FlushStreamHandler(OutputStream out, Formatter formatter)
    {
      super(out, formatter);
    }
    
    @Override
    public synchronized void publish(LogRecord record)
    {
      super.publish(record);
      flush();
    }
  }
  
  @Override
  public String format(LogRecord record)
  {
    StringBuilder sb = new StringBuilder();
    sb.append(DFMT.format(new Date(record.getMillis())));
    sb.append(getClaseEmisora(record.getSourceClassName()));
    sb.append("[" + record.getLevel().getName() + "] ");
    sb.append(formatMessage(record) + "\n");
    
    if (record.getThrown() != null)
    {
      try
      {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        record.getThrown().printStackTrace(pw);
        pw.close();
        sb.append(sw.toString());
      }
      catch (Exception ex)
      {
        // ignore
      }
    }
    
    return sb.toString();
  }
  
  private static Object getClaseEmisora(final String sourceClassName)
  {
    StringTokenizer token = new StringTokenizer(sourceClassName, ".");
    String last = "Desconocida";
    while (token.hasMoreElements())
      last = (String) token.nextElement();
    return "[" + last + "]";
  }
  
}
