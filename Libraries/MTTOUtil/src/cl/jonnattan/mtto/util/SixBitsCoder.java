package cl.jonnattan.mtto.util;

/**
 * Clase que codifica el texto a formato ASCII definido por sisdef. De la cadena
 * de bytes entrante se convierte una letra cada 6 bits
 * 
 * @author Jonnattan Griffiths
 * @version 1.0 de 06/12/2019
 */
public class SixBitsCoder {
  private final byte[] alfabeto = { '(', ')', '+', '-', ':', ';', '<', '>',
      '=', '?', '.', ',', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
      'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P' };
  
  public SixBitsCoder()
  {}
  
  /**
   * Convierte una cadena de bytes en un texto codificado a 6 bits
   * 
   * @param inBuffer
   *          Array de bytes
   * @return Texto codificado a 6 bits con letras ASCII
   */
  public String getDataTo6Bits(final byte[] inBuffer)
  {
    // Tenemos la data multiplo de 3
    // Calculamos el largo en bytes que tendra la salida.
    int outLen = ((inBuffer.length / 3) * 4);
    // creamos el array de bytes de respuesta
    byte[] outBytes = new byte[outLen];
    BitBuffer bitBuffer = new BitBuffer();
    bitBuffer.setArray(inBuffer);
    // Se llena el buffer de salida
    int bitPos = 0;
    for (int i = 0; i < outLen; i++)
    {
      outBytes[i] = getLetraEncapsulated(bitBuffer.get(bitPos, 6));
      bitPos += 6;
    }
    return new String(outBytes);
  }
  
  /**
   * Obtiene la cadena de bytes original a partir del texto codificado a 6 bits
   * 
   * @param inBuffer
   *          texto con letras ASCII
   * @return cadena de Bytes
   */
  public byte[] getDataTo8Bits(final String inBuffer)
  {
    int outLen = ((inBuffer.length() / 4) * 3);
    // Calcula largo de buffer de salida
    byte[] outBytes = new byte[outLen];
    // crea buffer
    BitBuffer bitBuffer = new BitBuffer();
    // crecimiento de la data de entrada
    for (int pos = 0; pos < inBuffer.length(); pos++)
    {
      byte letra = getByteRepresentado(inBuffer.charAt(pos));
      bitBuffer.putByte(letra, 2, 6);
    }
    System.arraycopy(bitBuffer.array(), 0, outBytes, 0, outLen);
    return outBytes;
  }
  
  /**
   * Obtiene la letra que representan los 6 bit
   * 
   * @param pos
   *          Posicion
   * @return Letra representada por los 6 bit
   */
  private byte getLetraEncapsulated(byte pos)
  {
    int index = ((pos >> 2) & 0x3F);
    return alfabeto[index];
  }
  
  /**
   * Rescata los 6 bytes representados por esa letra
   * 
   * @param letra
   * @return
   */
  private byte getByteRepresentado(char letra)
  {
    int value = 0x00;
    for (int i = 0; i < alfabeto.length; i++)
    {
      if (letra == alfabeto[i])
      {
        value = i;
        break;
      }
    }
    return (byte) (value & 0x3F);
  }
  
}
