package cl.jonnattan.mtto.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * Clase encargada de comunicarse con el Manager UDP
 * 
 * @author Jonnattan Griffiths
 * @since 21/10/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class UDPSender {
  
  private InetAddress    server  = null;
  private DatagramSocket socket  = null;
  private int            port    = 0;
  private String         address = "localhost";
  private DatagramPacket output  = null;
  
  public UDPSender()
  {
    // TODO Auto-generated constructor stub
  }
  
  public void UDPSendSms(int port, String address) {
    this.port = port;
    this.address = address;
  }
  
  private boolean configSender() {
    if (this.socket == null) {
      try {
        this.socket = new DatagramSocket();
        this.server = InetAddress.getByName(address);
      }
      catch (Exception ex) {
        ex.printStackTrace();
        return false;
      }
    }
    return true;
  }
  
  /**
   * Envia mensaje a la puerta UDP de envio de mensajes
   * 
   * @param number
   * @param equipment
   * @param message
   */
  public void sendMsg(final ByteBuffer bBuffer) {
    if (configSender())
      writer(bBuffer.array());
    else System.err.println("No se puede enviar dato");
  }
  
  /**
   * Escribe la data en el puerto
   * 
   * @param bArray
   */
  private void writer(final byte[] bArray) {
    output = new DatagramPacket(bArray, bArray.length, server, port);
    try {
      socket.send(output);
      
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
  
  public int getPort() {
    return port;
  }
  
  public void setPort(int port) {
    this.port = port;
  }
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
}
