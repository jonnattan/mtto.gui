package cl.jonnattan.mtto.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

/**
 * Clase abstracta que recibe que se conecta y rebibe mensajes desde el server
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public abstract class ATCPConnection implements Runnable {
  private enum states {
    FIND_SYNC,
    FIND_HEADER,
    SAVING_DATA
  };
  
  private Thread           thread       = null;
  private Socket           clientSocket = null;
  private DataOutputStream outToServer  = null;
  private DataInputStream  inFromServer = null;
  private boolean          conected     = false;
  private String           serverIp     = "127.0.0.1";
  private int              serverPort   = 1234;
  private ByteBuffer       bufferRx     = null;
  private short            bufferLen    = 0;
  private states           state        = states.FIND_SYNC;
  
  protected static Logger  logger       = null;
  
  public ATCPConnection(String addr, int port)
  {
    super();
    logger = Logger.getLogger(this.getClass().getName());
    serverIp = addr;
    serverPort = port;
    bufferRx = ByteBuffer.allocate(CONSTConst.BUFFERSIZE);
  }
  
  public abstract void connected();
  
  public abstract void disconnected();
  
  private void conectar()
  {
    disconnected();
    do
    {
      try
      {
        Thread.sleep(1000);
        clientSocket = new Socket(serverIp, serverPort);
        inFromServer = new DataInputStream(clientSocket.getInputStream());
        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        conected = clientSocket.isConnected();
        info("Conectado a " + serverIp + ":" + serverPort
            + ". Puerto asignado: " + clientSocket.getLocalPort());
        // Si la session esta distinta de null, es porque esta HMI ya se habia
        // logeado por lo que se logea nuevamente al ESAT sin necesidad de pedir
        // nombre de user y pass
        connected();
        
      }
      catch (Exception ex)
      {
        info(" ERROR Conectando a " + serverIp + ":" + serverPort + " "
            + ex.getMessage());
      }
    }
    while (!conected);
  }
  
  protected void info(String string)
  {
    ATCPConnection.logger.info(string);
  }
  
  public void startTcpClient()
  {
    thread = new Thread(this);
    thread.setName("Cliente Tcp");
    thread.start();
  }
  
  private void desconectar()
  {
    try
    {
      if (clientSocket != null && clientSocket.isConnected())
      {
        conected = false;
        inFromServer.close();
        inFromServer = null;
        outToServer.close();
        outToServer = null;
        clientSocket.close();
        clientSocket = null;
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  public void stopTcpClient()
  {
    desconectar();
    thread.interrupt();
    thread = null;
  }
  
  public void sendMessage(final ByteBuffer bBuffer)
  {
    if (outToServer != null)
    {
      try
      {
        outToServer.write(bBuffer.array());
        info("Tx " + bBuffer.remaining() + " bytes al Server");
      }
      catch (Exception ex)
      {
        disconnected();
        connectionError("No se pueden enviar los bytes");
      }
    }
    else
    {
      connectionError("El cliente se encuentra desconectado");
    }
  }
  
  public abstract void connectionError(final String msg);
  
  @Override
  public void run()
  {
    while (true)
    {
      conectar();
      byte[] bytes = new byte[CONSTConst.BUFFERSIZE];
      int length = 0;
      while (conected)
      {
        try
        {
          length = inFromServer.read(bytes);
          if (length == -1)
          {
            conected = false;
            disconnected();
            break;
          }
          processStateMachine(length, bytes);
        }
        catch (IOException e)
        {
          // e.printStackTrace();
          break;
        }
      }
      info("Cliente Desconectado del Server");
    }
  }
  
  /**
   * Procesa todos los bytes recibidos por el canal TCP evaluando 1x1 si
   * corresponde a data o al envabezado
   * 
   * @param length
   *          Largo del Paquete recibido
   * @param bytes
   *          Cadena de bytes
   */
  private void processStateMachine(int length, byte[] bytes)
  {
    byte byteData = 0x00;
    for (int i = 0; i < length; i++)
    {
      byteData = bytes[i];
      switch (state) {
        case FIND_SYNC:
        {
          if (byteData == CONSTConst.BYTE_SYNC)
          {
            state = states.FIND_HEADER;
            bufferRx.clear();
            bufferRx.put(byteData);
          }
        }
          break;
        case FIND_HEADER:
        {
          bufferRx.put(byteData);
          if (bufferRx.position() == CONSTConst.HEADERLEN)
          {
            bufferRx.flip();
            bufferRx.get(); // byte de sync
            short len = (short) (0xFFFF & bufferRx.getShort());
            short invlen = (short) (0xFFFF & bufferRx.getShort());
            if (len == ~invlen)
            {
              bufferLen = len;
              bufferRx.clear();
              // Para que la decodificacion no falle se vuelve a la normalidad
              bufferRx.put(CONSTConst.BYTE_SYNC);
              bufferRx.putShort(len);
              bufferRx.putShort(invlen);
              state = states.SAVING_DATA;
            }
            else
            {
              // Largo no coincide con el invertido, se descarta todo
              state = states.FIND_SYNC;
            }
          }
        }
          break;
        case SAVING_DATA:
        {
          bufferLen--;
          bufferRx.put(byteData);
          if (bufferLen == 0)
          {
            notifyBuffer();
            state = states.FIND_SYNC;
          }
        }
          break;
      }
    }
  }
  
  private void notifyBuffer()
  {
    bufferRx.flip();
    ByteBuffer bb = ByteBuffer.allocate(bufferRx.remaining());
    bb.put(bufferRx);
    bb.flip();
    recivMessage(bb);
  }
  
  public abstract void recivMessage(final ByteBuffer bBuffer);
}
