package cl.jonnattan.mtto.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Lista de Funciones Estaticas del Sistema de Control Trafico Maritimo
 * 
 * @author Jonnattan Griffiths
 * @since 26/03/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class GLOBALFunction
{
  private final static char EXCLAMATION_LABEL = 0X21;
  private final static char DOLLAR_LABEL      = 0X24;
  private final static char ASTERISK_LABEL    = 0X2A;
  private final static int  ERROR_CONVERTER   = -100;
  
  /**
   * Obtiene una cadena de bytes en hexa como string para ser impresos en
   * pantalla
   * 
   * @param bytes
   * @param len
   * @return los bytes como un string
   */
  public static String getDataString(final byte[] bytes, final int len)
  {
    String Str = "";
    for (int i = 0; i < len; i++)
    {
      Str += String.format("%02X ", bytes[i]);
    }
    return Str;
  }
  
  /**
   * Convierte desde un valor double a un strin de 2 decimales
   * 
   * @param value
   *          double a convertir
   * @return error o el valor en string
   */
  public static String doubleToString(final Double value)
  {
    String str = "error";
    DecimalFormat myFormatter = new DecimalFormat("###,###.##");
    try
    {
      str = myFormatter.format(value != null ? value.doubleValue() : 0.0);
    }
    catch (ArithmeticException ex)
    {
      str = "error";
    }
    return str;
  }
  
  /**
   * Convierte desde un valor double a un strin de 2 decimales
   * 
   * @param value
   *          double a convertir
   * @return error o el valor en string
   */
  public static double stringToDouble(final String str)
  {
    double value = 0.0;
    DecimalFormat myFormatter = new DecimalFormat("###,###.##");
    try
    {
      value = myFormatter.parse(str).doubleValue();
    }
    catch (ParseException ex)
    {
      value = 0.0;
    }
    return value;
  }
  
  /**
   * Calcula el CkSum de un texto en formato ASCII
   * 
   * @param aStream
   *          Texto
   * @return 2 caracteres con el cksum calculado
   */
  public static String getCksum(final String aStream)
  {
    char checksum = 0;
    char aux = 0;
    for (int i = 0; i < aStream.length(); i++)
    {
      aux = (char) aStream.charAt(i);
      if (aux == ASTERISK_LABEL)
        break;
      if ((aux != ASTERISK_LABEL) && (aux != DOLLAR_LABEL)
          && (aux != EXCLAMATION_LABEL))
        checksum ^= aux;
    }
    String cksCalculado = String.format("%02X", (byte) checksum);
    return cksCalculado;
  }
  
  /**
   * Calcula el cksum como una suma de los bytes de la data
   * 
   * @param bArray
   *          cadena de bytes
   * @return cksum en 2 bytes
   */
  public static short cksum(final byte[] bytes)
  {
    long cksum = 0;
    for (int i = 0; i < bytes.length; i++)
      cksum += (long) (bytes[i] & 0xFF);
    return (short) (cksum & 0xFFFF);
  }
  
  /**
   * (Algoritmo Burbuja) Ordena un vector chico, no es muy eficiente, pero
   * rapido para vectores chikitos. En general los chat no son grandes en
   * cantidad de usuarios asi que es suficiente
   * 
   * @param aArray
   *          Array a ordenar (con elementos comparables)
   * @return Array ordenado de menor a mayor
   */
  public static void ordenar(short[] aArray)
  {
    short aux = 0;
    for (int i = 0; i < aArray.length - 1; i++)
    {
      for (int j = 0; j < aArray.length - 1 - i; j++)
      {
        if (aArray[j] > aArray[j + 1])
        {
          aux = aArray[j];
          aArray[j] = aArray[j + 1];
          aArray[j + 1] = aux;
        }
      }
    }
    
  }
  
  /**
   * Convierte un strin en una cadena de bytes
   * 
   * @param message
   * @return
   */
  public static byte[] strToByte(final String message)
  {
    byte[] copyTo = null;
    byte[] copyFrom = null;
    byte encoding = (byte) 0x00;
    try
    {
      copyFrom = message.getBytes(CONSTConst.TEXTENCODING);
      encoding = (byte) 0x01;
    }
    catch (UnsupportedEncodingException ex)
    {
      copyFrom = message.getBytes();
    }
    copyTo = new byte[copyFrom.length + 1];
    copyTo[0] = encoding;
    System.arraycopy(copyFrom, 1, copyTo, 0, copyTo.length);
    return copyTo;
  }
  
  /**
   * Transforma una cadena de bytes en String
   * 
   * @param bArray
   * @return String
   */
  public static String byteToStr(final byte[] bArray)
  {
    String message = null;
    byte encoding = bArray[0];
    if (encoding == 0x01)
    {
      try
      {
        message = new String(bArray, 1, (bArray.length - 1),
            CONSTConst.TEXTENCODING);
      }
      catch (UnsupportedEncodingException ex)
      {
      }
    }
    else
    {
      message = new String(bArray, 1, (bArray.length - 1));
    }
    return message;
  }
  
  /**
   * Obtiene la fecha que se escribio el mensaje
   * 
   * @param time_seg
   * @param timeZone
   * @return
   */
  public static Date getDate(final int time_seg, final short timeZone_min)
  {
    long ms = time_seg * 1000L + TimeUnit.MINUTES.toMillis(timeZone_min);
    return new Date(ms);
  }
  
  /**
   * Obtiene la longitud entre ]-180.0 y 180.0] de un string en grado min y seg
   * 
   * @param aValue
   * @return
   */
  public static double getLongitude_deg(String aValue)
  {
    double dvalue = 0.0;
    int grados = 0, min = 0, seg = 0;
    String letra = "";
    
    String[] datos = aValue.trim().split(" ");
    try
    {
      grados = Integer.parseInt(datos[0]);
      min = Integer.parseInt(datos[1]);
      seg = Integer.parseInt(datos[2]);
      letra = datos[3];
    }
    catch (NumberFormatException ex)
    {
      return ERROR_CONVERTER;
    }
    dvalue = (grados + (min / 60.0) + (seg / 3600.0));
    if (letra.equals("W") && dvalue > 0.0f)
      dvalue *= -1.0;
    return dvalue;
  }
  
  /**
   * Obtiene la latitud entre ]-90.0 y 90.0] de un string en grado min y seg
   * 
   * @param aValue
   * @return
   */
  public static double getLatitude_deg(String aValue)
  {
    double dvalue = 0.0;
    int grados = 0, min = 0, seg = 0;
    String letra = "";
    
    String[] datos = aValue.trim().split(" ");
    try
    {
      grados = Integer.parseInt(datos[0]);
      min = Integer.parseInt(datos[1]);
      seg = Integer.parseInt(datos[2]);
      letra = datos[3];
    }
    catch (NumberFormatException ex)
    {
      return ERROR_CONVERTER;
    }
    dvalue = (grados + (min / 60.0) + (seg / 3600.0));
    if (letra.equals("S") && dvalue > 0.0f)
      dvalue *= -1.0;
    return dvalue;
  }
  
  /**
   * Hace un calculo para presenter la latitud en Grados Minutos y Segundos
   * 
   * @param Longitud
   *          en Grados
   * @return Otro Formato
   * @author jgonzalez
   */
  public static String getLonGMS(double longitud)
  {
    String aux = "";
    int grados = 0, min = 0, seg = 0;
    double lat = longitud;
    if (lat > 180.0 || lat < -180.0)
    {
      return null;
    }
    String ns = "E";
    if (lat < 0)
    {
      ns = "W";
      lat = 0 - lat; // para hacer positivo al dato
    }
    grados = (int) lat;
    lat = lat - grados; // rescato los decimales de los grados
    lat = 60 * lat; // calculo los minutos
    while (lat >= 60.0)
    { // Pregunto si los minutos son mas de 60
      lat = lat - 60.0; // calculo los nuevos minutos
      grados = grados + 1; // sumo los 60 min a los grados, ed, 1 grado
    }
    min = (int) lat;
    lat = lat - min; // rescato los decimales de los min
    lat = 60 * lat; // calculo los minutos
    while (lat >= 60.0)
    { // Pregunto si los minutos son mas de 60
      lat = lat - 60.0; // calculo los nuevos minutos
      min = min + 1; // sumo los 60 min a los grados, ed, 1 grado
    }
    seg = (int) lat;
    // validamos fomato
    if (grados < 100 && grados > 10)
    {
      aux = "0";
    }
    if (grados < 10)
    {
      aux = "00" + aux;
    }
    aux = aux + String.valueOf(grados) + " ";
    if (min < 10)
    {
      aux = aux + "0" + String.valueOf(min) + " ";
    }
    else
    {
      aux = aux + String.valueOf(min) + " ";
    }
    if (seg < 10)
    {
      aux = aux + "0" + String.valueOf(seg < 30 ? seg + 1 : seg) + " " + ns;
    }
    else
    {
      aux = aux + String.valueOf(seg < 30 ? seg + 1 : seg) + " " + ns;
    }
    return aux;
  }
  
  /**
   * Hace un calculo para presenter la latitud en Grados Minutos y Segundos
   * 
   * @param latitud
   * @return Posicion Geografica Latitud
   * @author jgonzalez
   */
  public static String getLatGMS(double latitud)
  {
    String aux = "";
    int grados = 0, min = 0, seg = 0;
    double lat = latitud;
    if (lat > 90.0 || lat < -90.0)
    {
      return null;
    }
    String ns = "N";
    if (lat < 0)
    {
      ns = "S";
      lat = 0 - lat; // para hacer positivo al dato
    }
    grados = (int) lat;
    lat = lat - grados; // rescato los decimales de los grados
    lat = 60 * lat; // calculo los minutos
    while (lat >= 60.0)
    { // Pregunto si los minutos son mas de 60
      lat = lat - 60.0; // calculo los nuevos minutos
      grados = grados + 1; // sumo los 60 min a los grados, ed, 1 grado
    }
    min = (int) lat;
    lat = lat - min; // rescato los decimales de los min
    lat = 60 * lat; // calculo los minutos
    while (lat >= 60.0)
    { // Pregunto si los minutos son mas de 60
      lat = lat - 60.0; // calculo los nuevos minutos
      min = min + 1; // sumo los 60 min a los grados, ed, 1 grado
    }
    seg = (int) lat;
    // validamos fomato
    if (grados < 10)
    {
      aux = "00" + aux + String.valueOf(grados) + " ";
    }
    else
    {
      aux = "0" + aux + String.valueOf(grados) + " ";
    }
    if (min < 10)
    {
      aux = aux + "0" + String.valueOf(min) + " ";
    }
    else
    {
      aux = aux + String.valueOf(min) + " ";
    }
    if (seg < 10)
    {
      aux = aux + "0" + String.valueOf(seg < 30 ? seg + 1 : seg) + " " + ns;
    }
    else
    {
      aux = aux + String.valueOf(seg < 30 ? seg + 1 : seg) + " " + ns;
    }
    return aux;
  }
  
  /**
   * Calcula los milisegundos asociados a la estructura QNX
   * 
   * @param countTime
   *          Tiempo
   * @param overflow
   * @return Tiempo en MiliSegundo desde 01/01/1970
   * @author jgonzalez
   */
  public static long getHoraR2P_ms(long countTime, byte overflow)
  {
    long MAX_COUNT_TIME = 0x80000000L; // 2147483648
    long time_hs = (((long) overflow) * MAX_COUNT_TIME) + (long) countTime;
    return (long) (10 * time_hs);
  }
  
}
