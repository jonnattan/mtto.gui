package cl.jonnattan.mtto.util;

import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Es una clase bajada de Internet y modificada a criterio mio, sirve para
 * achicar la imagen y que sea menos pesada.
 * 
 * @author Jonnattan G.
 * @since 10/04/2019
 * @version 1.0 Copyright(c)
 * @see {@link http://www.electroduendes.com/blog/procesado-de-imagenes-en-java-ii/#more-84}
 */

public class UtilImages
{
  private static Logger  logger   = Logger.getGlobal();
  private RenderingHints opciones = new RenderingHints(null);

  public UtilImages()
  {
    // Cargo las opciones de renderizado que me apetezcan
    opciones.put(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    opciones.put(RenderingHints.KEY_ALPHA_INTERPOLATION,
        RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    opciones.put(RenderingHints.KEY_DITHERING,
        RenderingHints.VALUE_DITHER_DISABLE);
    opciones.put(RenderingHints.KEY_FRACTIONALMETRICS,
        RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    opciones.put(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
    opciones.put(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);
    opciones.put(RenderingHints.KEY_STROKE_CONTROL,
        RenderingHints.VALUE_STROKE_NORMALIZE);
    opciones.put(RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  }

  /**
   * Devuelve la lista de formatos disponibles a leer por ImageIO
   * 
   * @return un array de strings con los mismos.
   */
  public String[] dameFormatosUsables()
  {
    return ImageIO.getReaderFormatNames();
  }

  /**
   * Calcula el factor de escala minimo y en base a eso escala la imagen segun
   * dicho factor.
   * 
   * @param nMaxWidth  maximo tama�o para el ancho
   * @param nMaxHeight nmaximo tama�o para el alto
   * @param imagen     Imagen que vamos a escalar
   * @return Devuelve la imagen escalada para poderla trastocar o null si hay
   *         error
   */
  public BufferedImage escalarATamanyo(final BufferedImage imagen,
      final int maximoAncho, final int maximoAlto)
  {

    // Comprobacion de parametros
    if (imagen == null || maximoAlto == 0 || maximoAncho == 0) {
      return null;
    }

    // Capturo ancho y alto de la imagen
    int anchoImagen = imagen.getHeight();
    int altoImagen  = imagen.getWidth();

    // Calculo la relacion entre anchos y altos de la imagen
    double escalaX = (double) maximoAncho / (double) anchoImagen;
    double escalaY = (double) maximoAlto / (double) altoImagen;

    // Tomo como referencia el minimo de las escalas
    double fEscala = Math.min(escalaX, escalaY);

    // Devuelvo el resultado de aplicar esa escala a la imagen
    return escalar(fEscala, imagen);
  }

  /**
   * Escala una imagen en porcentaje.
   * 
   * @param factorEscala ejemplo: factorEscala=0.6 (escala la imagen al 60%)
   * @param srcImg       una imagen BufferedImage
   * @return un BufferedImage escalado
   */
  public BufferedImage escalar(final double factorEscala,
      final BufferedImage srcImg)
  {

    // Comprobacion de parametros
    if (srcImg == null) {
      return null;
    }

    // Compruebo escala nula
    if (factorEscala == 1) {

      return srcImg;
    }

    // La creo con esas opciones
    AffineTransformOp op = new AffineTransformOp(
        AffineTransform.getScaleInstance(factorEscala, factorEscala), opciones);

    // Devuelve el resultado de aplicar el filro sobre la imagen
    return op.filter(srcImg, null);
  }

  /**
   * Metodo que guarda una imagen en disco
   * 
   * @param imagen      Imagen a almacenar en disco
   * @param rutaFichero Ruta de la imagen donde vamos a salvar la imagen
   * @param formato     Formato de la imagen al almacenarla en disco
   * @return Booleano indicando si se consiguio salvar con exito la imagen
   */
  public boolean save(final BufferedImage imagen, final String rutaFichero,
      final String formato)
  {
    if (imagen != null && rutaFichero != null && formato != null) {
      try {
        ImageIO.write(imagen, formato, new File(rutaFichero));
        return true;
      } catch (Exception e) {
        if (logger.isLoggable(Level.INFO) == true) {
          String CODIGO_MENSAJE_ERROR_GUARDADO_FICHERO = "No se pudo guardar correctamente la imagen en "
              + rutaFichero;
          logger.info(CODIGO_MENSAJE_ERROR_GUARDADO_FICHERO);
        }
        return false;
      }
    } else {
      logger.severe("No hay parametros correctos");
      return false;
    }
  }
}
