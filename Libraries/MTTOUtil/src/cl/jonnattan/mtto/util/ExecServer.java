package cl.jonnattan.mtto.util;

import java.io.InputStream;

public class ExecServer implements Runnable
{
  private Thread       thread  = null;
  private final String command;
  private Process      process = null;

  public ExecServer(String app)
  {
    super();
    this.command = app;
  }

  public void start()
  {
    thread = new Thread(this);
    thread.start();
  }

  public void stop()
  {
    try {
      if (process != null)
        process.destroy();
      while (process.isAlive())
        Thread.sleep(1);
      thread.interrupt();
      thread = null;
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public boolean isRunProgram()
  {
    return (process != null && process.isAlive());
  }

  @Override
  public void run()
  {
    byte[]  bytes = new byte[1024];
    int     len   = 0;
    Runtime rt    = Runtime.getRuntime();
    try {
      process = rt.exec(this.command);
      if (process.isAlive()) {
        len = 0;
        InputStream is = process.getInputStream();
        while ((len = is.read(bytes, 0, bytes.length)) > 0) {
          String texto = new String(bytes, 0, len);
          System.out.print("SERVER: " + texto);
          texto = null;
          Thread.sleep(50);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
