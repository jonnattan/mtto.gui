package cl.jonnattan.mtto.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TimerEq extends TimerTask
{
  public interface EqAdvisor {
    public void infoEq();
  }
  
  private final Timer timer;
  private final List<EqAdvisor> listerners;
  
  public TimerEq( long miliseg)
  {
    super();
    listerners = new ArrayList<>();
    timer = new Timer();
    timer.scheduleAtFixedRate(this, 30000, miliseg);
  }
  
  public void addListener( final EqAdvisor listener )
  {
    listerners.add(listener);
  }
  
  @Override
  public void run()
  {
    for( EqAdvisor listener : listerners)
      listener.infoEq();
  }

}
