package cl.jonnattan.mtto.util;

/**
 * @author Jonnattan Gonzalez
 * @version 1.0 de 17-07-2019 Copyright(c) -  
 */
public class BitBuffer
{
  public static int  EXIT_FAILURE = 0;
  public static int  EXIT_SUCCESS = 1;

  public static int  BITS_IN_BYTE = 8;
  private static int BUFSIZ       = 1024;

  private int        _position    = 0;
  private int        _length      = 0;
  private int        _capacity    = 0;
  private int        _sizeBuffer  = 0;
  private byte[]     pData        = null;

  private byte[]     maskLSB;
  private byte[]     maskMSB;
  private boolean    logEnable    = false;

  public BitBuffer()
  {
    _position = 0;
    _length = 0;
    _sizeBuffer = BUFSIZ;
    _capacity = (BUFSIZ * BITS_IN_BYTE);
    pData = new byte[BUFSIZ];
    initMask();
  }

  public BitBuffer( int aSizeBuffer )
  {
    _position = 0;
    _length = 0;
    _sizeBuffer = aSizeBuffer;
    _capacity = (aSizeBuffer * BITS_IN_BYTE);
    pData = new byte[aSizeBuffer];
    initMask();
  }

  /**
   * Ingresa bits al buffer considerando el orden desde LSB -> MSB. En
   * posiciones imprimibles es asi: 0123 4567 Pametros:
   * 
   * @param aByte
   *          Byte de data
   * @param aOffset
   *          Posicion del primer bit de data considerado
   * @param aLen
   *          Cantidad de Bit
   * @return
   */
  public int putByte( byte aByte, int aOffset, int aLen )
  {
    int success = EXIT_SUCCESS;
    if ( (aLen + _length) > _capacity )
    {
      System.out.println( "[BitBuffer] ERROR 1" );
      success = EXIT_FAILURE;
    }
    if ( aLen > BITS_IN_BYTE )
    {
      System.out.println( "[BitBuffer] ERROR 2" );
      success = EXIT_FAILURE;
    }
    if ( (aLen + aOffset) > BITS_IN_BYTE )
    {
      System.out.println( "[BitBuffer] ERROR 3" );
      success = EXIT_FAILURE;
    }

    if ( success == EXIT_FAILURE )
    {
      System.out
          .println( "[BitBuffer] ERROR: Algo anda mal con el ingreso de bits con parametros: aByte["
              + String.format( "%02X", aByte )
              + "] aOffset["
              + aOffset
              + "] aLen[" + aLen + "]" );
      return success;
    }

    byte mask = 0x00;
    if ( logEnable )
      System.out.format( "[BitBuffer] Put %d bit de 0x%02X desde lugar %d\n",
          aLen, aByte, aOffset );
    // ---------------------------------------------------------
    byte bite = getReOrderByte( aByte, aOffset, aLen );
    // ---------------------------------------------------------
    // Cantidad de bits ocupados en el ultimo bytes de data
    // ... xxxxxxxx xxxxxxxx xxxxx000 00000000 00000000 ...
    // En este caso por ejemplo el bitOfMore es 5
    int bitsOfMore = (int) ((_length >= BITS_IN_BYTE) ? (_length % BITS_IN_BYTE)
        : _length);

    if ( logEnable )
      System.out.format(
          "[BitBuffer] Hay %d bit de data. Trabajamos en byte[%d]\n", _length,
          _position );
    if ( logEnable )
      System.out.format( "[BitBuffer] Del byte actual hay %d bit de data \n",
          bitsOfMore );
    // Puede ser que no haya data inicialmente o que la data
    // llenada hasta el momento este justo al tope de un byte
    if ( bitsOfMore == 0 )
    {
      if ( logEnable )
        System.out.format(
            "[BitBuffer] Se llena el byte de dato con %d bits.\n", aLen );
      mask = getMSBMask( aLen );
      pData[_position] = (byte) (bite & mask);
      _length += aLen;
      // Avanza solo si se mete exatacamente un byte al buffer
      if ( aLen == BITS_IN_BYTE )
      {
        _position++;
        if ( logEnable )
          System.out.format( "[BitBuffer] Se aumenta _position a: %d.\n",
              _position );
      } else
      {
        if ( logEnable )
          System.out
              .format( "[BitBuffer] No aumenta el contador porque no se completa el byte\n" );
      }
      // if(logEnable) PrintHexaData();
    }
    // Cuando el numero de bits que estan utilizados en el ultimo byte de data
    // es mayor que cero se debe considerar el corrimiento a la derecha, pues
    // anteriormente la data la dejamos al lado izq del byte
    // Si con el corrimiento incluido + la data que se quiere ingresar no se
    // alcanza
    // a completar el byte hasta aqui quedamos. Sino, se ocupa el siguiente byte
    if ( bitsOfMore > 0 )
    {
      // Numero de bit que se desean ingresar + los bit que ya llenan el ultimo
      // byte
      // ... xxxxxxxx xxxxx000 00000000 ... e ingresan yyyyyy00
      // ... xxxxxxxx xxxxxyyy yyy00000 ...
      // aLen = 6 bitsOfMore = 5 --> nroBitsIn = 11
      int nroBitsIn = bitsOfMore + aLen;
      // N de bit de data que faltan para completar el byte actual en la
      // posicion _position
      int missingBits = (BITS_IN_BYTE - bitsOfMore);
      // Cantidad de bits que ingresan no es suficiente para completar el byte
      // ... xxxxxxxx xxyyyy00 ...
      if ( nroBitsIn < BITS_IN_BYTE )
      {
        if ( logEnable )
          System.out.println( "[BitBuffer] No se completa el byte" );
        // Para el ejemplo la mascara es: Mask = 00111100
        mask = getMask( bitsOfMore, aLen );
        byte byte_dato = (byte) (pData[_position] | ((bite >> bitsOfMore) & mask));

        if ( logEnable )
          System.out.format(
              "[BitBuffer] (%02X |((%02X >> %d) & %02X)) = %02X \n",
              pData[_position], bite, bitsOfMore, mask, byte_dato );
        pData[_position] = byte_dato;
        _length += aLen;
        // if(logEnable) PrintHexaData();
        // No avanza la posicion en el buffer, solamente la cantidad de bits de
        // data
      } else if ( nroBitsIn == BITS_IN_BYTE )
      {
        if ( logEnable )
          System.out.format( "[BitBuffer] Se llena justo el byte \n" );
        mask = getLSBMask( missingBits );
        pData[_position] = (byte) (pData[_position] | ((bite >> bitsOfMore) & mask));
        _length += aLen;
        // if(logEnable) PrintHexaData();
        _position++;
      } else
      {
        if ( logEnable )
          System.out.format(
              "[BitBuffer] Sobran %d bit para completar el Byte[%d] \n",
              missingBits, _position );
        mask = getLSBMask( missingBits );
        byte byte_dato = (byte) (pData[_position] | ((bite >> bitsOfMore) & mask));
        if ( logEnable )
          System.out.format(
              "[BitBuffer] (%02X | ((%02X >> %d) & %02X)) = %02X\n",
              pData[_position], bite, bitsOfMore, mask, byte_dato );
        pData[_position] = byte_dato;
        _position++;
        mask = getMSBMask( bitsOfMore );
        // En el nuevo byte se supone que no hay data
        byte_dato = (byte) ((bite << missingBits) & mask);
        pData[_position] = byte_dato;
        if ( logEnable )
          System.out.format( "[BitBuffer] ((%02X << %d) & %02X) = %02X\n",
              bite, missingBits, mask, byte_dato );
        // En numero de data en bits
        _length += aLen;
        // if(logEnable) PrintHexaData();
      }
    }
    return success;
  }

  /**
   * Permite ingresar mas de 8 Bits l buffer
   * 
   * @param aBytes
   *          La cantidad de bits es pasada por parametro
   * @param aLen
   *          Cantidad de bits
   * @param aBigEndian
   *          Endian de como se ingresan
   * @return
   */
  public int putBytes( byte[] aBytes, int aLen, boolean aBigEndian )
  {
    int success = EXIT_SUCCESS;
    int bitsOfMore = (aLen >= BITS_IN_BYTE) ? (aLen % BITS_IN_BYTE) : aLen;
    int nroBytes = (int) ((aLen - bitsOfMore) / BITS_IN_BYTE);
    boolean more = bitsOfMore > 0;
    nroBytes = more ? nroBytes + 1 : nroBytes;
    if ( logEnable )
      System.out
          .println( "[BitBuffer] +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" );
    if ( logEnable )
      System.out.format( "[BitBuffer] bitsOfMore[%d] Bytes[%d] more[%s]\n",
          bitsOfMore, nroBytes, more ? "TRUE" : "FALSE" );
    if ( aBigEndian )
    {
      if ( logEnable )
      {
        System.out.format(
            "[BitBuffer] Put Big Endian [%d] bit osea %d bytes: ", aLen,
            nroBytes );
        for ( int i = 0; i < nroBytes; i++ )
          System.out.format( "%02X ", (byte) aBytes[i] );
        System.out.println();
        System.out
            .println( "[BitBuffer] +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
      }
      // La forma de trabajo por defecto de QNX es Big Endian
      for ( int i = 0; i < nroBytes; i++ )
      {
        int pos = (nroBytes - i - 1);
        if ( logEnable )
          System.out.format( "[BitBuffer] Put[pArray[%d]] = Put(0x%02X)\n",
              pos, aBytes[pos] );
        if ( pos > 0 )
        {
          if ( logEnable )
            System.out.format( "[BitBuffer] Put(0x%02X)\n", aBytes[pos] );
          success = putByte( aBytes[pos], 0, BITS_IN_BYTE );
          if ( logEnable )
            System.out
                .println( "[BitBuffer] -------------------------------------------------------------------------------" );
          if ( success == 0 )
            return success;
        }
      }
      if ( more )
      {
        if ( logEnable )
          System.out.format( "[BitBuffer] Put(%02X, 0, %d)\n", aBytes[0],
              bitsOfMore );
        success = putByte( aBytes[0], 0, bitsOfMore );
        if ( success == EXIT_FAILURE )
          return success;
      } else
      {
        if ( logEnable )
          System.out.format( "[BitBuffer] Put0(0x%02X)\n", aBytes[0] );
        success = putByte( aBytes[0], 0, BITS_IN_BYTE );
        if ( logEnable )
          System.out
              .println( "[BitBuffer] -------------------------------------------------------------------------------" );
        if ( success == EXIT_FAILURE )
          return success;
      }
    } else
    {
      if ( logEnable )
      {
        System.out
            .println( "[BitBuffer] +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" );
        System.out.format( "[BitBuffer] Put Normal %d bit osea %d bytes: ",
            aLen, nroBytes );
        for ( int i = 0; i < nroBytes; i++ )
          System.out.format( "%02X ", (byte) aBytes[i] );
        System.out.println();
        System.out
            .println( "[BitBuffer] +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" );
      }
      int len = more ? nroBytes - 1 : nroBytes;
      for ( int i = 0; i < len; i++ )
      {
        success = putByte( aBytes[i], 0, BITS_IN_BYTE );
        if ( logEnable )
          System.out
              .println( "[BitBuffer] -------------------------------------------------------------------------------" );
        if ( success == EXIT_FAILURE )
          return success;
      }
      if ( more )
      {
        if ( logEnable )
          System.out.println( "[BitBuffer] finalmente " + bitsOfMore
              + " bits desde " + String.format( " %02X ", aBytes[len] ) );
        success = putByte( aBytes[len], 0, bitsOfMore );
      }
    }
    return success;
  }

  // Se reordena
  private byte getReOrderByte( byte aByte, int aOffset, int aLen )
  {
    // Dejamos los bits considerados a la izquierda
    byte mask = getMSBMask( aLen );
    byte bite = (byte) ((aByte << aOffset) & mask);
    if ( logEnable )
      System.out.format(
          "[BitBuffer] Reordenando: ((%02X << %d) & %02X) = %02X\n", aByte,
          aOffset, mask, bite );
    return bite;
  }

  private byte getMSBMask( int aLen )
  {
    return maskMSB[aLen - 1];
  }

  private byte getLSBMask( int aLen )
  {
    return maskLSB[aLen - 1];
  }

  private byte getMask( int aOffset, int aLen )
  {
    return (byte) (getMSBMask( aOffset + aLen ) & getLSBMask( BITS_IN_BYTE
        - aOffset ));
  }

  private void initMask()
  {
    maskLSB = new byte[BITS_IN_BYTE];
    maskMSB = new byte[BITS_IN_BYTE];

    maskMSB[0] = (byte) 0x80;
    maskLSB[0] = (byte) 0x01;
    maskMSB[1] = (byte) 0xC0;
    maskLSB[1] = (byte) 0x03;
    maskMSB[2] = (byte) 0xE0;
    maskLSB[2] = (byte) 0x07;
    maskMSB[3] = (byte) 0xF0;
    maskLSB[3] = (byte) 0x0F;
    maskMSB[4] = (byte) 0xF8;
    maskLSB[4] = (byte) 0x1F;
    maskMSB[5] = (byte) 0xFC;
    maskLSB[5] = (byte) 0x3F;
    maskMSB[6] = (byte) 0xFE;
    maskLSB[6] = (byte) 0x7F;
    maskMSB[7] = (byte) 0xFF;
    maskLSB[7] = (byte) 0xFF;
  }

  /**
   * Obtiene el total del buffer lleno de bits y ademas por referencia pasa la
   * cantidad de bytes que son necesarios para guardar la cantidad de bits
   */
  public byte[] array()
  {
    return pData;
  }

  /**
   * obtiene la cantidad de bits pedidos (max 1 byte). Llena la respuesta desde
   * el MSB considerando el byte como [MSB]--> = 01234567
   * 
   * @param initBit
   *          : Bit Inicial
   * @param aLen
   *          : Cantidad de Bits
   * @return
   */
  public byte get( int initBit, int aLen )
  {
    // get(4,6);
    // 0123[4567 01]234567 01234567
    byte bit = 0x00;
    byte bitAux = 0x00;
    // Valida que la cantidad de bits pedidos sea menor que 1 byte
    if ( aLen > BITS_IN_BYTE )
      return bit;
    // Verifica que haya suficiente data para sacar lo que se necesita
    int posFinal = ((aLen + initBit) - 1);
    if ( posFinal > (_length - 1) )
      return bit;
    // -------------------------------------------------------------------------
    int byteInit = (int) initBit >= BITS_IN_BYTE ? (initBit / BITS_IN_BYTE) : 0;
    // Cantidad de Bits no considerados en el Byte inicial
    int dummyBits = (initBit < BITS_IN_BYTE) ? initBit
        : (initBit % BITS_IN_BYTE);
    int bitsOfMore = (BITS_IN_BYTE - dummyBits);
    byte mask = 0;
    if ( pData != null )
    {
      // verifico si la cantidad de bits a la derecha del bit inicial
      // es mayor a la cantidad de bits que se piden (antes de completar el
      // byte)
      boolean caen = aLen <= bitsOfMore;
      if ( caen )
      {
        mask = getMSBMask( aLen );
        bit = (byte) ((pData[byteInit] << dummyBits) & mask);
        // System.out.println(String.format("%02X & %02X ", bit, mask));
      } else
      {
        mask = getMSBMask( bitsOfMore );
        bit = (byte) ((pData[byteInit] << dummyBits) & mask);
        // System.out.println(String.format("%02X & %02X ", bit, mask));
        mask = getLSBMask( dummyBits );
        bitAux = (byte) (((pData[byteInit + 1] >> bitsOfMore) & mask));
        // System.out.println(String.format("%02X & %02X ", bitAux, mask));
        mask = getMSBMask( aLen );
        bit = (byte) ((bit | bitAux) & mask);
        // System.out.println(String.format("%02X & %02X ", bit, mask));
      }
    }
    return bit;
  }

  /**
   * Se obtienen los bits que se desean
   * 
   * @param initBit
   * @param aLen
   * @return
   */
  public byte[] getBits( int initBit, int aLen )
  {
    byte[] byteData = null;
    if ( aLen > BITS_IN_BYTE )
    {
      // Calculamos el tamano de la respuesta y creamos la data
      int resto = (aLen % BITS_IN_BYTE);
      int bytesLen = (int) (aLen / BITS_IN_BYTE);
      boolean isExacto = (resto == 0);
      bytesLen = isExacto ? bytesLen : (bytesLen + 1);
      byteData = new byte[bytesLen];
      // -------------------------------------------------------
      int pos = initBit;
      for ( int i = 0; i < bytesLen - 1; i++ )
      {
        byteData[i] = get( pos, BITS_IN_BYTE );
        pos += BITS_IN_BYTE;
      }
      byteData[bytesLen - 1] = get( pos, isExacto ? BITS_IN_BYTE : resto );
    }
    return byteData;
  }

  public void setArray( byte[] aData )
  {
    _length = (BITS_IN_BYTE * aData.length);
    _position = 0;
    _sizeBuffer = _length;
    _capacity = (_sizeBuffer * BITS_IN_BYTE);
    System.arraycopy( aData, 0, pData, 0, aData.length );
  }

  // dice la cantidad de bits utilizados
  public int remaining()
  {
    return _capacity - _length;
  }

  public int length()
  {
    return _length;
  }

  public int capacity()
  {
    return _capacity;
  }

  public int capacityInByte()
  {
    return _sizeBuffer;
  }

}
