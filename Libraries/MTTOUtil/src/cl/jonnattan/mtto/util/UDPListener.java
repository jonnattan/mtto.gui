/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.jonnattan.mtto.util;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Observable;

/**
 * Monitor de puerta UDP
 * 
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class UDPListener extends Observable implements Runnable {
  private DatagramSocket socket    = null;
  private int            port      = 0;
  private String         addressRx = "";
  
  public UDPListener(int port)
  {
    this.port = port;
  }
  
  @Override
  public void run() {
    ByteBuffer bb = null;
    if (port > 0) {
      byte[] buffer = new byte[CONSTConst.BUFFERSIZE];
      DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
      try {
        socket = new DatagramSocket(port);
        socket.setReuseAddress(true);
        // socket.bind( new InetSocketAddress( "localhost", port ) );
        while (true) {
          System.out.println("Escucho Puerto: " + port);
          socket.receive(incoming);
          addressRx = incoming.getAddress().getHostAddress();
          System.out.println("Recibo " + incoming.getLength() + " byte de "
              + addressRx);
          bb = ByteBuffer.wrap(buffer, 0, incoming.getLength());
          setChanged();
          notifyObservers(bb);
        }
        
      }
      catch (Exception ex) {
        // ex.printStackTrace();
      }
    }
    if (socket != null) {
      socket.disconnect();
      socket.close();
    }
    socket = null;
    System.out.println("Listener UDP Terminado en Puerto: " + port);
  }
  
  public String getAddressRx() {
    return addressRx;
  }
  
  /**
   * @param terminated
   *          the terminated to set
   */
  public void setTerminated() {
    if (socket != null)
      socket.close();
  }
}
