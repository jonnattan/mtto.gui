package cl.jonnattan.mtto.util;

import java.awt.Font;

/**
 * Constantes del proyecto que son utiles para todos los programas
 * 
 * @author Jonnattan Griffiths
 * @since 26/03/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class CONSTConst
{
  // ---------------------------------------------------------------
  // Tamanio de los Buffer que son creados en el sistema
  // ---------------------------------------------------------------
  /** Buffer de datos tacticos, se saco del CDC */
  public final static int BUFFERSIZE   = (100 * 1024);
  /** Tamanio de Archivo Adjunto a un Chat */
  public final static int FILESIZE     = (10 * 1024);
  public final static int LITTLEBUFFER = 1024;

  // ---------------------------------------------------------------
  // Puertos NO pasados por configuracion (inventados como mejoras)
  // ---------------------------------------------------------------
  /** Puerto Coordinacion Programa Cambio Claves */
  public final static int UDP_PORT_KEY      = 4512;
  /** Puerto Respuesta para Cambio Clave */
  public final static int UDP_PORT_RESPONSE = 4514;
  /** Puerto Aviso De Conexion o Desconexion a Data Service */
  public final static int UDP_PORT_AVISOR   = 4514;
  /** Puerto Respuesta de Script de Conexion y Desconexion */
  public final static int UDP_PORT_SCRIPT   = 4515;
  public final static int PATHPHOTOTYPE     = 1;
  // Contantes para codificaciones/decodificaciones
  public final static String TEXTENCODING = "ISO-8859-1";
  public final static byte   BYTE_SYNC    = 0x7E;
  public final static int    HEADERLEN    = 5;

  // ---------------------------------------------------------------
  // Utiles para el sistema. Graficos y Otros
  // ---------------------------------------------------------------
  // Font del sistema
  public final static Font font = new Font("Monospaced", Font.BOLD, 12);

  // ----------------------------------------------------------------
  public final static String barra = System.getProperty("file.separator");
  // Ruta del Sistema Control Trafico Maritimo Movil
  public final static String PATH_MTTO = "C:" + barra + "JONNY" + barra + "MTTO" + barra;
  // Ruta absoluta de las fotografias
  public final static String PATH_IMAGE = PATH_MTTO + "fotos";
}
