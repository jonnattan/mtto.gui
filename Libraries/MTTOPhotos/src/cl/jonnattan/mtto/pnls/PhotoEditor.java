package cl.jonnattan.mtto.pnls;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import cl.jonnattan.mtto.photo.RealPhoto;

public class PhotoEditor extends JPanel
{
  public enum EImageType
  {
    REDUCIDA, NORMAL
  };
  
  private static final long serialVersionUID = 1L;
  private final String      DEFAULTPATH      = "C:/Users/Nata/Desktop/JONNATTAN/GUI/Mantenimiento/resources/";
  private final String      DEFAULTFILE      = "default.png";
  private JLabel            lblImage         = null;
  private JButton           btnLoad          = null;
  private BufferedImage     defaultIcon      = null;
  private String            pathFileImage    = null;
  private String            nameFileImage    = null;
  private BufferedImage     original         = null;
  private BufferedImage     reducida         = null;
  private String            photoFolder      = ".";
  private int               sizeMax          = 10 * 1024;
  private int               width            = 200;
  private int               height           = 200;
  private static Logger     logger           = Logger.getGlobal();
  private UtilImage         util             = null;
  private RealPhoto         pnlRealSize      = null;
  private boolean           hasChange        = false;
  
  public PhotoEditor()
  {
    super();
    util = new UtilImage();
    initialize();
    backIconDefault();
  }
  
  private void initialize()
  {
    setLayout(new BorderLayout(0, 0));
    setBorder(new TitledBorder(null, "Imagen", TitledBorder.LEADING,
        TitledBorder.TOP, null, null));
    
    lblImage = new JLabel("");
    lblImage.setHorizontalAlignment(SwingConstants.CENTER);
    lblImage.addMouseListener(new MouseAdapter()
    {
      @Override
      public void mouseClicked(MouseEvent event)
      {
        if (event.getClickCount() == 1)
        {
          showOriginalPhoto();
        }
      }
    });
    add(lblImage, BorderLayout.CENTER);
    
    JPanel panel = new JPanel();
    panel.setOpaque(false);
    add(panel, BorderLayout.SOUTH);
    
    btnLoad = new JButton("Cargar");
    btnLoad.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        if (((JButton) event.getSource()).isEnabled())
          loadImagenFile();
      }
    });
    panel.add(btnLoad);
    btnLoad.setOpaque(false);
    btnLoad.setFont(new Font("Tahoma", Font.BOLD, 11));
  }
  
  @Override
  public void setEnabled(boolean enable)
  {
    // setEnabled(enable);
    if (lblImage != null)
      lblImage.setEnabled(enable);
    if (btnLoad != null)
      btnLoad.setEnabled(enable);
  }
  
  /**
   * Crea un Frame con una Fotografia
   */
  protected void showOriginalPhoto()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        if (pnlRealSize == null)
          pnlRealSize = new RealPhoto();
        pnlRealSize.loadFoto(original);
      }
    });
  }
  
  /**
   * Setea los parametros de alto y ancho de las fotos reducidas
   * 
   * @param height
   * @param width
   */
  public void setPhotoDimension(final int height, final int width)
  {
    this.height = height;
    this.width = width;
    backIconDefault();
  }
  
  /**
   * Setea el tamanno max del archivo
   * 
   * @param sizeMax
   */
  public void setSizeMax(final int sizeMax)
  {
    this.sizeMax = sizeMax;
  }
  
  public void setPhotoFolder(final String folder)
  {
    this.photoFolder = folder;
  }
  
  /**
   * Invoka la carga de archivos de fotografias
   */
  private void loadImagenFile()
  {
    JFileChooser fchooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter(
        "Imagenes Soportadas", ImageIO.getReaderFormatNames());
    fchooser.setFileFilter(filter);
    fchooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fchooser.setMultiSelectionEnabled(false);
    fchooser.setCurrentDirectory(new File(photoFolder));
    int res = fchooser.showOpenDialog(null);
    if (res == JFileChooser.APPROVE_OPTION)
    {
      File file = fchooser.getSelectedFile();
      long tam = file.length();
      if (tam <= sizeMax)
      {
        loadFromFile(file);
        hasChange = true;
      }
      else
      {
        String msg = "El tama�o del archivo no puede ser mayor a "
            + (sizeMax / 1024) + " Kb.";
        JOptionPane.showMessageDialog(PhotoEditor.this, msg, "Sr Usuario",
            JOptionPane.ERROR_MESSAGE);
      }
    }
    
    if (res == JFileChooser.CANCEL_OPTION)
    {
      // do nothing
    }
  }
  
  /**
   * Carga la imagen en el panel
   * 
   * @param bytes
   * @param formato
   */
  public void loadFromBytes(final byte[] bytes, final String formato)
  {
    try
    {
      original = util.bytesToBufferedImage(bytes, formato);
      pathFileImage = null;
      nameFileImage = null;
      loadBufferedImage(original);
    }
    catch (IOException ex)
    {
      String msg = "Error Cargando Archivo de imagen";
      JOptionPane.showMessageDialog(PhotoEditor.this, msg, "Sr Usuario",
          JOptionPane.ERROR_MESSAGE);
      logger.log(Level.SEVERE, "Cargando Imagen ", ex);
    }
  }
  
  /**
   * Carga la imagen en el panel desde un archivo
   * 
   * @param file
   */
  public void loadFromFile(final File file)
  {
    try
    {
      pathFileImage = file.getAbsolutePath();
      nameFileImage = file.getName();
      original = ImageIO.read(file);
      loadBufferedImage(original);
    }
    catch (IOException ex)
    {
      String msg = "Error Cargando archivo de imagen";
      JOptionPane.showMessageDialog(PhotoEditor.this, msg, "Sr Usuario",
          JOptionPane.ERROR_MESSAGE);
      logger.log(Level.SEVERE, "Cargando Imagen " + file.getAbsolutePath(), ex);
    }
  }
  
  /**
   * Setea la Imagen por defecto
   */
  public void backIconDefault()
  {
    original = getDefaultImage();
    loadBufferedImage(original);
    hasChange = false;
  }
  
  private BufferedImage getDefaultImage()
  {
    if (defaultIcon == null)
    {
      try
      {
        pathFileImage = DEFAULTPATH;
        nameFileImage = DEFAULTFILE;
        URL url = PhotoEditor.class.getResource(DEFAULTPATH + DEFAULTFILE);
        if(url != null)
          defaultIcon = ImageIO.read(url);
      }
      catch (IOException e)
      {
        logger.log(Level.SEVERE, "Cargando Imagen ", e);
      }
    }
    return defaultIcon;
  }
  
  /**
   * Muestra imagen
   * 
   * @param bImage
   */
  private void loadBufferedImage(final BufferedImage bImage)
  {
    if(bImage == null) return;
    reducida = util.escalarImagen(bImage, width, height);
    if (reducida != null)
      lblImage.setIcon(new ImageIcon(reducida));
    else
      logger.warning("No se pudo Escalar Imagen");
  }
  
  /**
   * Retorna el path de la imagen
   * 
   * @return
   */
  public String getPathImagen()
  {
    return pathFileImage;
  }
  
  public String getNameFileImage()
  {
    return nameFileImage;
  }
  
  /**
   * Obtiene la cadena de bytes asociada a la Imagen
   * 
   * @param type
   * @param formato
   * @return
   */
  public byte[] getBytesImagen(final EImageType type, final String formato)
  {
    byte[] bArray = null;
    try
    {
      switch (type)
      {
        case REDUCIDA:
          bArray = util.bufferedImgToBytes(reducida, formato);
          break;
        
        case NORMAL:
          bArray = util.bufferedImgToBytes(original, formato);
          break;
      }
    }
    catch (IOException ex)
    {
      logger.log(Level.SEVERE, "Obteniendo Imagen ", ex);
    }
    return bArray;
  }
  
  public boolean hasChange()
  {
    return hasChange;
  }
  
  /**
   * Guarda Ambas imagenes
   * 
   * @param nombreSinFormato
   */
  public void saveImages(final String ruta)
  {
    if (hasChange)
    {
      try
      {
        StringTokenizer token = new StringTokenizer(getNameFileImage(), ".");
        if (token.hasMoreElements())
        {
          String name = token.nextToken();
          name = name.replaceAll(" ", "_");
          util.saveImage(original, ruta + name, "bmp");
          util.saveImage(reducida, ruta + name, "jpg");
        }
        
      }
      catch (IOException ex)
      {
        logger.log(Level.SEVERE, "Guardando Imagen ", ex);
      }
    }
  }
  
  /**
   * 
   * @return Nombre de la imagen pequenia
   */
  protected String getNameImageRedicida()
  {
    String nameImagen = "";
    StringTokenizer token = new StringTokenizer(getNameFileImage(), ".");
    if (token.hasMoreElements())
    {
      nameImagen = token.nextToken();
      nameImagen = nameImagen.replaceAll(" ", "_");
    }
    return nameImagen + ".jpg";
  }
  
}
