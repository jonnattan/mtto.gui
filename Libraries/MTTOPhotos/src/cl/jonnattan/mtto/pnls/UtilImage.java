package cl.jonnattan.mtto.pnls;

import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

public class UtilImage
{
  
  private RenderingHints opciones = new RenderingHints(null);
  private List<String>   formatos = null;
  
  public UtilImage()
  {
    // Cargo las opciones de renderizado que me apetezcan
    opciones.put(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    opciones.put(RenderingHints.KEY_ALPHA_INTERPOLATION,
        RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    opciones.put(RenderingHints.KEY_DITHERING,
        RenderingHints.VALUE_DITHER_DISABLE);
    opciones.put(RenderingHints.KEY_FRACTIONALMETRICS,
        RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    opciones.put(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
    opciones.put(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);
    opciones.put(RenderingHints.KEY_STROKE_CONTROL,
        RenderingHints.VALUE_STROKE_NORMALIZE);
    opciones.put(RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    formatos = new ArrayList<String>();
    for (String formato : ImageIO.getReaderFormatNames())
      formatos.add(formato);
  }
  
  /**
   * Calcula el factor de escala minimo y en base a eso escala la imagen segun
   * dicho factor.
   * 
   * @param nMaxWidth
   *          maximo tama�o para el ancho
   * @param nMaxHeight
   *          nmaximo tama�o para el alto
   * @param imagen
   *          Imagen que vamos a escalar
   * @return Devuelve la imagen escalada para poderla trastocar o null si hay
   *         error
   */
  public BufferedImage escalarImagen(final BufferedImage imagen,
      final int maximoAncho, final int maximoAlto)
  {
    
    // Comprobacion de parametros
    if (imagen == null || maximoAlto == 0 || maximoAncho == 0)
    {
      return null;
    }
    
    // Capturo ancho y alto de la imagen
    int anchoImagen = imagen.getHeight();
    int altoImagen = imagen.getWidth();
    
    // Calculo la relacion entre anchos y altos de la imagen
    double escalaX = (double) maximoAncho / (double) anchoImagen;
    double escalaY = (double) maximoAlto / (double) altoImagen;
    
    // Tomo como referencia el minimo de las escalas
    double fEscala = Math.min(escalaX, escalaY);
    
    // Devuelvo el resultado de aplicar esa escala a la imagen
    return escalar(fEscala, imagen);
  }
  
  /**
   * Escala una imagen en porcentaje.
   * 
   * @param factorEscala
   *          ejemplo: factorEscala=0.6 (escala la imagen al 60%)
   * @param srcImg
   *          una imagen BufferedImage
   * @return un BufferedImage escalado
   */
  public BufferedImage escalar(final double factorEscala,
      final BufferedImage srcImg)
  {
    // Comprobacion de parametros
    if (srcImg == null)
      return null;
    // Compruebo escala nula
    if (factorEscala == 1)
      return srcImg;
    
    // La creo con esas opciones
    AffineTransformOp op = new AffineTransformOp(
        AffineTransform.getScaleInstance(factorEscala, factorEscala), opciones);
    // Devuelve el resultado de aplicar el filtro sobre la imagen
    return op.filter(srcImg, null);
  }
  
  public BufferedImage bytesToBufferedImage(final byte[] bArray,
      final String formato) throws IOException
  {
    BufferedImage iBuffer = null;
    if (formatos.contains(formato))
    {
      InputStream is = new ByteArrayInputStream(bArray);
      iBuffer = ImageIO.read(is);
    }
    return iBuffer;
  }
  
  public byte[] getImgBytes(final Image image) throws IOException
  {
    return bufferedImgToBytes(getBufferedImage(image), "bmp");
  }
  
  public byte[] bufferedImgToBytes(final BufferedImage bImage,
      final String formato) throws IOException
  {
    byte[] bArray = null;
    if (formatos.contains(formato))
    {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ImageIO.write(bImage, formato, baos);
      bArray = baos.toByteArray();
      baos.close();
    }
    return bArray;
  }
  
  /** Retorna el Buffer de la imagen */
  private BufferedImage getBufferedImage(final Image imgData)
  {
    BufferedImage bufferedImage = new BufferedImage(imgData.getWidth(null),
        imgData.getHeight(null), BufferedImage.TYPE_INT_RGB);
    bufferedImage.getGraphics().drawImage(imgData, 0, 0, null);
    return bufferedImage;
  }
  
  /**
   * Metodo que guarda una imagen en disco
   * 
   * @param imagen
   *          Imagen a almacenar en disco
   * @param rutaFichero
   *          Ruta de la imagen donde vamos a salvar la imagen
   * @param formato
   *          Formato de la imagen al almacenarla en disco
   * @return Booleano indicando si se consiguio salvar con exito la imagen
   * @throws IOException
   */
  public boolean saveImage(final BufferedImage imagen,
      final String rutaFichero, final String formato) throws IOException
  {
    boolean success = false;
    if (imagen != null && rutaFichero != null && formato != null)
    {
      if(formatos.contains(formato))
      {
        File file = new File(rutaFichero + "." + formato);
        success = ImageIO.write(imagen, formato, file);
      }
    }
    else
      success = false;
    return success;
  }
}
