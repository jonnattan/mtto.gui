package cl.jonnattan.mtto.photo;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class RealPhoto extends JDialog
{
  private static final long serialVersionUID = 1L;
  private JLabel            lblFoto          = null;
  
  public RealPhoto()
  {
    super();
    setAlwaysOnTop(true);
    setTitle("Imagen Original");
    setResizable(true);
    lblFoto = new JLabel("");
    lblFoto.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblFoto, BorderLayout.CENTER);
  }
  
  public void loadFoto(final BufferedImage bImage)
  {
    lblFoto.setIcon(new ImageIcon(bImage));
    int size = bImage.getHeight();
    if (bImage.getWidth() > size)
      size = bImage.getWidth();
    setSize(size + 50, size + 50);
    setPreferredSize(getSize());
    setMaximumSize(getSize());
    setMinimumSize(getSize());
    setVisible(true);
  }
}
