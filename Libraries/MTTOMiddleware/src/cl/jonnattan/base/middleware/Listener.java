/*
 * <p>Copyright: Copyright (c) 2004</p> <p>Company:   Ltd.</p> Created on Mar 26, 2004
 */
package cl.jonnattan.base.middleware;

/**
 * Interfaz generica que debe implementarse para atender callbacks.
 * 
 * @author ayachan
 */
public interface Listener
{
    /**
     * Recibe el objeto fruto del callback.
     * 
     * @param source Origen del callback.
     * @param obj Objecto asociado.
     */
    public void handle( Object source, Object obj );
}
