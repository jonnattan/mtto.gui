/**
 * 20/07/2019 - ayachan
 */
package cl.jonnattan.base.middleware.queue;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Define una cola de data con atributos, implementada utilizando un <code>LinkedList</code>. Los atributos definen
 * características de actualización de la lista en la adiciones.
 * 
 * @param <T> tipo de la cola.
 * @author ayachan
 */
public class DataQueue<T>
{
    /**
     * Lista que contiene los elementos.
     */
    private final LinkedList<QueuedData<T>> queue; // NOPMD

    /**
     * Clase que contiene la informacion.
     * 
     * @author eosorio
     * @param <M> Tipo que soporta la clase.
     */
    protected class QueuedData<M>
    {
        /**
         * Tipo de la clase.
         */
        private final M                     obj;
        /**
         * Viegencia del dato.
         */
        private final DataAttribute.Vigency latency;

        /**
         * Constructor de la clase QueuedData.
         * 
         * @param obj Objeto que va estar almacenado.
         * @param latency Viegencia del elemento.
         */
        public QueuedData( final M obj, final DataAttribute.Vigency latency )
        {
            this.obj = obj;
            this.latency = latency;
        }

        /**
         * Obtiene el valor contenido.
         * 
         * @return Objeto contenido.
         */
        public M getData()
        {
            return this.obj;
        }

        /**
         * Obtiene la vigencia del elemento.
         * 
         * @return Valor de la vigencia.
         */
        public DataAttribute.Vigency getLatency()
        {
            return this.latency;
        }

        /**
         * {@inheritDoc}
         */
        @SuppressWarnings( "unchecked" )
        @Override
        public boolean equals( final Object other )
        {
            boolean result = false;
            if ( other instanceof QueuedData )
            {
                result = this.obj.equals( ( (QueuedData<M>) other ).obj );
            }
            return result;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode()
        {
            return this.obj.hashCode();
        }
    }

    /**
     * Constructor for DataQueue.
     */
    public DataQueue()
    {
        this.queue = new LinkedList<QueuedData<T>>();
    }

    /**
     * If the unicity condition is multiple, just add the incoming event, if the condition is unique, replace the
     * existing if exist, and if the condition is single, then post the incoming event only if another doesnt exist
     * already.
     * 
     * @param obj Elemento a postear.
     * @param latency Tiempo de vigencia.
     * @param unicity Tipo de unicidad.
     */
    public synchronized void post( final T obj, final DataAttribute.Vigency latency, final DataAttribute.Unicity unicity )
    {
        final QueuedData<T> qd = new QueuedData<T>( obj, latency );

        if ( unicity == DataAttribute.Unicity.MULTIPLE )
        {
            // doesn't care, just add it
            this.queue.add( qd );
        }
        else
        {
            final int index = this.queue.indexOf( qd );
            if ( index >= 0 )
            {
                // an event already exist
                if ( unicity == DataAttribute.Unicity.UNIQUE )
                {
                    // replace the existing one
                    this.queue.set( index, qd );
                }
            }
            else
            {
                // no event exist already: either unique or single we can post it under
                // the event uniqueness protocol
                this.queue.add( qd );
            }
        }
    }

    /**
     * Obtiene primer elemento de la lista.
     * 
     * @return Elemento obtenido de la lista.
     */
    public synchronized T pop()
    {
        T obj = null;
        if ( !this.queue.isEmpty() )
        {
            QueuedData<T> qe = this.queue.removeFirst();
            while ( ( qe != null ) && ( qe.getLatency().isExpired() ) )
            {
                qe = this.queue.removeFirst();
            }

            if ( qe != null )
            {
                obj = qe.getData();
            }
        }
        return obj;
    }

    /**
     * Obtiene el tamnno de la cola.
     * 
     * @return Valor del tamanno de la cola.
     */
    public synchronized int size()
    {
        clean();
        return this.queue.size();
    }

    /**
     * Iterate over the list removing all expired messages, returning the number of removed messages.
     * 
     * @return Numero de elementos borrado.
     */
    public synchronized int clean()
    {
        int removed = 0;

        final Iterator<QueuedData<T>> it = this.queue.iterator();
        while ( it.hasNext() )
        {
            final QueuedData<T> qe = it.next();
            if ( qe.getLatency().isExpired() )
            {
                it.remove();
                ++removed;
            }
        }

        return removed;
    }

}
