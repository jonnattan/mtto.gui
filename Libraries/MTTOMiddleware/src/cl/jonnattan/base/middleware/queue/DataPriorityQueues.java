/**
 * 20/07/2019 - ayachan
 */
package cl.jonnattan.base.middleware.queue;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implement a priority queue using queues for each priority as defined in <code>DataAttribute.Priority</code>.
 * <p>
 * This implementation use a check list in order to poll the queues in a way that a queue with a given priority will be
 * quest double than that with the lower priority.
 * </p>
 * <p>
 * 20/07/2019<br>
 * Modified so pop operation can wait for an element or return null if none.
 * </p>
 * 
 * @param <T> tipo soportado por la cola.
 * @author ayachan
 */
public class DataPriorityQueues<T> extends Queue<T>
{
    /**
     * Arerglo de colas de datos.
     */
    private DataQueue<T>[]  queues          = null;
    /**
     * Arreglo de indice de ciclo de cada cola.
     */
    private int[]           queueIndexCycle = null;
    /**
     * Indice de ciclo.
     */
    private int             cycleIndex      = 0;

    /**
     * Number of elements in the queue; faster than counting each queue in sequence.
     */
    private int             size            = 0;

    /**
     * Lock de las colas.
     */
    private final Lock      lock            = new ReentrantLock();
    /**
     * Condicion de lock.
     */
    private final Condition queueSignal     = this.lock.newCondition();

    /**
     * Rango de prioridades.
     */
    private final int       priorityRange   = DataAttribute.Priority.values().length;

    /**
     * Constructor for DataPriorityQueues.
     */
    @SuppressWarnings( "unchecked" )
    public DataPriorityQueues()
    {
        this.queues = new DataQueue[ this.priorityRange ];
        for ( int i = 0; i < this.priorityRange; i++ )
        {
            this.queues[ i ] = new DataQueue<T>();
        }

        // generate index list
        generateCycleIndex();
    }

    /**
     * Recursive index list builder.
     * 
     * @param index Valor del indice.
     * @param from Valor inicial.
     * @return el valor del indice.
     */
    private int generate( final int index, final int from )
    {
        int workingIndex = index;
        if ( from < this.priorityRange )
        {
            // store that index
            this.queueIndexCycle[ workingIndex ] = from;
            ++workingIndex;

            // fill the following
            for ( int i = 0; i < 2; i++ )
            {
                workingIndex = generate( workingIndex, from + 1 );
            }
        }
        return workingIndex;
    }

    /**
     * Generate the index list. This happens only once.
     */
    protected final void generateCycleIndex()
    {
        // The size of the list
        int qsize = 0;
        int exp = 1;
        for ( int i = 0; i < this.priorityRange; i++ )
        {
            qsize += exp;
            exp <<= 1;
        }
        this.queueIndexCycle = new int[ qsize ];

        // fill the list (using a recursive algorithm)
        generate( 0, 0 );

        // index initialization
        this.cycleIndex = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized int size()
    {
        return this.size;
        // int queuesSize = 0;
        // for (int i = 0; i < queues.length; i++)
        // {
        // queuesSize += queues[i].size();
        // }
        // return queuesSize;
    }

    /**
     * Extrae el siguiente elemento.
     * 
     * @return Elemento de la cola.
     */
    private T extractNext()
    {
        if ( this.size == 0 )
        {
            return null;
        }

        final int lastCycleIndex = this.cycleIndex;

        T obj = this.queues[ this.cycleIndex ].pop();
        this.cycleIndex = ( this.cycleIndex + 1 ) % this.queues.length;
        while ( ( obj == null ) && ( this.cycleIndex != lastCycleIndex ) )
        {
            obj = this.queues[ this.cycleIndex ].pop();
            this.cycleIndex = ( this.cycleIndex + 1 ) % this.queues.length;
        }
        return obj;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized T pop()
    {
        return extractNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized T poll( final long time, final TimeUnit unit ) throws InterruptedException
    {
        T obj = null;
        this.lock.lock();
        try
        {
            boolean success = true;
            if ( this.size == 0 )
            {
                success = this.queueSignal.await( time, unit );
            }
            if ( success && ( this.size > 0 ) )
            {
                obj = extractNext();
            }
        }
        finally
        {
            this.lock.unlock();
        }

        if ( obj != null )
        {
            this.size -= 1;
        }
        return obj;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void post( final T obj, final DataAttribute.Priority priority, final DataAttribute.Vigency latency,
            final DataAttribute.Unicity unicity )
    {
        this.lock.lock();
        try
        {
            this.queues[ priority.ordinal() ].post( obj, latency, unicity );
            this.size += 1;
            this.queueSignal.signal();
        }
        finally
        {
            this.lock.unlock();
        }
    }
}
