/**
 * 20/07/2019 - ayachan
 */
package cl.jonnattan.base.middleware.queue;

/**
 * Define los atributos de un bloque de data para efectos de su transferencia.
 * 
 * @author ayachan
 */
public interface DataAttribute
{
    /**
     * Priority of the message. It could be LOW, NORMAL or HIGH. NORMAL messages are dispatched double time LOW
     * messages. HIGH priority messages are dispatched double time NORMAL messages. By default, messages has NORMAL
     * priority.
     */
    static enum Priority
    {
        /** Prioridad baja. */
        LOW,
        /** Prioridad normal. */
        NORMAL,
        /** Prioridad alta. */
        HIGH
    };

    /**
     * Unicity of the message: the condition of existence of more than one event of the same type in the message queue.
     * By default, each message of a given type replace the last that already exist if any. Setting the unicity to
     * false, provoque the coexistence of two or more of same-type events at the same time. A new mode is added; it
     * applyes when the event must not be established if any other event of the same type already exist. So now this
     * class has not a binary state (unique or not) so a ternary state instead: unique, multiple and just-if-single (or
     * just "single", for short).
     */
    static enum Unicity
    {
        /** Mensaje unico, un nuevo mensaje del mismo tipo reemplaza el existente. */
        UNIQUE,
        /** Mensaje multiple, permite la existencia de mas de un mismo tipo de mensaje. */
        MULTIPLE,
        /** Mensaje single, solo ingresa un mensaje del tipo, el resto es descartado. */
        SINGLE
    };

    /**
     * Valor que indica que la vigencia es siempre.
     */
    long    VIGENCY_NONE = 0;

    /**
     * Viegencia permanente.
     */
    Vigency VIGENCYNONE  = new Vigency( DataAttribute.VIGENCY_NONE );

    /**
     * Vigency of an message: the time that the message is valid and is dispatched. It could be NONE (by default): just
     * dispatched at the moment not caring about the time that this happens, and also any elapsed time after the message
     * became invalid and not dispatched.
     */
    public static class Vigency
    {
        /**
         * Tiempo de vigencia.
         */
        private long vigencyMs;

        /**
         * Constructor de la clase Vigency.
         */
        public Vigency()
        {
            this.vigencyMs = DataAttribute.VIGENCY_NONE;
        }

        /**
         * Constructor de la clase Vigency.
         * 
         * @param elapseMs Tiempo transcurrido.
         */
        public Vigency( final long elapseMs )
        {
            if ( elapseMs == DataAttribute.VIGENCY_NONE )
            {
                this.vigencyMs = DataAttribute.VIGENCY_NONE;
            }
            else if ( elapseMs > 0 )
            {
                this.vigencyMs = System.currentTimeMillis() + elapseMs;
            }
            else
            {
                this.vigencyMs = DataAttribute.VIGENCY_NONE;
            }
        }

        /**
         * Obtiene el valor del estado, expirado.
         * 
         * @return Verdadero cuando el valor de la vigencia es igual o superior al tiempo actual.
         */
        public boolean isExpired()
        {
            if ( this.vigencyMs == DataAttribute.VIGENCY_NONE )
            {
                return false;
            }
            else
            {
                return ( this.vigencyMs < System.currentTimeMillis() );
            }
        }

        /**
         * Obtiene el tiempo de vigencia.
         * 
         * @return Valor del tiempo en mili segundos hasta cuando el atributo es vigente.
         */
        public long getVigency()
        {
            return this.vigencyMs;
        }

    }

}
