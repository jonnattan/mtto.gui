package cl.jonnattan.base.middleware.queue;

import java.util.concurrent.TimeUnit;

/**
 * Define la interfaz de una cola donde los elementos son bloques de data con atributos.
 * 
 * @author ayachan
 */
public abstract class Queue<T>
{

    /**
     * @return The number of elements at the queue.
     */
    abstract public int size();

    /**
     * @return The next element available in the queue.
     */
    public abstract T pop();

    public abstract T poll( long timeout, TimeUnit timeUnit ) throws InterruptedException;

    /**
     * Post a message with default priority, latency and unicity.
     */
    public void post( final T obj )
    {
        post( obj, DataAttribute.Priority.NORMAL, DataAttribute.VIGENCYNONE, DataAttribute.Unicity.UNIQUE );
    }

    /**
     * Post a message with given priority and default latency and unicity.
     */
    public void post( final T obj, final DataAttribute.Priority priority )
    {
        post( obj, priority, DataAttribute.VIGENCYNONE, DataAttribute.Unicity.UNIQUE );
    }

    /**
     * Post a message with given latency and default priority and unicity.
     */
    public void post( final T obj, final DataAttribute.Vigency latency )
    {
        post( obj, DataAttribute.Priority.NORMAL, latency, DataAttribute.Unicity.UNIQUE );
    }

    /**
     * Post a message with given unicity and default priority and latency.
     */
    public void post( final T obj, final DataAttribute.Unicity unicity )
    {
        post( obj, DataAttribute.Priority.NORMAL, DataAttribute.VIGENCYNONE, unicity );
    }

    /**
     * Post a message with the given priority, latency and unicity.
     */
    abstract public void post( T obj, DataAttribute.Priority priority, DataAttribute.Vigency latency,
            DataAttribute.Unicity unicity );
}
