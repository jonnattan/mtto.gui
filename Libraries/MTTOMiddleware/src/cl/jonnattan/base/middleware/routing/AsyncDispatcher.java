package cl.jonnattan.base.middleware.routing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.base.middleware.queue.DataAttribute;
import cl.jonnattan.base.middleware.queue.DataPriorityQueues;

/**
 * Event decoupler service that route objects between subscribers.
 * 
 * @author ayachan
 * @param <T> Clase asociado al generico.
 */
public class AsyncDispatcher<T> implements Dispatcher<T>
{
    /**
     * Instancia del despachador.
     */
    private static AsyncDispatcher<?> instance      = null;
    /**
     * Lock para la instanciacion de la clase.
     */
    private static final Lock         INSTANCE_LOCK = new ReentrantLock();

    /**
     * Mentodo que obtiene instancia unica de la clase.
     * 
     * @param <R> Clase asociado al generico.
     * @return Instancia unica de la clase.
     */
    @SuppressWarnings( "unchecked" )
    public static <R> AsyncDispatcher<R> getDispatcher()
    {
        if ( AsyncDispatcher.instance == null )
        {
            AsyncDispatcher.INSTANCE_LOCK.lock();
            try
            {
                if ( AsyncDispatcher.instance == null )
                {
                    AsyncDispatcher.instance = new AsyncDispatcher<R>();
                }
            }
            finally
            {
                AsyncDispatcher.INSTANCE_LOCK.unlock();
            }
        }
        return (AsyncDispatcher<R>) AsyncDispatcher.instance;
    }

    protected Map<Listener, SubscriberHost<T>> subscribers     = null;
    protected final Lock                       listLock        = new ReentrantLock();

    private QueueReader                        reader          = null;

    private ExecutorService                    executorService = null;

    protected DataPriorityQueues<T>            queues          = null;

    /**
     * Host of each subscriber.
     * 
     * @author Alejandro
     */
    protected class SubscriberHost<P> implements Runnable
    {
        static private final long time_ms  = 500;

        private Listener          listener = null;
        private BlockingQueue<P>  queue    = null;
        boolean                   running  = false;

        public SubscriberHost( final Listener listener )
        {
            this.listener = listener;
            this.queue = new LinkedBlockingQueue<P>();
        }

        public void stop()
        {
            this.running = false;
        }

        public void put( final P obj ) throws InterruptedException
        {
            this.queue.put( obj );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run()
        {
            this.running = true;
            while ( this.running )
            {
                try
                {
                    // get data if available and dispatch to subscriber
                    final P obj = this.queue.poll( SubscriberHost.time_ms, TimeUnit.MILLISECONDS );
                    if ( ( obj != null ) && ( this.listener != null ) )
                    {
                        this.listener.handle( this, obj );
                    }
                }
                catch ( final InterruptedException e )
                {
                    // understand that it is time to quit
                    this.running = false;
                }
            }
        }
    }

    /**
     * Task that reads data push'd onto the dispatcher.
     * 
     * @author Alejandro
     */
    protected class QueueReader implements Runnable
    {
        /**
         * Indica si la cola esta activa.
         */
        private boolean running = false;

        /**
         * Detiene la cola.
         */
        public void stop()
        {
            this.running = false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run()
        {
            this.running = true;
            while ( this.running )
            {
                try
                {
                    final T obj = AsyncDispatcher.this.queues.poll( 500, TimeUnit.MILLISECONDS );
                    if ( obj != null )
                    {
                        AsyncDispatcher.this.listLock.lock();
                        try
                        {
                            final Iterator<Listener> itl = AsyncDispatcher.this.subscribers.keySet().iterator();
                            while ( itl.hasNext() )
                            {
                                final SubscriberHost<T> subscriber = AsyncDispatcher.this.subscribers.get( itl.next() );
                                subscriber.put( obj );
                            }
                        }
                        finally
                        {
                            AsyncDispatcher.this.listLock.unlock();
                        }
                    }
                }
                catch ( final InterruptedException e )
                {
                    // understand that it is time to quit
                    this.running = false;
                }
            }
            System.out.println( "QueueReader IS DONE!" );
        }

    }

    /**
     * Constructor de la clase AsyncDispatcher.
     */
    public AsyncDispatcher()
    {
        this.subscribers = new HashMap<Listener, SubscriberHost<T>>();
        this.queues = new DataPriorityQueues<T>();

        this.reader = new QueueReader();

        this.executorService = Executors.newCachedThreadPool();
        this.executorService.execute( this.reader );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void finalize() throws Throwable
    {
        this.executorService.shutdownNow();

        super.finalize();
    }

    /**
     * Tamanno de la cola.
     * 
     * @return El tamanno de la cola.
     */
    public int queueSize()
    {
        return this.queues.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribe( final Listener listener )
    {
        this.listLock.lock();
        try
        {
            // register the subscriber
            final SubscriberHost<T> subscriber = new SubscriberHost<T>( listener );
            this.subscribers.put( listener, subscriber );
            // start his thread
            this.executorService.execute( subscriber );
        }
        finally
        {
            this.listLock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unsubscribe( final Listener listener )
    {
        // fetch the subscription
        if ( this.subscribers.containsKey( listener ) )
        {
            this.listLock.lock();
            try
            {
                final SubscriberHost<T> subscriber = this.subscribers.get( listener );
                if ( subscriber != null )
                {
                    subscriber.stop();
                }
                this.subscribers.remove( listener );
            }
            finally
            {
                this.listLock.unlock();
            }
        }
    }

    /**
     * Add an object to the priority queue.
     * 
     * @param obj Objeto a ser enviado.
     * @param priority Prioridad de envio.
     * @param vigency Tiempo de vigencia.
     * @param unicity Indicador si el mensaje es unico.
     */
    @Override
    public synchronized void post( final T obj, final DataAttribute.Priority priority,
            final DataAttribute.Vigency vigency, final DataAttribute.Unicity unicity )
    {
        // System.out.printf("posting %s\n", obj);
        this.queues.post( obj, priority, vigency, unicity );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void post( final T obj )
    {
        post( obj, DataAttribute.Priority.NORMAL, DataAttribute.VIGENCYNONE, DataAttribute.Unicity.UNIQUE );
    }

}
