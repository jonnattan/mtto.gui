/*
 * <p>Copyright: Copyright (c) 2004</p> <p>Company:   Ltd.</p> Created on Apr 27, 2004
 */
package cl.jonnattan.base.middleware.routing;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.base.middleware.queue.DataAttribute;
import cl.jonnattan.base.middleware.queue.DataPriorityQueues;
import cl.jonnattan.base.middleware.ring.ObjectRingBuffer;

/**
 * <p>
 * Un servicio interno que recibe inscripciones de un número indeterminado de interesados. Cada objeto despachado es
 * redirigido a todos los interesados. Cada interesado debe saber decidir si el mensaje le concierne o no.
 * </p>
 * <p>
 * Para evitar cualquier acoplamiento, tanto en la recepción como en el despacho de los eventos, cada involucrado opera
 * en su propio thread.<br>
 * Por una parte, el emisor inserta sus eventos en una cola simple desde donde son extraídos por el thread del receptor
 * del despachador. Este thread, en su turno, inserta los eventos en la cola - de prioridad - de cada subscriptor, desde
 * son extraídos por el thread asociado a cada uno de ellos.
 * </p>
 * <p>
 * Con lo anterior, se eluden todas las posibilidades de acoplamiento.
 * </p>
 * <p>
 * Normalmente esta clase tiene una única instanciación. Es un singleton.
 * </p>
 * 
 * @author ayachan
 */
public class OldAsyncDispatcher implements Dispatcher<Object>
{
    /**
     * La instanciación única de esta clase.
     */
    private static OldAsyncDispatcher singleDispatcher = null;

    /**
     * El método de recuperación del despachador. Es creado al momento de la primera invocación.
     * 
     * @return
     */
    public static OldAsyncDispatcher getDispatcher()
    {
        if ( OldAsyncDispatcher.singleDispatcher == null )
        {
            OldAsyncDispatcher.singleDispatcher = new OldAsyncDispatcher();
        }
        return OldAsyncDispatcher.singleDispatcher;
    }

    public static void generateAsyncDispatcher( final int ringBufferSize )
    {
        if ( OldAsyncDispatcher.singleDispatcher == null )
        {
            OldAsyncDispatcher.singleDispatcher = new OldAsyncDispatcher( ringBufferSize );
        }
    }

    protected Object           subscriptorsListLock = new Object();

    /**
     * El número máximo de eventos que pueden acumularse en la entrada del despachador.
     */
    public static final int    MAX_EVENTS           = 60;

    /**
     * La cola de entrada del despachador.
     */
    protected ObjectRingBuffer ring                 = null;

    /**
     * El thread que despacha los eventos a cada subscriptor.
     * 
     * @author ayachan
     */
    protected class SubscriptorHost extends DispatcherThreadIntf
    {
        /**
         * El subscriptor al cual hay que evacuar los eventos.
         */
        private Listener                     listener = null;
        /**
         * Cada subscriptor posee su propia cola de eventos.
         */
        protected DataPriorityQueues<Object> queues   = null;

        public SubscriptorHost( final Listener listener )
        {
            this.listener = listener;
            this.queues = new DataPriorityQueues<Object>();
        }

        /*
         * (non-Javadoc)
         * 
         * @see cl.sisdef.middleware.routing.DispatcherThreadIntf#next()
         */
        @Override
        public Object next()
        {
            return this.queues.pop();
        }

        /*
         * (non-Javadoc)
         * 
         * @see cl.sisdef.middleware.routing.DispatcherThreadIntf#dispatch(java.lang. Object)
         */
        @Override
        public void dispatch( final Object obj )
        {
            if ( this.listener != null )
            {
                this.listener.handle( this, obj );
            }
        }
    }

    /**
     * El thread que recoge los eventos de la entrada y los redirecciona a cada anfitrión de subscriptor.
     * 
     * @author ayachan
     */
    protected class Receiver extends DispatcherThreadIntf
    {

        /*
         * (non-Javadoc)
         * 
         * @see cl.sisdef.middleware.routing.DispatcherThreadIntf#next()
         */
        @Override
        public Object next()
        {
            Object obj = null;

            try
            {
                if ( OldAsyncDispatcher.this.ring.available() > 0 )
                {
                    obj = OldAsyncDispatcher.this.ring.pop();
                }
            }
            catch ( final IOException e )
            {
                e.printStackTrace();
            }
            return obj;
        }

        /*
         * (non-Javadoc)
         * 
         * @see cl.sisdef.middleware.routing.DispatcherThreadIntf#dispatch(java.lang. Object)
         */
        @Override
        public void dispatch( final Object obj )
        {
            /*
             * El "despacho", en este caso, consiste en colocar el evento en la cola de prioridad de cada subscriptor.
             */
            synchronized ( OldAsyncDispatcher.this.subscriptorsListLock )
            {
                final Iterator<Listener> it = OldAsyncDispatcher.this.list.keySet().iterator();
                while ( it.hasNext() )
                {
                    final Listener listener = it.next();
                    final SubscriptorHost sh = OldAsyncDispatcher.this.list.get( listener );

                    final InnerEvent ie = (InnerEvent) obj;
                    sh.queues.post( ie.obj, ie.priority, ie.vigency, ie.unicity );
                }
            }
        }

    }

    /**
     * La lista de subscriptores, cada uno con su thread.
     */
    protected HashMap<Listener, SubscriptorHost> list     = null;

    /**
     * El thread que atiende la primera cola.
     */
    private DispatcherThreadIntf                 receiver = null;

    protected OldAsyncDispatcher()
    {
        this( OldAsyncDispatcher.MAX_EVENTS );
    }

    /**
     * Instancia un despachador. Protegido para forzar que sea un <i>singleton</i>.
     */
    protected OldAsyncDispatcher( final int ringBufferSize )
    {
        super();

        /*
         * La cola (circular) de entrada de eventos al despachador.
         */
        this.ring = new ObjectRingBuffer( ringBufferSize );
        /*
         * La lista de subscriptores.
         */
        this.list = new HashMap<Listener, SubscriptorHost>();
        /*
         * El thread de atención.
         */
        this.receiver = new Receiver();
        this.receiver.start();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable
    {
        // detiene su receptor
        this.receiver.stop();
        // detiene los anfitriones de subscriptores
        final Iterator<Listener> it = this.list.keySet().iterator();
        while ( it.hasNext() )
        {
            final Listener listener = it.next();
            final SubscriptorHost sh = this.list.get( listener );

            sh.stop();
        }
    }

    /**
     * Inscribe un interesado a la lista de despacho.
     * 
     * @param listener
     */
    @Override
    public void subscribe( final Listener listener )
    {
        synchronized ( this.subscriptorsListLock )
        {
            // System.out.printf("subscribe(Listener listener)\n");
            final SubscriptorHost sh = new SubscriptorHost( listener );

            final Object previous = this.list.put( listener, sh );
            if ( previous != null )
            {
                final Listener lprv = (Listener) previous;
                System.err.println( "listener " + listener + " already exists as " + lprv );
            }

            // System.out.println("added listener " + listener);
            sh.start();
        }
    }

    /**
     * Des-inscribe un anfitrión de la lista.
     * 
     * @param host
     */
    @Override
    public void unsubscribe( final Listener listener )
    {
        synchronized ( this.subscriptorsListLock )
        {
            final SubscriptorHost sh = this.list.remove( listener );

            if ( sh == null )
            {
                System.err.println( "listener " + listener + " was not subscribed" );

                System.err.println( "  current listeners (" + this.list.size() + ")are:" );
                System.err.print( "    " );
                final Iterator<Listener> it = this.list.keySet().iterator();
                while ( it.hasNext() )
                {
                    final Listener lst = it.next();
                    System.err.print( lst + " " );
                }
                System.err.println();
            }
            else
            {
                sh.stop();
                // System.out.println("removed listener " + listener);
            }
        }
    }

    /**
     * Una clase auxiliar para almacenar los eventos entre que son recibidos e insertados en la cola de cada
     * subscriptor.
     * 
     * @author ayachan
     */
    protected class InnerEvent
    {
        Object                 obj      = null;
        DataAttribute.Priority priority = null;
        DataAttribute.Vigency  vigency  = null;
        DataAttribute.Unicity  unicity  = null;

        public InnerEvent( final Object obj, final DataAttribute.Priority priority,
                final DataAttribute.Vigency vigency, final DataAttribute.Unicity unicity )
        {
            this.obj = obj;
            this.priority = priority;
            this.vigency = vigency;
            this.unicity = unicity;
        }
    }

    /**
     * Agrega un objeto a la cola, de donde será extraído por el despachador, el cual invocará el método dispatch, a
     * implementar por cada tipo de servicio.
     * 
     * @param obj
     * @param priority
     * @param vigency
     * @param unicity
     */
    @Override
    public synchronized void post( final Object obj, final DataAttribute.Priority priority,
            final DataAttribute.Vigency vigency, final DataAttribute.Unicity unicity )
    {
        try
        {
            if ( this.ring.available() == this.ring.getCapacity() )
            {
                this.ring.setCapacity( this.ring.getCapacity() + ( this.ring.getCapacity() / 2 ) );
                System.out.println( "AsyncDispatcher ring buffer incremented." );
            }

            this.ring.push( new InnerEvent( obj, priority, vigency, unicity ) );
        }
        catch ( final IOException e )
        {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void post( final Object obj )
    {
        post( obj, DataAttribute.Priority.NORMAL, DataAttribute.VIGENCYNONE, DataAttribute.Unicity.UNIQUE );
    }

    /**
     * @return El número actual de elementos en la cola de entrada del despachador.
     */
    public int getRingBufferSize()
    {
        int size = 0;
        if ( this.ring != null )
        {
            size = this.ring.available();
        }
        return size;
    }

}
