/*
 * <p>Copyright: Copyright (c) 2004</p> <p>Company:   Ltd.</p> Created on Mar 23, 2004
 */
package cl.jonnattan.base.middleware.routing;

import cl.jonnattan.base.middleware.runnable.MonitoredRunnable;

/**
 * Template de los threads involucrados en el proceso del despachador de eventos.
 * 
 * @author ayachan
 */
abstract public class DispatcherThreadIntf extends MonitoredRunnable
{
    /**
     * sleep between fetchs while exist elements in the queue.
     */
    private static final int SLEEP_BETWEEN = 1;
    /**
     * sleep between fetchs if don't exist elements in the queue.
     */
    private static final int SLEEP_EMPTY   = 10;

    /**
     * @return El próximo evento de la cola o null si no hay ninguno.
     */
    abstract Object next();

    /**
     * El despacho del evento.
     * 
     * @param obj El objeto a despachar.
     */
    abstract void dispatch( Object obj );

    /**
     * Recoge y emite mensajes.
     * 
     * @see cl.sisdef.util.runnable.MonitoredRunnable#perform()
     */
    @Override
    public void perform() throws InterruptedException
    {
        long sleepMS = 0;

        final Object obj = next();
        if ( obj != null )
        {
            dispatch( obj );
            sleepMS = DispatcherThreadIntf.SLEEP_BETWEEN;
        }
        else
        {
            sleepMS = DispatcherThreadIntf.SLEEP_EMPTY;
        }
        Thread.sleep( sleepMS );
    }

}
