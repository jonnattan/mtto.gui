/**
 * Jul 21, 2019 - Alejandro
 */
package cl.jonnattan.base.middleware.routing;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.base.middleware.queue.DataAttribute;

/**
 * @author Alejandro
 */
public interface Dispatcher<T>
{
    void subscribe( Listener listener );

    void unsubscribe( Listener listener );

    void post( T obj );

    void post( T obj, DataAttribute.Priority priority, DataAttribute.Vigency vigency, DataAttribute.Unicity unicity );
}
