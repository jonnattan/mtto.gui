/*
 * Modified on July 5, 2005.
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cl.jonnattan.base.middleware.ring;

/**
 * Define una excepción de overflow de la capacidad de un ring buffer.
 * 
 * @author Alejandro
 */
public class RingBufferOverflowException extends Exception
{
    private static final long serialVersionUID = 2094818259406353935L;

    /**
     * Constructor for RingBufferOverflow.
     */
    public RingBufferOverflowException()
    {
        super();
    }

    /**
     * Constructor for RingBufferOverflow.
     * 
     * @param arg0
     */
    public RingBufferOverflowException( final String arg0 )
    {
        super( arg0 );
    }

    /**
     * Constructor for RingBufferOverflow.
     * 
     * @param arg0
     * @param arg1
     */
    public RingBufferOverflowException( final String arg0, final Throwable arg1 )
    {
        super( arg0, arg1 );
    }

    /**
     * Constructor for RingBufferOverflow.
     * 
     * @param arg0
     */
    public RingBufferOverflowException( final Throwable arg0 )
    {
        super( arg0 );
    }

}
