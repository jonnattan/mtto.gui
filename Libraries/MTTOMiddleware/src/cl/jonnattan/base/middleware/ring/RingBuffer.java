/*
 * Modified on July 5, 2005.
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cl.jonnattan.base.middleware.ring;

import java.io.IOException;

import cl.jonnattan.base.middleware.UtilByte;

/**
 * This class implements a ring buffer of bytes of a given size.
 * 
 * @author Alejandro
 */
public class RingBuffer
{
    /**
     * La cantidad por defecto de elementos del ring.
     */
    static final public int DEFAULT_RING_SIZE = 1024;

    /**
     * Tamaño del ring buffer.
     */
    private int             capacity          = 0;
    /**
     * La memoria del ring buffer.
     */
    private byte[]          buffer            = null;
    /**
     * Número de elementos en el ring buffer.
     */
    private int             size              = 0;

    /**
     * The index of the position in the circular buffer at which the next byte of data will be stored when received.
     */
    private int             in                = 0;
    /**
     * The index of the position in the circular buffer at which the next byte of data will be read.
     */
    private int             out               = 0;

    /**
     * Constructor for RingBuffer.
     */
    public RingBuffer( final int capacity )
    {
        super();
        this.capacity = capacity;
        this.buffer = new byte[ capacity ];
        this.size = 0;
    }

    /**
     * @return Retorna la capacidad de almacenamiento del ring buffer.
     */
    public int getCapacity()
    {
        return this.capacity;
    }

    /**
     * Carga el tamaño del ring buffer, creando uno nuevo del tamaño indicado y copiando la data del antiguo a éste.
     * 
     * @param newcapacity Capacidad del ring buffer expandido.
     * @throws IOException
     */
    public synchronized void setCapacity( final int newcapacity ) throws IOException
    {
        if ( this.capacity != newcapacity )
        {
            final byte[] newbuffer = new byte[ newcapacity ];

            int len = 0;
            for ( int n = 0; ( n < newbuffer.length ) && ( available() > 0 ); n++ )
            {
                newbuffer[ n ] = (byte) pop();
                len++;
            }

            this.out = 0;
            this.capacity = newcapacity;
            this.buffer = newbuffer;

            if ( len <= 0 )
            {
                this.in = 0;
                this.size = 0;
            }
            else
            {
                this.in = len % this.capacity;
                this.size = len;
            }
        }
    }

    /**
     * @return El número de elementos en el ring buffer.
     */
    public int available()
    {
        return this.size;
    }

    /**
     * Agrega un byte al ring buffer.
     * 
     * @param b El byte a agregar.
     * @throws IOException Si se excede la capacidad del buffer.
     */
    public synchronized void push( final int b ) throws IOException
    {
        if ( this.size == this.capacity )
        {
            throw new IOException( "Ring buffer overflow." );
        }
        this.buffer[ this.in ] = (byte) b;
        this.in = ( this.in + 1 ) % this.capacity;
        ++this.size;
    }

    public synchronized void push( final byte[] burst, final int off, final int len ) throws IOException
    {
        for ( int n = off; n < ( off + len ); n++ )
        {
            push( UtilByte.unsignedByte( burst[ n ] ) );
        }
    }

    /**
     * Extrae un byte del buffer.
     * 
     * @return El byte más antiguo del buffer.
     * @throws IOException
     */
    public synchronized int pop() throws IOException
    {
        if ( this.size == 0 )
        {
            throw new IOException( "Ring buffer empty." );
        }
        final byte result = this.buffer[ this.out ];
        this.out = ( this.out + 1 ) % this.capacity;
        --this.size;

        return UtilByte.unsignedByte( result );
    }

}
