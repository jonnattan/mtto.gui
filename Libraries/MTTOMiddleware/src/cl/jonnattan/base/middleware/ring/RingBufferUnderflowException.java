/*
 * Modified on July 5, 2005.
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cl.jonnattan.base.middleware.ring;

/**
 * Define una excepción de underflow de la capacidad de un ring buffer.
 * 
 * @author Alejandro
 */
public class RingBufferUnderflowException extends Exception
{
    private static final long serialVersionUID = -5957141672197230630L;

    /**
     * Constructor for RingBufferUnderflowException.
     */
    public RingBufferUnderflowException()
    {
        super();
    }

    /**
     * Constructor for RingBufferUnderflowException.
     * 
     * @param arg0
     */
    public RingBufferUnderflowException( final String arg0 )
    {
        super( arg0 );
    }

    /**
     * Constructor for RingBufferUnderflowException.
     * 
     * @param arg0
     * @param arg1
     */
    public RingBufferUnderflowException( final String arg0, final Throwable arg1 )
    {
        super( arg0, arg1 );
    }

    /**
     * Constructor for RingBufferUnderflowException.
     * 
     * @param arg0
     */
    public RingBufferUnderflowException( final Throwable arg0 )
    {
        super( arg0 );
    }

}
