/*
 * Modified on July 5, 2005.
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cl.jonnattan.base.middleware.ring;

import java.io.IOException;
import java.io.InputStream;

/**
 * Define un stream de entrada basado en un ring buffer.
 * 
 * @author Alejandro
 */
public class RingBufferInputStream extends InputStream
{
    /**
     * El ring buffer de almacenamiento de este stream.
     */
    protected RingBuffer ringBuffer = null;

    /**
     * Flag cuando se cierra el stream.
     */
    private boolean      closed     = true;

    /**
     * Constructor for RingBufferInputStream.
     */
    public RingBufferInputStream()
    {
        this( RingBuffer.DEFAULT_RING_SIZE );
    }

    /**
     * Constructor que define el tamaño del buffer.
     * 
     * @param capacity Tamaño del ring buffer.
     */
    public RingBufferInputStream( final int capacity )
    {
        super();
        this.ringBuffer = new RingBuffer( capacity );
        this.closed = false;
    }

    /**
     * @return La capacidad de este ring buffer.
     */
    public int getCapacity()
    {
        return this.ringBuffer.getCapacity();
    }

    /**
     * Modifica la capacidad del buffer.
     * 
     * @param newcapacity La nueva capacidad del buffer.
     * @throws IOException
     */
    public synchronized void setCapacity( final int newcapacity ) throws IOException
    {
        this.ringBuffer.setCapacity( newcapacity );
    }

    /**
     * Agrega un elemento al buffer.
     * 
     * @param b El elemento a agregar.
     * @throws IOException
     */
    public void push( final int b ) throws IOException
    {
        this.ringBuffer.push( b );
    }

    /**
     * @return El elemento más antiguo en el buffer.
     * @throws IOException
     */
    public synchronized int pop() throws IOException
    {
        return this.ringBuffer.pop();
    }

    /**
     * @return El número de elementos disponibles en el buffer.
     */
    @Override
    public synchronized int available() throws IOException
    {
        return this.ringBuffer.available();
    }

    /**
     * @see java.io.InputStream#read()
     */
    @Override
    public synchronized int read() throws IOException
    {
        if ( this.closed )
        {
            throw new IOException( "Is already closed." );
        }
        return ( available() > 0 ? pop() & 0xFF : -1 );
    }

    /**
     * Cierra (y libera) el buffer.
     */
    @Override
    public synchronized void close() throws IOException
    {
        this.closed = true;
        this.ringBuffer = null;
    }

}
