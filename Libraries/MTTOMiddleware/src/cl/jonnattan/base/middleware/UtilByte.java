package cl.jonnattan.base.middleware;

/**
 * Utilitario para la operación con bytes.
 * 
 * @author ayachan
 */
public final class UtilByte
{
    /**
     * Constructor de la clase.
     */
    private UtilByte()
    {
        // Se declara privado para ocultar el constructor.
    }

    /**
     * Valor máximo de un byte.
     */
    private static final int MAX_BYTE_VALUE = 256;

    /**
     * Retorna el valor entero sin signo del byte (intr�nsicamente) con signo.
     * 
     * @param value El byte con signo a convertir.
     * @return Un valor entero que representa el byte interpretado sin signo.
     */
    public static int unsignedByte( final byte value )
    {
        int ubyte = value;
        if ( ubyte < 0 )
        {
            ubyte += UtilByte.MAX_BYTE_VALUE;
        }
        return ubyte;
    }

}
