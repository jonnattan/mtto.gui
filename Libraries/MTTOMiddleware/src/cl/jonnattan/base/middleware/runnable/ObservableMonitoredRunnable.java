/*
 * Created on Jul 18, 2005
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cl.jonnattan.base.middleware.runnable;

import java.util.Observable;

/**
 * Implementa un thread monitoreable y observable.
 * 
 * @author ayachan
 * @see MonitoredRunnable
 */
public abstract class ObservableMonitoredRunnable extends Observable implements Runnable
{
    private volatile Thread thisthrd;

    public void start()
    {
        if ( this.thisthrd == null )
        {
            this.thisthrd = new Thread( this );
            this.thisthrd.start();
        }
    }

    public void stop()
    {
        final Thread thrdx = this.thisthrd;
        this.thisthrd = null;
        thrdx.interrupt();
    }

    /**
     * Ejecuta lo que corresponda en el thread.
     */
    abstract public void perform() throws InterruptedException;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run()
    {
        final Thread thisThread = Thread.currentThread();
        while ( this.thisthrd == thisThread )
        {
            try
            {
                /*
                 * ejecuta el ciclo "infinito" del thread
                 */
                perform();

                // una pausa de gentileza
                Thread.sleep( 0 );
            }
            catch ( final InterruptedException e )
            {
            }
        }
    }

}
