/*
 * <p>Copyright: Copyright (c) 2004</p> <p>Company:   Ltd.</p> Created on Apr 16, 2004
 */
package cl.jonnattan.base.middleware.runnable;

/**
 * @author ayachan
 */
public abstract class MonitoredRunnable implements Runnable
{
    private volatile Thread thisthrd;

    public void start()
    {
        if ( this.thisthrd == null )
        {
            this.thisthrd = new Thread( this );
            this.thisthrd.start();
        }
    }

    public void stop()
    {
        final Thread thrdx = this.thisthrd;
        this.thisthrd = null;
        thrdx.interrupt();
    }

    /**
     * @return Returns the thisthrd.
     */
    protected Thread getThisthrd()
    {
        return this.thisthrd;
    }

    /**
     * Ejecuta lo que corresponda en el thread.
     */
    abstract public void perform() throws InterruptedException;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run()
    {
        final Thread thisThread = Thread.currentThread();
        while ( this.thisthrd == thisThread )
        {
            try
            {
                /*
                 * ejecuta el ciclo "infinito" del thread
                 */
                perform();

                // una pausa de gentileza
                Thread.sleep( 1 );
            }
            catch ( final InterruptedException e )
            {
            }
        }
    }

}
