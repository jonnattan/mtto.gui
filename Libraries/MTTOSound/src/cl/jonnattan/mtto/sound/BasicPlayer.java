/*
 * BasicPlayer.
 *
 * JavaZOOM : jlgui@javazoom.net
 *            http://www.javazoom.net
 *
 *-----------------------------------------------------------------------
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as published
 *   by the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 */
package cl.jonnattan.mtto.sound;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

import cl.jonnattan.mtto.interfaces.IPioneroPlayer;

/**
 * BasicPlayer is a threaded simple player class based on JavaSound API. It has
 * been successfully tested under J2SE 1.3.x, 1.4.x and 1.5.x.
 */
public class BasicPlayer implements IPioneroPlayer, Runnable
{
  public static int          EXTERNAL_BUFFER_SIZE    = 4000 * 4;
  public static int          SKIP_INACCURACY_SIZE    = 1200;
  protected Thread           m_thread                = null;
  protected File             m_dataSource;
  protected AudioInputStream m_encodedaudioInputStream;
  protected int              encodedLength           = -1;
  protected AudioInputStream m_audioInputStream;
  protected AudioFileFormat  m_audioFileFormat;
  protected SourceDataLine   m_line;
  protected FloatControl     m_gainControl;
  protected FloatControl     m_panControl;
  protected String           m_mixerName             = null;
  private int                m_lineCurrentBufferSize = -1;
  private int                lineBufferSize          = -1;
  private long               threadSleep             = -1;

  /**
   * These variables are used to distinguish stopped, paused, playing states. We
   * need them to control Thread.
   */
  public static final int    UNKNOWN                 = -1;
  public static final int    PLAYING                 = 0;
  public static final int    PAUSED                  = 1;
  public static final int    STOPPED                 = 2;
  public static final int    OPENED                  = 3;
  public static final int    SEEKING                 = 4;
  private int                m_status                = UNKNOWN;

  /**
   * Constructs a Basic Player.
   */
  public BasicPlayer()
  {
    m_dataSource = null;
    reset();
  }

  protected void reset()
  {
    m_status = UNKNOWN;
    if ( m_audioInputStream != null )
    {
      synchronized (m_audioInputStream)
      {
        closeStream();
      }
    }
    m_audioInputStream = null;
    m_audioFileFormat = null;
    m_encodedaudioInputStream = null;
    encodedLength = -1;
    if ( m_line != null )
    {
      m_line.stop();
      m_line.close();
      m_line = null;
    }
    m_gainControl = null;
    m_panControl = null;
  }

  /**
   * Set SourceDataLine buffer size. It affects audio latency. (the delay
   * between line.write(data) and real sound). Minimum value should be over
   * 10000 bytes.
   * 
   * @param size
   *          -1 means maximum buffer size available.
   */
  public void setLineBufferSize( int size )
  {
    lineBufferSize = size;
  }

  /**
   * Return SourceDataLine buffer size.
   * 
   * @return -1 maximum buffer size.
   */
  public int getLineBufferSize()
  {
    return lineBufferSize;
  }

  /**
   * Return SourceDataLine current buffer size.
   * 
   * @return
   */
  public int getLineCurrentBufferSize()
  {
    return m_lineCurrentBufferSize;
  }

  /**
   * Set thread sleep time. Default is -1 (no sleep time).
   * 
   * @param time
   *          in milliseconds.
   */
  public void setSleepTime( long time )
  {
    threadSleep = time;
  }

  /**
   * Return thread sleep time in milliseconds.
   * 
   * @return -1 means no sleep time.
   */
  public long getSleepTime()
  {
    return threadSleep;
  }

  /**
   * Returns BasicPlayer status.
   * 
   * @return status
   */
  public int getStatus()
  {
    return m_status;
  }

  /**
   * Open file to play.
   */
  public void open( File file )
  {
    if ( file != null )
    {
      m_dataSource = file;
      initAudioInputStream();
    }
  }

  /**
   * Inits AudioInputStream and AudioFileFormat from the data source.
   * 
   * @throws BasicPlayerException
   */
  protected void initAudioInputStream()
  {
    try
    {
      reset();
      initAudioInputStream( m_dataSource );
      createLine();

      m_status = OPENED;

    } catch ( Exception ex )
    {
      ex.printStackTrace();
    }
  }

  /**
   * Inits Audio ressources from file.
   */
  protected void initAudioInputStream( File file )
  {
    try
    {
      m_audioInputStream = AudioSystem.getAudioInputStream( file );
      m_audioFileFormat = AudioSystem.getAudioFileFormat( file );
    } catch ( Exception ex )
    {
      ex.printStackTrace();
    }

  }

  /**
   * Inits Audio ressources from AudioSystem.<br>
   */
  protected void initLine() throws LineUnavailableException
  {
    if ( m_line == null )
      createLine();
    if ( !m_line.isOpen() )
    {
      openLine();
    } else
    {
      AudioFormat lineAudioFormat = m_line.getFormat();
      AudioFormat audioInputStreamFormat = m_audioInputStream == null ? null
          : m_audioInputStream.getFormat();
      if ( !lineAudioFormat.equals( audioInputStreamFormat ) )
      {
        m_line.close();
        openLine();
      }
    }
  }

  /**
   * Inits a DateLine.<br>
   * 
   * We check if the line supports Gain and Pan controls.
   * 
   * From the AudioInputStream, i.e. from the sound file, we fetch information
   * about the format of the audio data. These information include the sampling
   * frequency, the number of channels and the size of the samples. There
   * information are needed to ask JavaSound for a suitable output line for this
   * audio file. Furthermore, we have to give JavaSound a hint about how big the
   * internal buffer for the line should be. Here, we say
   * AudioSystem.NOT_SPECIFIED, signaling that we don't care about the exact
   * size. JavaSound will use some default value for the buffer size.
   */
  protected void createLine() throws LineUnavailableException
  {
    // log.info( "Create Line" );
    if ( m_line == null )
    {
      AudioFormat sourceFormat = m_audioInputStream.getFormat();
      // log.info( "Create Line : Source format : " + sourceFormat.toString() );
      int nSampleSizeInBits = sourceFormat.getSampleSizeInBits();
      if ( nSampleSizeInBits <= 0 )
        nSampleSizeInBits = 16;
      if ( (sourceFormat.getEncoding() == AudioFormat.Encoding.ULAW)
          || (sourceFormat.getEncoding() == AudioFormat.Encoding.ALAW) )
        nSampleSizeInBits = 16;
      if ( nSampleSizeInBits != 8 )
        nSampleSizeInBits = 16;
      AudioFormat targetFormat = new AudioFormat(
          AudioFormat.Encoding.PCM_SIGNED, sourceFormat.getSampleRate(),
          nSampleSizeInBits, sourceFormat.getChannels(),
          sourceFormat.getChannels() * (nSampleSizeInBits / 8),
          sourceFormat.getSampleRate(), false );
      // log.info( "Create Line : Target format: " + targetFormat );
      // Keep a reference on encoded stream to progress notification.
      m_encodedaudioInputStream = m_audioInputStream;
      try
      {
        // Get total length in bytes of the encoded stream.
        encodedLength = m_encodedaudioInputStream.available();
      } catch ( IOException e )
      {
        // log.error( "Cannot get m_encodedaudioInputStream.available()", e );
      }
      // Create decoded stream.
      m_audioInputStream = AudioSystem.getAudioInputStream( targetFormat,
          m_audioInputStream );
      AudioFormat audioFormat = m_audioInputStream.getFormat();
      DataLine.Info info = new DataLine.Info( SourceDataLine.class,
          audioFormat, AudioSystem.NOT_SPECIFIED );
      Mixer mixer = getMixer( m_mixerName );
      if ( mixer != null )
      {
        // log.info( "Mixer : " + mixer.getMixerInfo().toString() );
        m_line = (SourceDataLine) mixer.getLine( info );
      } else
      {
        m_line = (SourceDataLine) AudioSystem.getLine( info );
        m_mixerName = null;
      }
      // log.info( "Line : " + m_line.toString() );
      // log.debug( "Line Info : " + m_line.getLineInfo().toString() );
      // log.debug( "Line AudioFormat: " + m_line.getFormat().toString() );
    }
  }

  /**
   * Opens the line.
   */
  protected void openLine()
  {
    if ( m_line != null )
    {
      AudioFormat audioFormat = m_audioInputStream.getFormat();
      int buffersize = lineBufferSize;
      if ( buffersize <= 0 )
        buffersize = m_line.getBufferSize();
      m_lineCurrentBufferSize = buffersize;
      try
      {
        m_line.open( audioFormat, buffersize );
      } catch ( Exception ex )
      {
        ex.printStackTrace();
      }

      if ( m_line.isControlSupported( FloatControl.Type.MASTER_GAIN ) )
        m_gainControl = (FloatControl) m_line
            .getControl( FloatControl.Type.MASTER_GAIN );
      if ( m_line.isControlSupported( FloatControl.Type.PAN ) )
        m_panControl = (FloatControl) m_line.getControl( FloatControl.Type.PAN );
    }
  }

  /**
   * Stops the playback.<br>
   * 
   * Player Status = STOPPED.<br>
   * Thread should free Audio ressources.
   */
  protected void stopPlayback()
  {
    if ( (m_status == PLAYING) || (m_status == PAUSED) )
    {
      if ( m_line != null )
      {
        m_line.flush();
        m_line.stop();
      }
      m_status = STOPPED;

      synchronized (m_audioInputStream)
      {
        closeStream();
      }

    }
  }

  /**
   * Pauses the playback.<br>
   * 
   * Player Status = PAUSED.
   */
  protected void pausePlayback()
  {
    if ( m_line != null )
    {
      if ( m_status == PLAYING )
      {
        m_line.flush();
        m_line.stop();
        m_status = PAUSED;
      }
    }
  }

  /**
   * Resumes the playback.<br>
   * 
   * Player Status = PLAYING.
   */
  protected void resumePlayback()
  {
    if ( m_line != null )
    {
      if ( m_status == PAUSED )
      {
        m_line.start();
        m_status = PLAYING;

      }
    }
  }

  /**
   * Starts playback.
   */
  protected void startPlayback()
  {
    if ( m_status == STOPPED )
      initAudioInputStream();
    if ( m_status == OPENED )
    {
      if ( !(m_thread == null || !m_thread.isAlive()) )
      {
        int cnt = 0;
        while ( m_status != OPENED )
        {
          try
          {
            if ( m_thread != null )
            {
              cnt++;
              Thread.sleep( 300 );
              if ( cnt > 2 )
              {
                m_thread.interrupt();
              }
            }
          } catch ( InterruptedException e )
          {
            e.printStackTrace();
          }
        }
      }
      // Open SourceDataLine.
      try
      {
        initLine();
      } catch ( Exception e )
      {
        e.printStackTrace();
      }

      m_thread = new Thread( this, "BasicPlayer_Thread" );
      m_thread.start();
      if ( m_line != null )
      {
        m_line.start();
        m_status = PLAYING;

      }
    }
  }

  /**
   * Main loop.
   * 
   * Player Status == STOPPED || SEEKING => End of Thread + Freeing Audio
   * Ressources.<br>
   * Player Status == PLAYING => Audio stream data sent to Audio line.<br>
   * Player Status == PAUSED => Waiting for another status.
   */
  public void run()
  {
    int nBytesRead = 1;
    byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];
    // Lock stream while playing.
    synchronized (m_audioInputStream)
    {
      // Main play/pause loop.
      while ( (nBytesRead != -1) && (m_status != STOPPED)
          && (m_status != SEEKING) && (m_status != UNKNOWN) )
      {
        if ( m_status == PLAYING )
        {
          // Play.
          try
          {
            nBytesRead = m_audioInputStream.read( abData, 0, abData.length );
            if ( nBytesRead >= 0 )
            {
              byte[] pcm = new byte[nBytesRead];
              System.arraycopy( abData, 0, pcm, 0, nBytesRead );
              m_line.write( abData, 0, nBytesRead );
              getEncodedStreamPosition();
              // System.out.println( "Escritos[" + nBytesWritten +
              // "] Faltantes[" + nEncodedBytes +"]");
            }
          } catch ( IOException e )
          {
            m_status = STOPPED;

          }
          // Nice CPU usage.
          if ( threadSleep > 0 )
          {
            try
            {
              Thread.sleep( threadSleep );
            } catch ( InterruptedException e )
            {
              e.printStackTrace();
            }
          }
        } else
        {
          // Pause
          try
          {
            Thread.sleep( 300 );
          } catch ( InterruptedException e )
          {
            e.printStackTrace();
          }
        }
      }
      // Free audio resources.
      if ( m_line != null )
      {
        m_line.drain();
        m_line.stop();
        m_line.close();
        m_line = null;
      }
      closeStream();
    }
    m_status = STOPPED;
  }

  /**
   * Skip bytes in the File inputstream. It will skip N frames matching to
   * bytes, so it will never skip given bytes length exactly.
   * 
   * @param bytes
   * @return value>0 for File and value=0 for URL and InputStream
   * @throws BasicPlayerException
   */
  protected long skipBytes( long bytes )
  {
    long totalSkipped = 0;
    if ( m_dataSource instanceof File )
    {
      int previousStatus = m_status;
      m_status = SEEKING;
      long skipped = 0;
      try
      {
        synchronized (m_audioInputStream)
        {

          initAudioInputStream();
          if ( m_audioInputStream != null )
          {
            while ( totalSkipped < (bytes - SKIP_INACCURACY_SIZE) )
            {
              skipped = m_audioInputStream.skip( bytes - totalSkipped );
              if ( skipped == 0 )
                break;
              totalSkipped = totalSkipped + skipped;
            }
          }
        }

        m_status = OPENED;
        if ( previousStatus == PLAYING )
          startPlayback();
        else if ( previousStatus == PAUSED )
        {
          startPlayback();
          pausePlayback();
        }
      } catch ( IOException e )
      {
        e.printStackTrace();
      }
    }
    return totalSkipped;
  }

  protected int getEncodedStreamPosition()
  {
    int nEncodedBytes = -1;
    if ( m_dataSource instanceof File )
    {
      try
      {
        if ( m_encodedaudioInputStream != null )
        {
          nEncodedBytes = encodedLength - m_encodedaudioInputStream.available();
        }
      } catch ( IOException e )
      {
        e.printStackTrace();
      }
    }
    return nEncodedBytes;
  }

  protected void closeStream()
  {
    try
    {
      if ( m_audioInputStream != null )
      {
        m_audioInputStream.close();

      }
    } catch ( IOException e )
    {
      e.printStackTrace();
    }
  }

  /**
   * Returns true if Gain control is supported.
   */
  public boolean hasGainControl()
  {
    if ( m_gainControl == null )
    {
      // Try to get Gain control again (to support J2SE 1.5)
      if ( (m_line != null)
          && (m_line.isControlSupported( FloatControl.Type.MASTER_GAIN )) )
        m_gainControl = (FloatControl) m_line
            .getControl( FloatControl.Type.MASTER_GAIN );
    }
    return m_gainControl != null;
  }

  /**
   * Returns Gain value.
   */
  public float getGainValue()
  {
    if ( hasGainControl() )
    {
      return m_gainControl.getValue();
    } else
    {
      return 0.0F;
    }
  }

  /**
   * Gets max Gain value.
   */
  public float getMaximumGain()
  {
    if ( hasGainControl() )
    {
      return m_gainControl.getMaximum();
    } else
    {
      return 0.0F;
    }
  }

  /**
   * Gets min Gain value.
   */
  public float getMinimumGain()
  {
    if ( hasGainControl() )
    {
      return m_gainControl.getMinimum();
    } else
    {
      return 0.0F;
    }
  }

  /**
   * Returns true if Pan control is supported.
   */
  public boolean hasPanControl()
  {
    if ( m_panControl == null )
    {
      // Try to get Pan control again (to support J2SE 1.5)
      if ( (m_line != null)
          && (m_line.isControlSupported( FloatControl.Type.PAN )) )
        m_panControl = (FloatControl) m_line.getControl( FloatControl.Type.PAN );
    }
    return m_panControl != null;
  }

  /**
   * Returns Pan precision.
   */
  public float getPrecision()
  {
    if ( hasPanControl() )
    {
      return m_panControl.getPrecision();
    } else
    {
      return 0.0F;
    }
  }

  /**
   * Returns Pan value.
   */
  public float getPan()
  {
    if ( hasPanControl() )
    {
      return m_panControl.getValue();
    } else
    {
      return 0.0F;
    }
  }

  /**
   * Deep copy of a Map.
   * 
   * @param src
   * @return
   */
  /*
   * protected Map deepCopy( Map src ) { HashMap map = new HashMap(); if ( src
   * != null ) { Iterator it = src.keySet().iterator(); while ( it.hasNext() ) {
   * Object key = it.next(); Object value = src.get( key ); map.put( key, value
   * ); } } return map; }
   */

  /**
   * @see cl.jonnattan.mtto.interfaces.IPioneroPlayer#seek(long)
   */
  public long seek( long bytes )
  {
    return skipBytes( bytes );
  }

  /**
   * @see cl.jonnattan.mtto.interfaces.IPioneroPlayer#play()
   */
  public void play()
  {
    startPlayback();
    setGain( 1.0 );
    setPan( 0.0 );
  }

  /**
   * @see cl.jonnattan.mtto.interfaces.IPioneroPlayer#stop()
   */
  public void stop()
  {
    stopPlayback();
  }

  /**
   * @see cl.jonnattan.mtto.interfaces.IPioneroPlayer#pause()
   */
  public void pause()
  {
    pausePlayback();
  }

  /**
   * @see cl.jonnattan.mtto.interfaces.IPioneroPlayer#resume()
   */
  public void resume()
  {
    resumePlayback();
  }

  /**
   * Sets Pan value. Line should be opened before calling this method. Linear
   * scale : -1.0 <--> +1.0
   */
  public void setPan( double fPan )
  {
    if ( hasPanControl() )
    {

      m_panControl.setValue( (float) fPan );

    }
  }

  /**
   * Sets Gain value. Line should be opened before calling this method. Linear
   * scale 0.0 <--> 1.0 Threshold Coef. : 1/2 to avoid saturation.
   */
  public void setGain( double fGain )
  {
    if ( hasGainControl() )
    {
      double minGainDB = getMinimumGain();
      double ampGainDB = ((10.0f / 20.0f) * getMaximumGain())
          - getMinimumGain();
      double cste = Math.log( 10.0 ) / 20;
      double valueDB = minGainDB + (1 / cste)
          * Math.log( 1 + (Math.exp( cste * ampGainDB ) - 1) * fGain );
      m_gainControl.setValue( (float) valueDB );

    }
  }

  /*
   * public List<Object> getMixers() { ArrayList<Object> mixers = new
   * ArrayList<Object>(); Mixer.Info[] mInfos = AudioSystem.getMixerInfo(); if (
   * mInfos != null ) { for ( int i = 0; i < mInfos.length; i++ ) { Line.Info
   * lineInfo = new Line.Info( SourceDataLine.class ); Mixer mixer =
   * AudioSystem.getMixer( mInfos[i] ); if ( mixer.isLineSupported( lineInfo ) )
   * { mixers.add( mInfos[i].getName() ); } } } return mixers; }
   */

  public Mixer getMixer( String name )
  {
    Mixer mixer = null;
    if ( name != null )
    {
      Mixer.Info[] mInfos = AudioSystem.getMixerInfo();
      if ( mInfos != null )
      {
        for ( int i = 0; i < mInfos.length; i++ )
        {
          if ( mInfos[i].getName().equals( name ) )
          {
            mixer = AudioSystem.getMixer( mInfos[i] );
            break;
          }
        }
      }
    }
    return mixer;
  }

  public String getMixerName()
  {
    return m_mixerName;
  }

  public void setMixerName( String name )
  {
    m_mixerName = name;
  }
}
