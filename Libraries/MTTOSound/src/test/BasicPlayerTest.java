package test;
/*
 * BasicPlayerTest.
 * 
 * JavaZOOM : jlgui@javazoom.net
 *            http://www.javazoom.net
 *
 *-----------------------------------------------------------------------
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as published
 *   by the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 */


import java.io.File;

import cl.jonnattan.mtto.interfaces.IPioneroPlayer;
import cl.jonnattan.mtto.sound.BasicPlayer;
/**
 * This class implements a simple player based on BasicPlayer.
 * BasicPlayer is a threaded class providing most features
 * of a music player. BasicPlayer works with underlying JavaSound 
 * SPIs to support multiple audio formats. Basically JavaSound supports
 * WAV, AU, AIFF audio formats. Add MP3 SPI (from JavaZOOM) and Vorbis
 * SPI( from JavaZOOM) in your CLASSPATH to play MP3 and Ogg Vorbis file.
 */
public class BasicPlayerTest
{
	/**
	 * Entry point.
	 * @param args filename to play.
	 */
	public static void main(String[] args)
	{
		BasicPlayerTest test = new BasicPlayerTest();
		test.play("P:\\Linux\\Java\\Resources\\conected.wav");		
	}
	
	/**
	 * Contructor.
	 */
	public BasicPlayerTest()
	{
		
	}

	public void play(String filename)
	{
		// Instantiate BasicPlayer.
		BasicPlayer player = new BasicPlayer();
		// BasicPlayer is a BasicController.
		IPioneroPlayer control = (IPioneroPlayer) player;
		// Register BasicPlayerTest to BasicPlayerListener events.
		// It means that this object will be notified on BasicPlayer
		// events such as : opened(...), progress(...), stateUpdated(...)
			
			// Open file, or URL or Stream (shoutcast) to play.
			control.open( new File(filename) );
			// control.open(new URL("http://yourshoutcastserver.com:8000"));
			control.setGain(1.0);
            // setPan should be called after control.play().
            control.setPan(0.0);
            
			control.play();
			
	}
		
	
}
