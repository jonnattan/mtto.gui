package cl.jonnattan.mtto.factory;

import cl.jonnattan.base.middleware.routing.AsyncDispatcher;
import cl.jonnattan.mtto.implementations.Identifier;
import cl.jonnattan.mtto.implementations.ImageProvider;
import cl.jonnattan.mtto.implementations.Parameters;
import cl.jonnattan.mtto.implementations.PioneroSound;
import cl.jonnattan.mtto.interfaces.IClassIdentifier;
import cl.jonnattan.mtto.interfaces.IImageProvider;
import cl.jonnattan.mtto.interfaces.IMessage;
import cl.jonnattan.mtto.interfaces.IParameters;
import cl.jonnattan.mtto.interfaces.ISound;

/**
 * Fabrica de objetos del sistema SCC
 * 
 * @author Jonnattan Griffiths
 * @since 02/08/2019
 * @version 1.0 Copyright(c)
 */
public final class Factories
{
  private static AsyncDispatcher<IMessage> dispatcher    = null;
  private static IImageProvider            imageProvider = null;
  private static IParameters               parameters    = null;
  private static IClassIdentifier          identifier    = null;
  private static ISound                    sounds        = null;

  private Factories()
  {
  }

  /** Reproduce los sonidos del Sistema */
  public static ISound getSounds()
  {
    if (Factories.sounds == null) {
      Factories.sounds = new PioneroSound();
    }
    return Factories.sounds;
  }

  /** Recupera nombre del topico de y otros */
  public static IClassIdentifier getHelpClass()
  {
    if (Factories.identifier == null) {
      Factories.identifier = new Identifier();
    }
    return Factories.identifier;
  }

  /** Obtiene la conexion con la base de datos */
  public static IParameters getParameters()
  {
    if (Factories.parameters == null) {
      Factories.parameters = new Parameters();
    }
    return Factories.parameters;
  }

  /** Se trae el dispatcher */
  public static AsyncDispatcher<IMessage> getDispatcher()
  {
    if (Factories.dispatcher == null) {
      Factories.dispatcher = new AsyncDispatcher<IMessage>();
    }
    return Factories.dispatcher;
  }

  /** Proveedor de Imagenes */
  public static IImageProvider getImageProvider()
  {
    if (Factories.imageProvider == null) {
      Factories.imageProvider = new ImageProvider();
    }
    return Factories.imageProvider;
  }
}
