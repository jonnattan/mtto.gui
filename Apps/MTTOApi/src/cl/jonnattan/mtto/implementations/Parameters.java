package cl.jonnattan.mtto.implementations;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import cl.jonnattan.mtto.interfaces.IParameters;
import cl.jonnattan.mtto.util.CONSTConst;

/**
 * Implementa la lectura de parametros
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class Parameters implements IParameters
{
  private final String  NAMEFILE   = "mtto.conf";
  private String        ipServer   = "SERVER_ADDRESS";
  private String        portServer = "SERVER_PORT";
  private String        ipLogger   = "LOGGER_ADDRESS";
  private String        portLogger = "LOGGER_PORT";
  private String        posX       = "POSITION_X";
  private String        posY       = "POSITION_Y";
  private String        height     = "HEIGHT";
  private String        width      = "WIDTH";
  private String        resources  = "PATH_RESOURCES";
  private String        logPath    = "PATH_LOGS";
  private String        timer    = "TIMER_REQUEST_SEG";
  private String        iniSever = "RUN_SERVER_HERE";
  
  private Properties    properties = null;
  private static Logger logger     = Logger.getGlobal();
  
  private void setDefaultParameters()
  {
    properties.setProperty(ipServer, "localhost");
    properties.setProperty(portServer, "4506");
    properties.setProperty(ipLogger, "localhost");
    properties.setProperty(portLogger, "4509");
    properties.setProperty(posX, "20");
    properties.setProperty(posY, "20");
    properties.setProperty(height, "600");
    properties.setProperty(width, "1000");
    properties.setProperty(resources, ".\\resources");
    properties.setProperty(logPath, ".\\logs");
    properties.setProperty(timer, "30");
    properties.setProperty(iniSever, "YES");
  }
  
  public Parameters()
  {
    properties = new Properties();
    readParameters();
  }
  
  /** Guarda las unidades con claves actualizads */
  public void saveParameter()
  {
    String comments = "Generado Automaticamente por MAC";
    try
    {
      FileOutputStream out = new FileOutputStream(NAMEFILE);
      properties.store(out, comments);
      out.flush();
      out.close();
    }
    catch (IOException ex)
    {
      logger.severe(ex.getMessage());
    }
  }
  
  public void readParameters()
  {
    File file = new File(NAMEFILE);
    try
    {
      if (file.exists())
      {
        logger.info("Lee Archivo Configuracion: " + file.getName());
        properties.load(new FileInputStream(file));
      }
      else
      {
        if (file.createNewFile())
        {
          logger.info("Crea Archivo Configuracion: " + file.getName());
          setDefaultParameters();
          saveParameter();
        }
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  
  @Override
  public boolean initServer()
  {
    boolean resp = properties.getProperty(iniSever).equals("YES");
    return resp;
  }
  
  @Override
  public int getPortDebug()
  {
    return getInt(portLogger);
  }
  
  @Override
  public long getTimerQuestion()
  {
    return (long) getInt(timer);
  }
  
  @Override
  public String getIpDebug()
  {
    return properties.getProperty(ipLogger);
  }
  
  @Override
  public Point getInitialPosition()
  {
    int x = getInt(posX);
    int y = getInt(posY);
    Point point = new Point(x, y);
    return point;
  }
  
  @Override
  public int getPortServer()
  {
    return getInt(portServer);
  }
  
  @Override
  public String getIpServer()
  {
    return properties.getProperty(ipServer);
  }
  
  /**
   * Obtiene el valor numerico del archivo de configuracion
   * 
   * @param key
   * @return valor
   */
  private int getInt(String key)
  {
    int value = 0;
    try
    {
      String aux = properties.getProperty(key);
      if (aux != null)
      {
        value = Integer.parseInt(aux.trim());
      }
      else
      {
        System.err.println("No se encuentra parametro: " + key);
      }
    }
    catch (NumberFormatException ex)
    {
      System.err.println("Error con parametro: " + key);
      // ex.printStackTrace();
    }
    return value;
  }
  
  
  
  @Override
  public String getResourcePath()
  {
    String path = properties.getProperty(resources);
    if(path != null)
    {
      File folder = new File(path);
      if (!folder.exists())
        folder.mkdir();
        
    }
    return path + CONSTConst.barra;
  }
  
  @Override
  public String getLogPath()
  {
    String path = properties.getProperty(logPath);
    if(path != null)
    {
      File folder = new File(path);
      if (!folder.exists())
        folder.mkdir();
    }
    return path + CONSTConst.barra;
  }
  
  @Override
  public String getImagePath()
  {
    String path = CONSTConst.PATH_IMAGE;
    File folder = new File(path);
    if (!folder.exists())
      folder.mkdir();
    return path + CONSTConst.barra;
  }
  
  @Override
  public Dimension getInitialDimension()
  {
    int h = getInt(height);
    int w = getInt(width);
    return new Dimension(w, h);
  }

}
