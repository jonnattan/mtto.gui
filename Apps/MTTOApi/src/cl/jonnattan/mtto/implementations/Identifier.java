package cl.jonnattan.mtto.implementations;

import cl.jonnattan.mtto.interfaces.IClassIdentifier;

/**
 * Implementa la identificacion declases
 * 
 * @author Jonnattan Griffiths
 * @since 26/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class Identifier implements IClassIdentifier {
  
  private enum EClases {
    STATUS_TYPE("cl.sisdef.pionero.data.tcp.EsatAttached"),
    ALARMA_TYPE("cl.sisdef.pionero.data.tcp.EsatAttached");
    
    private String clase = "";
    
    EClases(String clase)
    {
      this.clase = clase;
    }
    
    public String getClase() {
      return clase;
    }
  }
  
  public Identifier()
  {
    super();
  }
  
  @Override
  public byte getIdentifier(Object clase) {
    for (EClases item : EClases.values())
      if (item.getClase().equals(clase))
        return (byte) item.ordinal();
    return -1;
  }
  
  @Override
  public String getClass(int identifier) {
    return EClases.values()[identifier].getClase();
  }
  
}
