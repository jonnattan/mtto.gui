package cl.jonnattan.mtto.implementations;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IImageProvider;
import cl.jonnattan.mtto.util.UtilImages;

/**
 * Implementacion de la interfaz para proveer imagenes
 * 
 * @author Jonnattan Griffiths
 * @since 02/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class ImageProvider implements IImageProvider
{
  private Image        sisdef         = null;
  private Image        imgIcon        = null;
  private Image        imgNaveNone    = null;
  private Image        imgCapNone     = null;
  private Image        imgTriNone     = null;
  private Image        imgDesconocido = null;
  private final String path;
  private final UtilImages util;

  public ImageProvider()
  {
    super();
    path = Factories.getParameters().getResourcePath();
    util = new UtilImages();
  }

  @Override
  public Image getIconSystem()
  {
    if (imgIcon == null)
      imgIcon = Toolkit.getDefaultToolkit().getImage(path + "icono.png");
    return imgIcon;
  }

  @Override
  public Image getImgSystem()
  {
    if (sisdef == null)
    {      
      try {
        BufferedImage deck = ImageIO.read(new File(path + "default.png"));
        BufferedImage bufferedImage = util.escalarATamanyo(deck, 50, 50);
        sisdef = new ImageIcon(bufferedImage).getImage();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return sisdef;
  }

  @Override
  public Image getTripulanteNullImg()
  {
    if (imgTriNone == null)
      imgTriNone = Toolkit.getDefaultToolkit()
          .getImage(path + "Foto_TRIPULACION_SIN_FOTO.bmp");
    return imgTriNone;
  }

  @Override
  public Image getCapitanNullImg()
  {
    if (imgCapNone == null)
      imgCapNone = Toolkit.getDefaultToolkit()
          .getImage(path + "Foto_CAPITAN_SIN_FOTO.bmp");
    return imgCapNone;
  }

  @Override
  public Image getDesconocidoNullImg()
  {
    if (imgDesconocido == null)
      imgDesconocido = Toolkit.getDefaultToolkit()
          .getImage(path + "Foto_NO_FOTO.bmp");
    return imgDesconocido;
  }

  @Override
  public Image getNaveNullImg()
  {
    if (imgNaveNone == null)
      imgNaveNone = Toolkit.getDefaultToolkit()
          .getImage(path + "Foto_NAVE_SIN_FOTO.bmp");
    return imgNaveNone;
  }

}
