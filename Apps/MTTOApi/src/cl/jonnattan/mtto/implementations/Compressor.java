package cl.jonnattan.mtto.implementations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import cl.jonnattan.mtto.interfaces.ICompressor;

/**
 * CImplementacion de la interfaz de compreasion. realizada en base a Zip
 * 
 * @author Jonnattan Griffiths
 * @since 06/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class Compressor implements ICompressor {
  public Compressor()
  {
    super();
  }
  
  @Override
  public ByteBuffer compress(ByteBuffer buffer) {
    ByteBuffer bb = null;
    try {
      byte[] barray = comprimirGZIP(buffer.array());
      bb = ByteBuffer.wrap(barray);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return bb;
  }
  
  @Override
  public ByteBuffer descompress(ByteBuffer buffer) {
    ByteBuffer bb = null;
    try {
      byte[] barray = descomprimirGZIP(buffer.array());
      bb = ByteBuffer.wrap(barray);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return bb;
  }
  
  /**
   * Compresor en base a GZip
   * 
   * @param barray
   * @return
   * @throws IOException
   */
  private byte[] comprimirGZIP(byte[] barray) throws IOException {
    ByteArrayOutputStream gzdata = new ByteArrayOutputStream();
    GZIPOutputStream gzipper = new GZIPOutputStream(gzdata);
    ByteArrayInputStream data = new ByteArrayInputStream(barray);
    byte[] readed = new byte[barray.length];
    int actual = 1;
    while ((actual = data.read(readed)) > 0) {
      gzipper.write(readed, 0, actual);
    }
    gzipper.finish();
    data.close();
    byte[] compressed = gzdata.toByteArray();
    // System.out.println( "Comprime " + barray.length + " bytes a " +
    // compressed.length + " bytes." );
    gzdata.close();
    return compressed;
  }
  
  /**
   * Descompresor en base a GZip
   * 
   * @param barray
   * @return
   * @throws IOException
   */
  private byte[] descomprimirGZIP(byte[] barray) throws IOException {
    ByteArrayInputStream gzdata = new ByteArrayInputStream(barray);
    GZIPInputStream gunzipper = new GZIPInputStream(gzdata, barray.length);
    ByteArrayOutputStream data = new ByteArrayOutputStream();
    byte[] readed = new byte[barray.length * 2];
    int actual = 1;
    while ((actual = gunzipper.read(readed)) > 0) {
      data.write(readed, 0, actual);
    }
    gzdata.close();
    gunzipper.close();
    byte[] returndata = data.toByteArray();
    data.close();
    // System.out.println( "Descomprime " + barray.length + " bytes a " +
    // returndata.length + " bytes." );
    return returndata;
  }
  
}
