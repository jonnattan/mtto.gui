package cl.jonnattan.mtto.implementations;

import java.io.File;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPioneroPlayer;
import cl.jonnattan.mtto.interfaces.ISound;
import cl.jonnattan.mtto.sound.BasicPlayer;

/**
 * Implementacion de la interfaz de sonidos, para mayor info
 * http://www.javazoom.net/jlgui/api.html
 * 
 * @author Jonnattan Griffiths
 * @since 02/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class PioneroSound implements ISound
{
  private IPioneroPlayer errorPlayer    = null;
  private IPioneroPlayer infoPlayer     = null;

  public PioneroSound()
  {
    super();
    configSounds();
  }

  private void configSounds()
  {
    String path = Factories.getParameters().getResourcePath();
    path += System.getProperty( "file.separator" );
    try
    {
      // Ring
      File file = new File( path + "error.wav" );
      if ( file.exists() )
      {
        errorPlayer = new BasicPlayer();
        errorPlayer.open( file );
      }
      // info
      file = new File( path + "info.wav" );
      if ( file.exists() )
      {
        infoPlayer = new BasicPlayer();
        infoPlayer.open( file );
      }

    } catch ( Exception ex )
    {
      ex.printStackTrace();
    }

  }

  @Override
  public void error()
  {
    if ( errorPlayer != null )
      errorPlayer.play();
    else
      System.err.println( "ERROR: No se encuentra archivo de sonido" );
  }

  @Override
  public void info()
  {
    if ( infoPlayer != null )
      infoPlayer.play();
    else
      System.err.println( "ERROR: No se encuentra archivo de sonido" );
  }

}
