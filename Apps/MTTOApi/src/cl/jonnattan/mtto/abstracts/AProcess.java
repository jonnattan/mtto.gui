package cl.jonnattan.mtto.abstracts;

import java.util.concurrent.LinkedBlockingQueue;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IMessage;
import cl.jonnattan.mtto.interfaces.IProcessor;
import cl.jonnattan.mtto.interfaces.IQueueData;

/**
 * Clase Abstracta que se crea como implementacion de la Interfaz que define a
 * los procesos del ESAT
 * 
 * @author Jonnattan Griffiths
 * @since 21/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public abstract class AProcess implements IProcessor
{
  private Thread                          thread = null;
  private boolean                         run    = false;
  private LinkedBlockingQueue<IQueueData> queue  = null;

  public AProcess()
  {
    super();
  }

  @Override
  public void handle( Object source, Object obj )
  {
    if ( obj != null && obj instanceof IMessage )
    {
      receivedInternalMessage( (IMessage) obj );
    }
  }

  public abstract void receivedInternalMessage( IMessage message );

  @Override
  public void setQueue( LinkedBlockingQueue<IQueueData> queue )
  {
    this.queue = queue;
  }

  protected LinkedBlockingQueue<IQueueData> getQueue()
  {
    return this.queue;
  }

  protected void notifyData( IQueueData data )
  {
    if ( getQueue() != null )
    {
       System.err.println("ERROR: No existe Cola en : " + getProccesName() );
    } else
    {
      System.err.println( "Queue is NULL" );
    }
  }

  private synchronized void setRun( boolean run )
  {
    this.run = run;
  }

  private synchronized boolean isRun()
  {
    return this.run;
  }

  @Override
  public void startProcess()
  {
    if ( thread == null )
    {
      thread = new Thread( this );
      thread.setName( getProccesName() );
      Factories.getDispatcher().subscribe( this );
      thread.start();
      setRun( thread.isAlive() );
      start();
    }
  }

  public abstract void start();

  public abstract String getProccesName();

  @Override
  public void stopProcess()
  {
    if ( thread != null )
    {
      Factories.getDispatcher().unsubscribe( this );
      thread.interrupt();
      setRun( false );
    }
    thread = null;
    stop();
  }

  public abstract void stop();

  @Override
  public boolean runProcess()
  {
    return isRun();
  }

}
