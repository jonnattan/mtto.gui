package cl.jonnattan.mtto.interfaces;

/**
 * Interfaz para Logger del sistema Pionero
 * 
 * @author Jonnattan Griffiths
 * @since 24/12/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IMyLogger
{
  /* Escribe un warning en el log */
  void warning( final Object clase, final String msg );

  /* Escribe un error en el log */
  void error( final Object clase, final String msg );

  /* Escribe un info en el log */
  void info( final Object clase, final String msg );

  /* Escribe un alarma en el log */
  void alarm( final Object clase, final String msg );
}
