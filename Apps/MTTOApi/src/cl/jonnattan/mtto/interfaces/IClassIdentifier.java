package cl.jonnattan.mtto.interfaces;

/**
 * Identifica los nombres de las clases para no tener que enviar todo
 * 
 * @author Jonnattan Griffiths
 * @since 26/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IClassIdentifier {
  /** Obtien el byte que identifica a la clase */
  byte getIdentifier(final Object clase);
  
  /** Ontiene el nombre de la clase identificada por ese byte */
  String getClass(final int identifier);
}
