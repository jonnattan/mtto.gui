package cl.jonnattan.mtto.interfaces;

import java.nio.ByteBuffer;

/**
 * Interfaz utilizada para comprimir y descomprimir datos
 * 
 * @author Jonnattan Griffiths
 * @since 06/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface ICompressor
{
  /** Metodo de compresion */
  ByteBuffer compress( ByteBuffer buffer );

  /** Metodo de descompresion */
  ByteBuffer descompress( ByteBuffer buffer );
}
