package cl.jonnattan.mtto.interfaces;

import java.awt.Dimension;
import java.awt.Point;

/**
 * Lee los parametros que son pasados en un archivo de configuracion
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IParameters
{
  /** Obtiene la ruta de la carpeta recurso */
  String getResourcePath();
  
  /** Obtiene la ruta de fotografias */
  String getImagePath();
  
  /** Obtiene carpeta donde se guarda la historia de chat */
  String getLogPath();
  
  /** Obtiene Puerto del Recolector de datos */
  int getPortServer();
  
  /** Obtiene IP del recolector de datos */
  String getIpServer();
  
  /** Obtiene Puerto del logger */
  int getPortDebug();
  
  /** Obtiene IP del Logger */
  String getIpDebug();
  
  /** Posicion en pantalla del programa */
  Point getInitialPosition();
  
  /** Dimension en pantalla del programa */
  Dimension getInitialDimension();
  
  long getTimerQuestion();
  /** Lee la variable que indica si debemos iniciar el servidor*/
  boolean initServer();
  
}
