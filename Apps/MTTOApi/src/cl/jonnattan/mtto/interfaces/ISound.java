package cl.jonnattan.mtto.interfaces;

/**
 * Interfaz de definicion de sonidos del sistema
 * 
 * @author Jonnattan Griffiths
 * @since 02/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface ISound
{
  /** Sonido de error */
  void error();

  /* Sonido de informacion */
  void info();
}
