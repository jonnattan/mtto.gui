package cl.jonnattan.mtto.interfaces;

import java.awt.Image;

/**
 * Clase que provee las imagenes requeridas.
 * 
 * @author Jonnattan Griffiths
 * @since 02/08/2019
 * @version 1.0 Copyright(c) 
 */
public interface IImageProvider
{
  /** Icono del sistema */
  Image getIconSystem();
  
  /** Obtiene Imagen de ICONO */
  Image getImgSystem();
  
  /** Obtiene Imagen Tripulante Sin foto */
  Image getTripulanteNullImg();
  
  /** Obtiene Imagen Capitan sin foto */
  Image getCapitanNullImg();
  
  /** Obtiene Imagen sin foto */
  Image getDesconocidoNullImg();
  
  /** Obtiene Imagen Nave Sin foto */
  Image getNaveNullImg();
  
}
