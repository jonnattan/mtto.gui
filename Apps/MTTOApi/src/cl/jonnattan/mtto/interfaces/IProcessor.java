package cl.jonnattan.mtto.interfaces;

import java.util.concurrent.LinkedBlockingQueue;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.mtto.interfaces.IQueueData;

/**
 * Define los procesos que corren en el ESAT. Normalmente esta pensado solo para
 * el motor de Chat
 * 
 * @author Jonnattan Griffiths
 * @since 21/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IProcessor extends Runnable, Listener
{
  /** Setea la cola sincronizada */
  void setQueue( LinkedBlockingQueue<IQueueData> queue );

  /** Inicia el proceso */
  void startProcess();

  /** Detiene el proceso */
  void stopProcess();
  
  /** Pregunta si esta corriendo el proceso*/
  boolean runProcess();
}
