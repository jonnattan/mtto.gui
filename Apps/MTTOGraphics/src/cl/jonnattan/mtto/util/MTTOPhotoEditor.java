package cl.jonnattan.mtto.util;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.pnls.PhotoEditor;

/**
 * Foto editor
 * 
 * @author Jonnattan Griffiths
 * @since SC1405 SCMMovil - Ltda
 * @version 1.1 del 23-11-2019
 */
public class MTTOPhotoEditor extends PhotoEditor
{
  private static final long serialVersionUID = 1L;
  private final int         SIZE             = (900 * 1024);
  //private static Logger     logger           = Logger.getGlobal();

  public MTTOPhotoEditor()
  {
    super();
    setPhotoDimension(180, 180);
    setSizeMax(SIZE);
    setPhotoFolder(Factories.getParameters().getImagePath());
  }

  public void showTripulanteDefault()
  {
    /*
     * foto.setNombrearchivo("Foto_TRIPULACION_SIN_FOTO.bmp");
     * foto.setTiporegistro(CONSTConst.PATHPHOTOTYPE); loadPhoto(foto);
     */
  }

  public void showCapitanDefault()
  {
    /*
     * foto.setNombrearchivo("Foto_CAPITAN_SIN_FOTO.bmp");
     * foto.setTiporegistro(CONSTConst.PATHPHOTOTYPE); loadPhoto(foto);
     */
  }

  public void showPersonaDefault()
  {
    /*
     * foto.setNombrearchivo("Foto_PERSONA_SIN_FOTO.bmp");
     * foto.setTiporegistro(CONSTConst.PATHPHOTOTYPE); loadPhoto(foto);
     */
  }

  public void showNaveDefault()
  {
    /*
     * foto.setNombrearchivo("Foto_NAVE_SIN_FOTO.bmp");
     * foto.setTiporegistro(CONSTConst.PATHPHOTOTYPE); loadPhoto(foto);
     */
  }

  /**
   * Carga la fotografia que representa el objeto en la base de datos
   * 
   * @param foto
   */
  /*
   * public void loadPhoto(final Fotografia foto) { if (foto != null) {
   * this.foto = foto; if (foto.getTiporegistro() == CONSTConst.PATHPHOTOTYPE) {
   * String nameFile = foto.getNombrearchivo(); String path = ""; if
   * (nameFile.contains("SIN_FOTO")) path =
   * Factories.getParameters().getResourcePath(); else path =
   * Factories.getParameters().getImagePath();
   * 
   * File file = new File(path + nameFile); if (file.exists())
   * loadFromFile(file); else
   * logger.warning("No existe archivo de fotografia en: " +
   * file.getAbsolutePath()); } else { byte[] bArray = foto.getFotografia(); if
   * (bArray != null) loadFromBytes(bArray, "bmp"); else
   * logger.warning("No exinten bytes de fotografia"); } } else
   * logger.warning("Fotografia Nula..."); }
   */

  /**
   * Obtiene la fotografia
   * 
   * @return
   */
  /*
   * public Fotografia getPhoto() {
   * foto.setTiporegistro(CONSTConst.PATHPHOTOTYPE);
   * foto.setNombrearchivo(getNameImageRedicida()); byte[] bArray =
   * getBytesImagen(EImageType.NORMAL, "bmp"); if (bArray != null)
   * foto.setFotografia(bArray); return foto; }
   */

  @Override
  public void saveImages(final String ruta)
  {
    String name = getNameFileImage();
    if (!name.contains("SIN_FOTO"))
      super.saveImages(ruta);
  }
}
