package cl.jonnattan.mtto.util;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import cl.jonnattan.mtto.util.CONSTConst;

/**
 * Redefinicion de campos de textos
 * 
 * @author Jonnattan Griffiths
 * @since 25-04-2019
 * @version 1.0 Copyright(c)   - 2019
 */

public class MTTOText extends JTextField
{
  private static final long serialVersionUID = 1L;
  private TxtKeyListener    listener         = null;
  
  public MTTOText()
  {
    super();
    setEditable(true);
    setFont(CONSTConst.font);
    setHorizontalAlignment(SwingConstants.LEFT);
    setColumns(10);
  }
  
  /**
   * Constructor que solamente valida el largo
   * 
   * @param maxChar
   *          Largo Maximo
   */
  public MTTOText(int maxChar)
  {
    this(maxChar, false, false, false);
  }
  
  /**
   * COnstructor que valida largo y convierte a mayusculas todo el texto
   * ingresado
   * 
   * @param maxChar
   *          largo
   * @param upOnly
   *          a mayuscula
   */
  public MTTOText(int maxChar, boolean upOnly)
  {
    this(maxChar, upOnly, false, false);
  }
  
  public MTTOText(int maxChar, boolean upOnly, boolean sinTilde)
  {
    this(maxChar, upOnly, sinTilde, false);
  }
  
  public MTTOText(int maxChar, boolean upOnly, boolean sinTilde,
      boolean isNumeric)
  {
    this();
    listener = new TxtKeyListener(maxChar);
    if (upOnly)
      listener.UpCaseOnly();
    if (sinTilde)
      listener.cleanChar();
    if (isNumeric)
      listener.numberOnly();
    addKeyListener(listener);
  }
}
