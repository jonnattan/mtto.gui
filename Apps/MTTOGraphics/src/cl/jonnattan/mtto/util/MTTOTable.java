package cl.jonnattan.mtto.util;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 * Define la tabla del proyecto
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class MTTOTable extends JTable
{
  private static final long serialVersionUID = 1L;
  
  public MTTOTable()
  {
    super();
    //setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setOpaque(false);
    setFillsViewportHeight(true);
    // Si habilito esto, el modelo se marea seleccionando
    // despues de ordenar
    // setAutoCreateRowSorter(true);
    getTableHeader().setVisible(true);
  }
}
