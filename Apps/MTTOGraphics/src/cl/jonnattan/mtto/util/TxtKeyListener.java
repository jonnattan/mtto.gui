package cl.jonnattan.mtto.util;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.text.JTextComponent;

/**
 * Listener que define funciones deseables de los textos
 * 
 * @author Jonnattan Griffiths
 * @since 25-04-2019
 * @version 1.0 Copyright(c)   - 2019
 */

public class TxtKeyListener extends KeyAdapter
{
  private int     maximo       = -1;
  private boolean enMayusculas = false;
  private boolean isNumeric    = false;
  private boolean sinTildes    = false;
  
  public TxtKeyListener()
  {
    this(-1);
  }
  
  public TxtKeyListener(int max)
  {
    this.maximo = max;
  }
  
  public void UpCaseOnly()
  {
    this.enMayusculas = true;
  }
  
  public void numberOnly()
  {
    this.isNumeric = true;
  }
  
  public void cleanChar()
  {
    this.sinTildes = true;
  }
  
  @Override
  public void keyTyped(KeyEvent keyEvent)
  {
    if (keyEvent.getComponent() instanceof JTextComponent)
    {
      JTextComponent textComp = (JTextComponent) keyEvent.getComponent();
      int length = textComp.getText().length();
      if (length >= maximo)
      {
        alarmMaximoExcedido(textComp);
        keyEvent.consume();
        return;
      }
    }
    
    // Realiza acciones
    Character letra = keyEvent.getKeyChar();
    if (isNumeric && letra != KeyEvent.VK_BACK_SPACE && letra != KeyEvent.VK_DELETE )
    {
      if (!Character.isDigit(letra))
      {
        alarmNoNumberChar(keyEvent.getComponent());
        keyEvent.consume();
        return;
      }
    }
    if (sinTildes)
      keyEvent.setKeyChar(sinTilde(letra));
    if (enMayusculas)
      keyEvent.setKeyChar(Character.toUpperCase(letra));
  }
  
  /**
   * Reemplaza letras con tildes por letras sin tildes
   * 
   * @param letra
   * @return
   */
  private Character sinTilde(final Character letra)
  {
    Character resp = ' ';
    switch (letra)
    {
      case 'á':
        resp = 'a';
        break;
      case 'Á':
        resp = 'A';
        break;
      case 'é':
        resp = 'e';
        break;
      case 'É':
        resp = 'E';
        break;
      case 'í':
        resp = 'i';
        break;
      case 'Í':
        resp = 'I';
        break;
      case 'ó':
        resp = 'o';
        break;
      case 'Ó':
        resp = 'O';
        break;
      case 'ú':
        resp = 'u';
        break;
      case 'Ú':
        resp = 'U';
        break;
      default:
        resp = letra;
        break;
    }
    return resp;
  }
  
  /**
   * Envia una alarma windows
   * 
   * @param comp
   */
  private void alarmMaximoExcedido(final Component comp)
  {
    String msg = "El largo del texto no debe superar los " + maximo
        + " caracteres";
    JOptionPane.showMessageDialog(comp, msg, "Sr. Operador",
        JOptionPane.WARNING_MESSAGE);
  }
  
  /**
   * Envia una alarma windows
   * 
   * @param comp
   */
  private void alarmNoNumberChar(final Component comp)
  {
    String msg = "S�lo se aceptar caracteres numericos";
    JOptionPane.showMessageDialog(comp, msg, "Sr. Operador",
        JOptionPane.WARNING_MESSAGE);
  }
}
