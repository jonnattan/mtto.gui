package cl.jonnattan.mtto.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.painter.BusyPainter.Direction;

public class MTTOWait extends JDialog
{
  private static final long serialVersionUID = 1L;
  private JXBusyLabel       busyLabel        = null;
  private final Dimension   mySize;
  private static MTTOWait   instance         = null;

  public static MTTOWait getInstance()
  {
    if (MTTOWait.instance == null)
      MTTOWait.instance = new MTTOWait();
    return MTTOWait.instance;
  }

  private MTTOWait()
  {
    super();
    getContentPane().setFont(new Font("Tahoma", Font.BOLD, 9));
    
    this.mySize = new Dimension(70, 60);
    initialize();
  }

  private void initialize()
  {
    JDialog.setDefaultLookAndFeelDecorated(true);
    getRootPane().setWindowDecorationStyle(JRootPane.NONE); 
    
    setUndecorated(true);
    setModalExclusionType(ModalExclusionType.NO_EXCLUDE);
    setResizable(false);
    setVisible(false);
    getRootPane().setOpaque(false);
    setBackground(new Color(0, 0, 0, 0));
    setPreferredSize(mySize);
    getContentPane().setLayout(new BorderLayout(0, 0));
    setModal(true);
    setAlwaysOnTop(true);
    
    JPanel panel = new JPanel();
    panel.setOpaque(false);
    panel.setRequestFocusEnabled(false);
    getContentPane().add(panel);

    busyLabel = new JXBusyLabel();
    busyLabel.setBounds(20, 11, 30, 30);
    busyLabel.setDirection(Direction.RIGHT);
    busyLabel.setHorizontalAlignment(SwingConstants.CENTER);
    busyLabel.setName("Bussy");
    busyLabel.getBusyPainter().setHighlightColor(Color.GREEN.darker());
    busyLabel.getBusyPainter().setBaseColor(Color.WHITE.brighter());
    busyLabel.setBusy(false);
    busyLabel.setDelay(100);
    busyLabel.getBusyPainter().setPoints(10);
    busyLabel.getBusyPainter().setTrailLength(10);
    panel.setLayout(null);
    busyLabel.setBusy(true);
    panel.add(busyLabel);
    
    JLabel lblCharge = new JLabel("Cargando...");
    lblCharge.setForeground(Color.GREEN);
    lblCharge.setHorizontalAlignment(SwingConstants.CENTER);
    lblCharge.setBounds(0, 36, 70, 14);
    panel.add(lblCharge);
    pack();
  }

  void posicion(final Component c)
  {
    int   x     = c.getLocation().x + (c.getWidth() / 2) - (mySize.width / 2);
    int   y     = c.getLocation().y + (c.getHeight() / 2) - (mySize.height / 2);
    Point point = new Point(x, y);
    //System.out.println("X: " + x + " Y: " + y);
    MTTOWait.instance.setLocation(point);

    pack();
  }

  /**
   * Inicio
   */
  public void iniciar(final Component c)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        busyLabel.setBusy(true);
        MTTOWait.instance.posicion(c);
        MTTOWait.instance.setVisible(true);
      }
    });
  }

  /**
   * Final
   */
  public void detener()
  {
    MTTOWait.instance.setVisible(false);
    busyLabel.setBusy(false);
    MTTOWait.instance.dispose();
  }
}
