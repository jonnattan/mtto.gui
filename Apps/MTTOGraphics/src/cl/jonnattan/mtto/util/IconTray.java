package cl.jonnattan.mtto.util;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.gui.GUIWrapper;

/**
 * Clase que administra el Icono del menu
 * 
 * @author Jonnattan Griffiths
 * @since 05/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class IconTray implements ActionListener {
  private SystemTray systemTray         = null;
  private TrayIcon   trayIcon           = null;
  private PopupMenu  ScreenKeyPopupMenu = null;
  private MenuItem   mnuSalir           = null;
  private MenuItem   mnuAbout           = null;
  private MenuItem   mnuMostrar         = null;
  private GUIWrapper frame              = null;
  
  public IconTray()
  { 
    
  }
  
  /** Inicia el Try */
  public boolean hiddenFrame()
  {
    boolean success = SystemTray.isSupported();
    if (success)
      systemTray = SystemTray.getSystemTray();
    else
      System.err.println("Tray Icon no soportado...");
    return success;
  }
  
  public void setFrame(GUIWrapper frame)
  {
    this.frame = frame;
    Image icono = Factories.getImageProvider().getIconSystem()
        .getScaledInstance(15, 15, 0);
    setSystemTray(icono, frame.getTitle(), getMenuSistema());
  }
  
  @Override
  public void actionPerformed(ActionEvent event)
  {
    if (event.getSource().equals(mnuSalir))
    {
      closeTray();
      frame.closeAll();
    }
    if (event.getSource().equals(mnuAbout))
    {
      frame.showAbout();
    }
    if (event.getSource().equals(mnuMostrar))
    {
      frame.setVisible(true);
      frame.setExtendedState(JFrame.NORMAL);
    }
  }
  
  public void closeTray()
  {
    if (systemTray != null && trayIcon != null)
      systemTray.remove(trayIcon);
  }
  
  private boolean setSystemTray(Image image, String toolTip, PopupMenu menu)
  {
    boolean success = false;
    try
    {
      if (systemTray != null)
      {
        if (systemTray.getTrayIcons().length == 0)
        {
          if (image != null)
          {
            trayIcon = new TrayIcon(image, toolTip);
            trayIcon.addActionListener(this);
            trayIcon.setPopupMenu(menu);
            systemTray.add(trayIcon);
            trayIcon.addMouseListener(new MouseAdapter() {
              public void mouseClicked(MouseEvent event)
              {
                if (event.getClickCount() == 2)
                {
                  if (frame != null)
                  {
                    frame.setVisible(!frame.isVisible());
                    if (frame.isVisible())
                      frame.setExtendedState(JFrame.NORMAL);
                    else
                      frame.setExtendedState(JFrame.ICONIFIED);
                    event.consume();
                  }
                }
              };
            });
            success = true;
          }
        }
        else
        {
          systemTray.getTrayIcons()[0].setImage(image);
          systemTray.getTrayIcons()[0].setToolTip(toolTip);
          systemTray.getTrayIcons()[0].setPopupMenu(menu);
        }
      }
    }
    catch (AWTException ex)
    {
      System.out.println("ERROR TRAY: " + ex.getMessage());
      success = false;
    }
    return success;
  }
  
  private PopupMenu getMenuSistema()
  {
    if (ScreenKeyPopupMenu == null)
    {
      ScreenKeyPopupMenu = new PopupMenu();
      mnuMostrar = new MenuItem("Mostrar App");
      mnuMostrar.addActionListener(this);
      mnuSalir = new MenuItem("Cerrar App");
      mnuSalir.addActionListener(this);
      mnuAbout = new MenuItem("Acerca de...");
      mnuAbout.addActionListener(this);
      ScreenKeyPopupMenu.add(mnuMostrar);
      ScreenKeyPopupMenu.add(mnuSalir);
      ScreenKeyPopupMenu.add(mnuAbout);
    }
    return ScreenKeyPopupMenu;
  }
  
  public void tryMessageInfo(final String message)
  {
    if (trayIcon != null)
      trayIcon.displayMessage(frame.getTitle(), message, MessageType.INFO);
  }
  
  public void tryMessageError(final String message)
  {
    if (trayIcon != null)
      trayIcon.displayMessage(frame.getTitle(), message, MessageType.ERROR);
  }
  
  public TrayIcon getTrayIcon()
  {
    return trayIcon;
  }
  
}
