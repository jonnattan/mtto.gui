package cl.jonnattan.mtto.util;

import java.awt.BorderLayout;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JPanel;
// import javax.swing.event.ChangeEvent;
// import com.lavantech.gui.comp.DateTimePicker;
// import com.lavantech.gui.comp.PickerEditorListener;
// import javax.swing.SwingConstants;
import org.jdesktop.swingx.JXDatePicker;

import cl.jonnattan.mtto.util.CONSTConst;

public class MTTOHourDateEditor extends JPanel /* implements PickerEditorListener*/
{
  private static final long serialVersionUID = 1L;
  private JXDatePicker    dateSelector     = null;
  private JLabel            lblEdad          = null;
  private boolean           calculateAge     = false;
  
  public MTTOHourDateEditor()
  {
    super();
    setOpaque(false);
    setLayout(new BorderLayout(0, 0));
    dateSelector = new JXDatePicker();
    dateSelector.setOpaque(false);
    // dateSelector.setPattern("E dd/MM/yyyy");
    // dateSelector.setDisplayClock(false);
    dateSelector.setFont(CONSTConst.font);
    // dateSelector.getRenderer().setHorizontalAlignment(SwingConstants.LEFT);
    add(dateSelector, BorderLayout.CENTER);
  }
  
  /**
   * Setea la fecha proveniente de un time Stap
   * 
   * @param date
   */
  public void setDate(final Timestamp date)
  {
    dateSelector.setDate(date != null ? date : new Date());
    if (calculateAge)
      calculateAge(date);
  }
  
  /**
   * Limpia los componentes con la fecha actual
   */
  public void cleanComponent()
  {
    Date date = new Date();
    dateSelector.setDate(date);
    if (calculateAge)
      calculateAge(date);
  }
  
  /**
   * Metodo de configuracion del componente
   * 
   * @param withClock
   * @param calculateAge
   */
  public void setWithClock(final boolean withClock, final boolean calculateAge)
  {
    this.calculateAge = calculateAge;
    // dateSelector.setDisplayClock(withClock);
    // dateSelector.setDisplayClock(withClock);
    // if (withClock)
      // dateSelector.setPattern("dd/MM/yyyy HH:mm:ss");
    if (calculateAge)
    {
      // dateSelector.addEditorListener(this);
      lblEdad = new JLabel("0 A\u00F1os");
      lblEdad.setFont(CONSTConst.font);
      add(lblEdad, BorderLayout.EAST);
    }
  }
  
  /*@Override
  public void setEnabled(boolean enabled)
  {
    dateSelector.setEnabled(enabled);
    if (lblEdad != null)
      lblEdad.setEnabled(enabled);
  }
  
  @Override
  public void editingStopped(ChangeEvent event)
  {
    DateTimePicker data = (DateTimePicker) event.getSource();
    if (calculateAge)
      calculateAge(data.getDate());
  }*/
  
  private void calculateAge(final Date nacimiento)
  {
    Calendar dateNac = Calendar.getInstance();
    // Se crea un objeto con la fecha actual
    Calendar current = Calendar.getInstance();
    // Se asigna la fecha recibida a la fecha de nacimiento.
    dateNac.setTime(nacimiento);
    // Se restan la fecha actual y la fecha de nacimiento
    int edad = current.get(Calendar.YEAR) - dateNac.get(Calendar.YEAR);
    int mes = current.get(Calendar.MONTH) - dateNac.get(Calendar.MONTH);
    int dia = current.get(Calendar.DATE) - dateNac.get(Calendar.DATE);
    // Se ajusta el a�o dependiendo el mes y el d�a
    if (mes < 0 || (mes == 0 && dia < 0))
    {
      edad--;
    }
    // Regresa la edad en base a la fecha de nacimiento
    lblEdad.setText(edad + " A\u00F1os");
  }
  
  public Timestamp getDate()
  {
    return new Timestamp(dateSelector.getDate().getTime());
  }
  /*
  @Override
  public void editingCanceled(ChangeEvent arg0)
  {
    // TODO Auto-generated method stub
    
  }
  
  @Override
  public void editingStarted(ChangeEvent arg0)
  {
    // TODO Auto-generated method stub
    
  }*/
}
