package cl.jonnattan.mtto.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.border.AbstractBorder;

/**
 * Borde de las ventanas
 * 
 * @author Jonnattan Griffiths
 * @since 08/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class MyBorderSystem extends AbstractBorder
{
  private static final long serialVersionUID = 7644739936531926341L;
  private static final int  THICKNESS        = 4;
  
  public MyBorderSystem()
  {
    super();
  }
  
  @Override
  public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
  {
    Graphics2D g2 = (Graphics2D) g.create();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setColor(Color.GRAY);
    g2.setStroke(new BasicStroke(THICKNESS, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    g2.drawRoundRect(THICKNESS, THICKNESS, width - THICKNESS - 2, height - THICKNESS - 2, 20, 20);
    g2.dispose();
  }
  
  @Override
  public Insets getBorderInsets(Component c)
  {
    return new Insets(THICKNESS, THICKNESS, THICKNESS, THICKNESS);
  }
  
  @Override
  public Insets getBorderInsets(Component c, Insets insets)
  {
    insets.left = insets.top = insets.right = insets.bottom = THICKNESS;
    return insets;
  }
  
  public boolean isBorderOpaque()
  {
    return true;
  }
}
