package cl.jonnattan.mtto.util;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;

public class CmbPlanRenderer<T extends AbsKeylable> implements ListCellRenderer<T>
{
  private final JLabel label;

  public CmbPlanRenderer()
  {
    super();
    label = new JLabel();
    label.setFont(CONSTConst.font);
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected,
      boolean cellHasFocus)
  {
    if (value != null && value instanceof jPlan) {
      jPlan item = (jPlan) value;
      label.setText(item != null ? item.getName() + " " + item.getVersion() : "Es null");
    } else {
      if (index >= 0) {
        jPlan item = (jPlan) list.getModel().getElementAt(index);
        label.setText(item != null ? item.getName() + " " + item.getVersion() : "Es null");
      } else
        label.setText("Seleccione un elemento");
    }

    return label;
  }

}