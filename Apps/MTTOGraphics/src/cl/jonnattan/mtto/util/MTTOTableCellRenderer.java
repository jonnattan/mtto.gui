package cl.jonnattan.mtto.util;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableCellRenderer;

/**
 * CellRenderer Default
 * 
 * @author Jonnattan Griffiths
 * @since 05/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class MTTOTableCellRenderer extends JTextPane implements TableCellRenderer
{
  private static final long serialVersionUID = 1L;

  public MTTOTableCellRenderer()
  {
    super();
    setOpaque( true );
  }

  @Override
  public Component getTableCellRendererComponent( JTable tableObject,
      Object valueObject, boolean isSelected, boolean hasFocus, int tableRow,
      int column )
  {
    if ( valueObject != null && valueObject instanceof String )
    {
      setText( valueObject.toString() );
    }
    setBackground( isSelected ? tableObject.getSelectionBackground()
        : tableObject.getBackground() );
    tableObject.setToolTipText( "Aca va el tool tips" );
    return this;
  }
}
