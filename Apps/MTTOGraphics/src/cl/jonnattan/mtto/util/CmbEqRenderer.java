package cl.jonnattan.mtto.util;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;

public class CmbEqRenderer<T extends AbsKeylable> implements ListCellRenderer<T>
{

  private final JLabel label;

  public CmbEqRenderer()
  {
    super();
    label = new JLabel();
    label.setFont(CONSTConst.font);
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected,
      boolean cellHasFocus)
  {
    if (value != null && value instanceof jEquipment) {
      jEquipment item = (jEquipment) value;
      label.setText(item != null ? item.getName() : "Es null");
    } else {
      if (index >= 0) {
        jEquipment item = (jEquipment) list.getModel().getElementAt(index);
        label.setText(item != null ? item.getName() : "Es null");
      } else
        label.setText("Seleccione un elemento");
    }
    return label;
  }

}