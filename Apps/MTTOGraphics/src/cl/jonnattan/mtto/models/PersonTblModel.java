package cl.jonnattan.mtto.models;

import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class PersonTblModel extends AMttoTblModel<jPerson>
{
  private final static int NAME   = 0;
  private final static int CARGO  = 1;
  private final static int EDAD   = 2;
  private final static int CORREO = 3;

  private final String[]    columnas         = { "Nombre", "Cargo", "Edad",
      "Correo" };
  private static final long serialVersionUID = 1L;

  public PersonTblModel()
  {
    super();
  }

  @Override
  protected Object getValue(jPerson dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case NAME:
      object = dato.getNombre();
      break;
    case CARGO:
      object = dato.getCargo();
      break;
    case EDAD:
      object = String.format("%d", dato.getEdad());
      break;
    case CORREO:
      object = dato.getCorreo();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return String.class;
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }
}
