package cl.jonnattan.mtto.models;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class CheckTblModel extends AMttoTblModel<jChecklist>
{
  private final static int TITLE = 0;
  private final static int DESCR = 1;

  private final String[]    columnas         = { "Titulo", "Descripción" };
  private static final long serialVersionUID = 1L;

  public CheckTblModel()
  {
    super();
  }

  @Override
  protected Object getValue(jChecklist dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case TITLE:
      object = dato.getTitulo();
      break;
    case DESCR:
      object = dato.getDescripcion();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return String.class;
  }
}
