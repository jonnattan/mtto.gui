package cl.jonnattan.mtto.models;

import java.util.Date;

import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class PlanTblModel extends AMttoTblModel<jPlan>
{
  private final static int NAME     = 0;
  private final static int VERSION  = 1;
  private final static int REVISION = 2;
  private final static int CANTIDAD = 3;

  private final String[]    columnas         = { "Nombre", "Versión", "Última Rev.", "Cantidad Items" };
  private static final long serialVersionUID = 1L;

  public PlanTblModel()
  {
    super();
  }

  @Override
  protected Object getValue(jPlan dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case NAME:
      object = dato.getName();
      break;
    case VERSION:
      object = dato.getVersion();
      break;
    case REVISION:
      object = dato.getRevision();
      break;
    case CANTIDAD:
      object = dato.getItems().size();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    Class<?> clase = String.class;
    switch (columnIndex) {
    case NAME:
      clase = String.class;
      break;
    case VERSION:
      clase = String.class;
      break;
    case REVISION:
      clase = Date.class;
      break;
    case CANTIDAD:
      clase = Integer.class;
      break;
    default:
      clase = String.class;
      break;
    }
    return clase;
  }
}
