package cl.jonnattan.mtto.models;

import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c)   - 2019
 */

public class CmteTblModel extends AMttoTblModel<jComponent>
{
  private final static int  NAME             = 0;
  private final static int  DESCRIPTION      = 1;
  private final String[]    columnas         = { "Nombre", "Descripción" };
  private static final long serialVersionUID = 1L;

  public CmteTblModel()
  {
    super();
  }

  @Override
  protected Object getValue(jComponent dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case NAME:
      object = dato.getName();
      break;
    case DESCRIPTION:
      object = dato.getDescripcion();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return String.class;
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }
}
