package cl.jonnattan.mtto.models;

import java.util.List;

import javax.swing.DefaultComboBoxModel;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;

/**
 * Modelo del combo de paises
 * 
 * @author Jonnattan Griffiths
 * @since 25-04-2019
 * @version 1.0 Copyright(c)
 */

public class MTTOCmbModel<T extends AbsKeylable> extends DefaultComboBoxModel<T>
{
  private static final long serialVersionUID = 1L;

  public MTTOCmbModel()
  {
    super();
  }

  public void save(final List<T> list)
  {
    for (T dato : list) {
      addElement(dato);
    }
  }

  public void selectToID(final Long idObject)
  {
    for (int i = 0; i < getSize(); i++) {
      T elem = getElementAt(i);
      if (elem.getId().equals(idObject)) {
        setSelectedItem(elem);
        break;
      }
    }
  }

  public Long getIdSelect()
  {
    @SuppressWarnings("unchecked")
    T t = (T) getSelectedItem();
    if (t != null)
      return t.getId();
    else
      return -1L;
  }

}
