package cl.jonnattan.mtto.models;

import java.text.SimpleDateFormat;

import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c)
 */

public class MttoTblModel extends AMttoTblModel<jReport>
{
  private final static int FECHA   = 0;
  private final static int PERSONA = 1;
  private final static int COMMENT = 2;

  private final String[]    columnas         = { "Fecha", "Realizado por",
      "Comentario" };
  private static final long serialVersionUID = 1L;

  private final SimpleDateFormat sdf;

  public MttoTblModel()
  {
    super();
    sdf = new SimpleDateFormat("dd/MM/yyyy");
  }

  @Override
  protected Object getValue(jReport dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case FECHA:
      object = sdf.format(dato.getFechahora());
      break;
    case PERSONA:
      object = dato.getPersona();
      break;
    case COMMENT:
      object = dato.getComentario();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }

  @Override
  public Class<?> getColumnClass(int arg0)
  {
    return String.class;
  }
}
