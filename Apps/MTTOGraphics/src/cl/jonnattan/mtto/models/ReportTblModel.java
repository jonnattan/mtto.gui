package cl.jonnattan.mtto.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import cl.jonnattan.mantenimiento.datajson.jReport;

/**
 * Modelo de tabla para naves
 * 
 * @author Jonnattan Griffiths
 * @since 28-12-2019
 * @version 1.0 Copyright(c) 2019
 */

public class ReportTblModel extends AbstractTableModel
{
  private static final long serialVersionUID = 1L;
  private List<jReport>     lista            = null;
  private static final int  COMENTARIO       = 0;
  public static final int   FALLA            = 1;
  public static final int   EXTERNO          = 2;
  private final String[]    columnas         = { "Comentario", "Aprueba", "Externo" };

  public ReportTblModel()
  {
    super();
    lista = new ArrayList<>();
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    jReport report = lista.get(rowIndex);
    Object  object = null;
    switch (columnIndex) {
    case COMENTARIO: {
      object = report.getComentario();
    }
      break;
    case FALLA: {
      object = report.getFalla();
    }
      break;
    case EXTERNO: {
      object = report.getExterno();
    }
      break;
    default:
      object = "ERROR";
    }
    return object;
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return columnIndex == FALLA;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return columnIndex == COMENTARIO ? String.class : Boolean.class;
  }

  @Override
  public int getRowCount()
  {
    return lista.size();
  }

  /** borra toda la lista */
  public void clearAll()
  {
    lista.clear();
    fireTableDataChanged();
  }

  /** Retorna el objeto en la posicion de la fila que corresponda */
  public jReport getData(int rowIndex)
  {
    jReport object = null;
    try {
      object = lista.get(rowIndex);
    } catch (IndexOutOfBoundsException ex) {
      object = null;
    }
    return object;
  }

  /** Retorna todos los objetos de la lista */
  public List<jReport> getList()
  {
    return lista;
  }

  /**
   * Inseta una fila en la tabla
   * 
   * @param unit
   */
  public void save(jReport unit)
  {
    if (unit != null) {
      try {
        int index = lista.indexOf(unit);
        if (index >= 0)
          lista.set(index, unit);
        else
          lista.add(unit);
        fireTableDataChanged();
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Elimina el elemento en esa posicion
   * 
   * @param aData
   */
  public void deleteRow(int index)
  {
    try {
      lista.remove(index);
      fireTableDataChanged();
    } catch (IndexOutOfBoundsException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Elimina el objeto de la lista
   * 
   * @param unit
   */
  public void deleteRow(jReport unit)
  {
    try {
      lista.remove(unit);
      fireTableDataChanged();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
