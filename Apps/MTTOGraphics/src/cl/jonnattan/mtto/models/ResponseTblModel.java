package cl.jonnattan.mtto.models;

import java.text.SimpleDateFormat;

import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class ResponseTblModel extends AMttoTblModel<jResponse>
{
  private final static int TITLE = 0;
  private final static int DATE  = 1;
  private final static int PASA  = 2;

  private final SimpleDateFormat format;

  private final String[]    columnas         = { "Check", "Fecha", "OK" };
  private static final long serialVersionUID = 1L;

  public ResponseTblModel()
  {
    super();
    format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  }

  @Override
  protected Object getValue(jResponse dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex) {
    case TITLE:
      object = dato.getCheckList();
      break;
    case DATE:
      object = format.format(dato.getDate());
      break;
    case PASA:
      object = dato.getYes();
      break;
    default:
      object = "NULL";
      break;
    }
    return object;
  }

  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }

  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    Class<?> object = null;
    switch (columnIndex) {
    case PASA:
      object = Boolean.class;
      break;
    default:
      object = String.class;
      break;
    }
    return object;
  }
}
