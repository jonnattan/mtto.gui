package cl.jonnattan.mtto.models;

import java.util.List;

import javax.swing.DefaultComboBoxModel;

/**
 * Modelo del combo de edad
 * 
 * @author Jonnattan Griffiths
 * @since 25-04-2019
 * @version 1.0 Copyright(c)
 */

public class CorrCmbModel extends DefaultComboBoxModel<Integer>
{
  private static final long serialVersionUID = 1L;
  private Integer           defaultData      = 15;

  public CorrCmbModel(final int init, final int end)
  {
    super();
    for (Integer i = init; i <= end; i++)
      addElement(i);
  }

  public CorrCmbModel(final int end)
  {
    this(0, end);
  }

  public void save(final List<Integer> list)
  {
    removeAllElements();
    for (Integer dato : list) {
      if (defaultData == null)
        defaultData = dato;
      addElement(dato);
    }
  }

  /**
   * Selecciona elemento
   * 
   * @param idBd ID en la Base de datos
   */
  public void selectObject(final Integer idBd)
  {
    setSelectedItem(idBd);
  }

  /**
   * Selecciona elemento por defecto
   */
  public void defaultSelection()
  {
    setSelectedItem(defaultData);
  }

}
