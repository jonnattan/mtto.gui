package cl.jonnattan.mtto.models;

import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mtto.abstracs.AMttoTblModel;

/**
 * Modelo de tabla para naves
 * 
 * @author Jonnattan Griffiths
 * @since 23-04-2019
 * @version 1.0 Copyright(c)   - 2019
 */

public class EqTblModel extends AMttoTblModel<jEquipment>
{
  private static final long serialVersionUID = 1L;
  private final int         NAME             = 0;
  private final int         PLAN            = 1;
  private final int         DETAIL            = 2;
  private final String[]    columnas         = { "Nombre", "Plan Mtto", "Comentario Último Mtto" };
  
  public EqTblModel()
  {
    super();
  }
  
  @Override
  protected Object getValue(jEquipment dato, int columnIndex)
  {
    Object object = null;
    switch (columnIndex)
    {
      case NAME:
        object = dato.getName();
        break;
      case PLAN:
        object = dato.getNamePlan();
        break;
      case DETAIL:
        object = dato.getLastDetail();
        break;
      default:
        object = "NULL";
        break;
    }
    return object;
  }
  
  @Override
  public String getColumnName(int columnIndex)
  {
    return columnas[columnIndex];
  }
  
  @Override
  public int getColumnCount()
  {
    return columnas.length;
  }
  
  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return String.class;
  }
}
