package cl.jonnattan.mtto.interfaces;

import cl.jonnattan.base.middleware.Listener;

/**
 * Interfaz de contrato para todos los paneles editores de elementos de base de datos
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public interface IPnlEditor extends Listener 
{
  /** Nombre del Tab */
  public String getTabName();
  
  /** Indica si se encuenta en estado de edicion */
  public boolean isWork();
  
}
