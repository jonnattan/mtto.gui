package cl.jonnattan.mtto.interfaces;

import java.util.Map;

import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c)
 */

public interface IPnlTuple
{
  /** Habilita o desabilita los componentes */
  public void pnlEnable(final boolean enable);

  /** Limpia el panel */
  public void dataClean();

  /** Pasa la Persistencia para rellenar campos del panel */
  public void fillPanel(final Map<?, ?> dataBd);

  /** Obtien el elemento actualmente en edicion */
  public IKeylable getActiveElement();

  /** Crea un nuevo elemento vacio */
  public void createElement();

  /** Crea un nuevo elemento vacio */
  public void querryResponse(final Object content, final Object source);

  /** Avisa la seleccion de un elemento en la interfaz grafica */
  public void elementSelected(final Object object);

  /** Avisa si se guardo un elemento en la persistencia */
  public void elementSaved(final Object object);

  /** Avisa si se removio un elemento en la persistencia */
  public void elementRemoved(final Object object);

  public void repaintForChangeLook();

}
