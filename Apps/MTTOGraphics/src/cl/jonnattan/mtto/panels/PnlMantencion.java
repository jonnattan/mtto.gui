package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.gui.GUIMtto;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.message.MSGToBdQuerry;
import cl.jonnattan.mtto.models.ResponseTblModel;
import cl.jonnattan.mtto.persistence.PersistenceManager;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOTable;
import cl.jonnattan.mtto.util.MTTOText;

public class PnlMantencion extends JPanel implements IPnlTuple
{
  private static final long      serialVersionUID = 1L;
  private static Logger          logger           = Logger.getGlobal();
  private final SimpleDateFormat dateFormat;
  private MTTOText               txtNameEq        = null;
  private MTTOText               txtPlanMtto      = null;
  private MTTOText               txtQuien         = null;
  private MTTOText               txtFechaMtto     = null;
  private MTTOText               txtFalla         = null;
  private ResponseTblModel       respTblModel     = null;
  private MTTOTable              respTable        = null;
  private JButton                btnMttoNew       = null;
  private JTextArea              txtComentario    = null;
  private jFullEquipment         currentEq        = null;
  private jPlan                  plan             = null;
  private jReport                active           = null;
  private GUIMtto                guiMtto          = null;

  public PnlMantencion()
  {
    super();
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    setOpaque(false);
    JLabel lblCamposObligatorios = new JLabel("* Campos Obligatorios");
    lblCamposObligatorios.setFont(CONSTConst.font);
    setLayout(new BorderLayout(0, 0));

    JPanel pnlInformacion = new JPanel();
    pnlInformacion.setBorder(
        new TitledBorder(null, "Datos para Mantenci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    pnlInformacion.setOpaque(false);
    add(pnlInformacion, BorderLayout.NORTH);
    pnlInformacion.setLayout(new BorderLayout(0, 0));

    JPanel pnlDatos = new JPanel();
    pnlDatos.setBorder(null);
    pnlDatos.setOpaque(false);
    pnlInformacion.add(pnlDatos, BorderLayout.CENTER);
    pnlDatos.setLayout(new GridLayout(0, 1, 0, 0));

    JPanel pnlNombres = new JPanel();
    pnlNombres.setOpaque(false);
    pnlDatos.add(pnlNombres);
    pnlNombres.setLayout(new BorderLayout(0, 0));

    JLabel lblEq = new JLabel("Equipo         ");
    lblEq.setFont(CONSTConst.font);
    pnlNombres.add(lblEq, BorderLayout.WEST);

    txtNameEq = new MTTOText(50, true);
    pnlNombres.add(txtNameEq);

    JPanel pnlApellidos = new JPanel();
    pnlApellidos.setOpaque(false);
    pnlDatos.add(pnlApellidos);
    pnlApellidos.setLayout(new BorderLayout(0, 0));

    JLabel lblPlan = new JLabel("Plan de Mtto.  ");
    lblPlan.setFont(CONSTConst.font);
    pnlApellidos.add(lblPlan, BorderLayout.WEST);

    txtPlanMtto = new MTTOText(50, true);
    pnlApellidos.add(txtPlanMtto);

    JPanel pnlEmail = new JPanel();
    pnlEmail.setOpaque(false);
    pnlDatos.add(pnlEmail);
    pnlEmail.setLayout(new BorderLayout(0, 0));

    JLabel lblFecha = new JLabel("Fecha Mtto.    ");
    lblFecha.setFont(CONSTConst.font);
    pnlEmail.add(lblFecha, BorderLayout.WEST);

    txtFechaMtto = new MTTOText(50);
    pnlEmail.add(txtFechaMtto);

    JPanel pnlMail = new JPanel();
    pnlMail.setOpaque(false);
    pnlDatos.add(pnlMail);
    pnlMail.setLayout(new BorderLayout(0, 0));

    JLabel lblFalla = new JLabel("Falla          ");
    lblFalla.setFont(CONSTConst.font);
    pnlMail.add(lblFalla, BorderLayout.WEST);

    txtFalla = new MTTOText(50);
    pnlMail.add(txtFalla, BorderLayout.CENTER);

    JPanel pnlLicencia = new JPanel();
    pnlLicencia.setOpaque(false);
    pnlDatos.add(pnlLicencia);
    pnlLicencia.setLayout(new BorderLayout(0, 0));

    JLabel lblQuien = new JLabel("Realizada por  ");
    lblQuien.setFont(CONSTConst.font);
    pnlLicencia.add(lblQuien, BorderLayout.WEST);

    txtQuien = new MTTOText(20);
    pnlLicencia.add(txtQuien);

    JPanel pnlCargo = new JPanel();
    pnlCargo.setBorder(
        new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)),
            "CheckList", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
    add(pnlCargo, BorderLayout.CENTER);
    pnlCargo.setOpaque(false);
    pnlCargo.setLayout(new BorderLayout(0, 0));

    JPanel pnlEdicionCargos = new JPanel();
    pnlEdicionCargos.setOpaque(false);
    pnlInformacion.add(pnlEdicionCargos, BorderLayout.SOUTH);
    pnlInformacion.setOpaque(false);

    btnMttoNew = new JButton("Nueva Mantenci\u00F3n");
    btnMttoNew.setEnabled(false);
    btnMttoNew.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent e)
      {
        iniciarNuevaMtto();
      }
    });
    pnlEdicionCargos.setLayout(new BorderLayout(0, 0));
    btnMttoNew.setFont(CONSTConst.font);

    JPanel pnlComentario = new JPanel();
    pnlComentario.setOpaque(false);
    pnlComentario.setLayout(new BorderLayout(0, 0));

    JLabel lblCmmto = new JLabel("Comentario     ");
    lblCmmto.setFont(CONSTConst.font);
    pnlComentario.add(lblCmmto, BorderLayout.WEST);

    txtComentario = new JTextArea();
    txtComentario.setLineWrap(true);
    txtComentario.setRows(2);
    pnlComentario.add(txtComentario, BorderLayout.CENTER);

    pnlEdicionCargos.add(pnlComentario, BorderLayout.CENTER);

    JPanel pnlBton = new JPanel();
    pnlBton.setOpaque(false);
    pnlEdicionCargos.add(pnlBton, BorderLayout.SOUTH);
    pnlBton.setLayout(new GridLayout(0, 3, 0, 0));
    JLabel lblNewLabel   = new JLabel("");
    JLabel lblNewLabel_1 = new JLabel("");
    pnlBton.add(lblNewLabel_1);
    pnlBton.add(btnMttoNew);
    pnlBton.add(lblNewLabel);

    JPanel pnlTable = new JPanel();
    pnlTable.setOpaque(false);
    pnlTable.setLayout(new BorderLayout(0, 0));
    respTable = new MTTOTable();
    respTblModel = new ResponseTblModel();
    respTable.setModel(respTblModel);
    pnlTable.add(respTable, BorderLayout.CENTER);
    JScrollPane scrollPane = new JScrollPane(pnlTable);
    pnlCargo.add(scrollPane, BorderLayout.CENTER);

  }

  protected void iniciarNuevaMtto()
  {
    showGuiMtto(this.currentEq);
  }

  private void showGuiMtto(final jFullEquipment Eq)
  {
    logger.info("Equipo que se hara mantenimiento:" + Eq.getName());
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        if (guiMtto == null)
          guiMtto = new GUIMtto(Eq);
        guiMtto.setVisible(true);
      }
    });
  }

  @Override
  public void dataClean()
  {

  }

  @Override
  public void pnlEnable(boolean enable)
  {
    txtNameEq.setEditable(enable);
    txtPlanMtto.setEditable(enable);
    txtQuien.setEditable(enable);
    txtFechaMtto.setEditable(enable);
    txtFalla.setEditable(enable);
    txtComentario.setEditable(enable);
  }

  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {

  }

  @Override
  public IKeylable getActiveElement()
  {
    return active;
  }

  @Override
  public void createElement()
  {
    active = new jReport();
  }

  @Override
  public void querryResponse(final Object content, final Object source)
  {
    if (source instanceof PersistenceManager && content instanceof jFullEquipment) {
      jFullEquipment eq = (jFullEquipment) content;
      if (eq != null) {
        btnMttoNew.setEnabled(true);
        currentEq = eq;
        txtNameEq.setText(currentEq.getName());
        plan = currentEq.getPlan();
        if (plan != null)
          txtPlanMtto.setText(plan.getName() + " version " + plan.getVersion());
      }
    }

    if (source instanceof PersistenceManager && content instanceof List<?>) {
      List<?> list = (List<?>) content;
      if (!list.isEmpty() && list.get(0) instanceof jResponse) {
        respTblModel.clearAll();
        for (Object resp : list) {
          if (resp != null && resp instanceof jResponse) {
            respTblModel.save((jResponse) resp);
          }
        }
      }
    }
  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jReport) {
      active = (jReport) object;
      if (active.getPasa() != null && active.getPasa())
        txtComentario.setText("Mantenci�n pasada con exito");
      else
        txtComentario.setText("Mantenci�n pasada con observaciones");

      if (active != null) {
        Date date = active.getFechahora();
        if (date != null)
          txtFechaMtto.setText(dateFormat.format(date));
        txtFalla.setText(active.getHayFalla() ? active.getFalla() : "No hay fallas reportadas");
        txtQuien.setText(active.getPersona());
        String texto = txtComentario.getText() + "\n" + active.getComentario();
        txtComentario.setText(texto);
        Long idReport = active.getId();
        // Manda a pedir las respuestas asociadasno hay drama
        MSGToBdQuerry querry = new MSGToBdQuerry();
        querry.setContent(jReport.class);
        querry.setSource(this);
        querry.setIdentifier(idReport);
        Factories.getDispatcher().post(querry);
      }
    }
  }

  @Override
  public void elementSaved(Object object)
  {
    if (object instanceof jReport) {
      if (guiMtto != null)
        guiMtto.informacionUser();
    }
  }

  @Override
  public void elementRemoved(Object object)
  {
    if (object instanceof jReport) {
      respTblModel.clearAll();
    }
  }

  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }
}
