package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReference;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.message.MSGToBdQuerry;
import cl.jonnattan.mtto.models.CmteTblModel;
import cl.jonnattan.mtto.models.MTTOCmbModel;
import cl.jonnattan.mtto.persistence.PersistenceManager;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.CmbPlanRenderer;
import cl.jonnattan.mtto.util.MTTOTable;
import cl.jonnattan.mtto.util.MTTOText;

/**
 * Pnel de Ingreso de Naves a la Base de datos
 * 
 * @author Jonnattan Griffiths
 * @since SC1405 SCMMovil -
 * @version 1.1 del 22-11-2019
 */
public class PnlEquipo extends JPanel implements IPnlTuple
{
  private static final long      serialVersionUID = 1L;
  private static Logger          logger           = Logger.getGlobal();
  private final SimpleDateFormat sdf;
  private JComboBox<jPlan>       cmbPlan          = null;
  private MTTOCmbModel<jPlan>    cmbPlanModel     = null;
  private MTTOText               txtSerie         = null;
  private MTTOText               txtParte         = null;
  private JLabel                 lblOPEN          = null;
  private JLabel                 dateInstalation  = null;
  private jFullEquipment         active           = null;
  private MTTOText               txtZona          = null;
  private MTTOText               txtLugar         = null;
  private MTTOTable              table            = null;
  private CmteTblModel           modelCtte        = null;
  private JTextField             txtNameEq        = null;

  public PnlEquipo()
  {
    sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    initialize();
  }

  private void initialize()
  {
    setLayout(new BorderLayout(0, 0));
    setOpaque(false);
    JPanel pnlIzq = new JPanel();
    pnlIzq.setOpaque(false);
    add(pnlIzq, BorderLayout.NORTH);
    pnlIzq.setLayout(new BorderLayout(0, 0));

    JPanel pnlDatos = new JPanel();
    pnlDatos.setOpaque(false);
    pnlDatos.setBorder(new TitledBorder(null, "Datos de Equipo", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    pnlIzq.add(pnlDatos, BorderLayout.NORTH);
    pnlDatos.setLayout(new GridLayout(0, 1, 0, 0));

    JPanel pnlNameEq = new JPanel();
    pnlDatos.add(pnlNameEq);
    pnlNameEq.setOpaque(false);
    pnlNameEq.setLayout(new BorderLayout(0, 0));

    JLabel lblNameEq = new JLabel("Nombre Eq.  ");
    lblNameEq.setFont(CONSTConst.font);
    pnlNameEq.add(lblNameEq, BorderLayout.WEST);

    txtNameEq = new JTextField();
    pnlNameEq.add(txtNameEq, BorderLayout.CENTER);
    txtNameEq.setColumns(10);

    JPanel pnlPlans = new JPanel();
    pnlPlans.setOpaque(false);
    pnlDatos.add(pnlPlans);
    pnlPlans.setLayout(new GridLayout(0, 2, 0, 0));

    JPanel pnlPlan = new JPanel();
    pnlPlan.setOpaque(false);
    pnlPlans.add(pnlPlan);
    pnlPlan.setLayout(new BorderLayout(0, 0));

    JLabel lblPlan = new JLabel("Plan Mtto   ");
    lblPlan.setFont(CONSTConst.font);
    pnlPlan.add(lblPlan, BorderLayout.WEST);

    cmbPlanModel = new MTTOCmbModel<>();
    cmbPlan = new JComboBox<>(cmbPlanModel);
    cmbPlan.setFont(CONSTConst.font);
    cmbPlan.setRenderer(new CmbPlanRenderer<jPlan>());
    // cmbPlan.setEnabled(false);
    pnlPlan.add(cmbPlan, BorderLayout.CENTER);

    JPanel pnlLastMtto = new JPanel();
    pnlLastMtto.setOpaque(false);
    pnlPlans.add(pnlLastMtto);
    pnlLastMtto.setLayout(new BorderLayout(0, 0));
    JLabel lblTot = new JLabel(" Último Rte.");
    lblTot.setFont(CONSTConst.font);
    pnlLastMtto.add(lblTot, BorderLayout.WEST);

    dateInstalation = new JLabel();
    dateInstalation.setToolTipText("Fecha Instalacion");
    dateInstalation.setText("00/00/0000 00:00:00");
    dateInstalation.setFont(CONSTConst.font);
    pnlLastMtto.add(dateInstalation);

    JPanel pnlSerieParte = new JPanel();
    pnlDatos.add(pnlSerieParte);
    pnlSerieParte.setLayout(new GridLayout(0, 2, 0, 0));

    JPanel pnlSerie = new JPanel();
    pnlSerie.setOpaque(false);
    pnlSerie.setLayout(new BorderLayout(0, 0));
    pnlSerieParte.add(pnlSerie);

    JLabel lblSerie = new JLabel("N\u00B0 Serie    ");
    lblSerie.setFont(CONSTConst.font);
    pnlSerie.add(lblSerie, BorderLayout.WEST);

    txtSerie = new MTTOText(15, false, false, true);
    pnlSerie.add(txtSerie);

    JPanel pnlParte = new JPanel();
    pnlParte.setOpaque(false);
    pnlSerieParte.add(pnlParte);
    pnlParte.setLayout(new BorderLayout(0, 0));

    JLabel lblParte = new JLabel(" N\u00B0 Parte    ");
    lblParte.setFont(CONSTConst.font);
    pnlParte.add(lblParte, BorderLayout.WEST);

    txtParte = new MTTOText(30, false, false, true);
    pnlParte.add(txtParte);

    JPanel pnlFile = new JPanel();
    pnlFile.setOpaque(false);
    pnlDatos.add(pnlFile);
    pnlFile.setLayout(new BorderLayout(0, 0));

    JLabel lblRef = new JLabel("Referencia  ");
    lblRef.setFont(CONSTConst.font);
    pnlFile.add(lblRef, BorderLayout.WEST);

    lblOPEN = new JLabel("archivo de referencia");
    lblOPEN.addMouseListener(new MouseListener()
    {

      @Override
      public void mouseClicked(MouseEvent e)
      {
        openRefFile(e.getButton(), ((JLabel) e.getSource()).getText());
      }

      @Override
      public void mousePressed(MouseEvent e)
      {
      }

      @Override
      public void mouseReleased(MouseEvent e)
      {
      }

      @Override
      public void mouseEntered(MouseEvent e)
      {
      }

      @Override
      public void mouseExited(MouseEvent e)
      {
      }

    });
    lblOPEN.setFont(CONSTConst.font);
    pnlFile.add(lblOPEN, BorderLayout.CENTER);

    JPanel panel = new JPanel();
    panel.setOpaque(false);
    panel.setBorder(new TitledBorder(null, "Ubicaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    pnlIzq.add(panel, BorderLayout.CENTER);
    panel.setLayout(new BorderLayout(0, 0));

    JPanel pnlNorte = new JPanel();
    pnlNorte.setOpaque(false);
    panel.add(pnlNorte, BorderLayout.NORTH);
    pnlNorte.setLayout(new GridLayout(2, 1, 0, 0));

    JPanel pnlLoc = new JPanel();
    pnlLoc.setOpaque(false);
    pnlLoc.setLayout(new BorderLayout(0, 0));
    pnlNorte.add(pnlLoc);

    JLabel lblZona = new JLabel("Zona        ");
    lblZona.setFont(CONSTConst.font);
    pnlLoc.add(lblZona, BorderLayout.WEST);

    txtZona = new MTTOText(50);
    pnlLoc.add(txtZona, BorderLayout.CENTER);
    txtZona.setFont(CONSTConst.font);

    JPanel pnlLugar = new JPanel();
    pnlLugar.setOpaque(false);
    pnlNorte.add(pnlLugar);
    pnlLugar.setLayout(new BorderLayout(0, 0));

    JLabel lblLugar = new JLabel("Lugar       ");
    lblLugar.setFont(CONSTConst.font);
    pnlLugar.add(lblLugar, BorderLayout.WEST);

    txtLugar = new MTTOText(50);
    txtLugar.setFont(CONSTConst.font);
    pnlLugar.add(txtLugar, BorderLayout.CENTER);

    JPanel pnlDerecha = new JPanel();
    pnlDerecha.setOpaque(false);
    add(pnlDerecha, BorderLayout.CENTER);
    pnlDerecha.setLayout(new BorderLayout(0, 0));

    JPanel pnlComponentes = new JPanel();
    pnlDerecha.add(pnlComponentes, BorderLayout.CENTER);
    pnlComponentes.setOpaque(false);
    pnlComponentes.setBorder(
        new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)),
            "Componentes", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
    pnlComponentes.setLayout(new GridLayout(0, 1, 0, 0));

    table = new MTTOTable();
    modelCtte = new CmteTblModel();
    table.setModel(modelCtte);

    // TableColumn columna = table.getColumnModel().getColumn(0);
    // columna.setPreferredWidth(20);
    // columna.setMaxWidth(20);
    // columna.setMinWidth(20);

    JScrollPane scroll = new JScrollPane(table);
    pnlComponentes.add(scroll);
  }

  protected void openRefFile(int button, String archivo)
  {
    if (button == MouseEvent.BUTTON1) {
      File file = new File(archivo);
      try {
        if (file.exists())
          Desktop.getDesktop().open(file);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  @Override
  public void dataClean()
  {
    cmbPlan.setSelectedIndex(0);
    txtSerie.setText("");
    txtParte.setText("");
    dateInstalation.setText("");
    txtNameEq.setText("");
    txtLugar.setText("");
    txtZona.setText("");
  }

  @Override
  public void pnlEnable(boolean enable)
  {
    cmbPlan.setEnabled(enable);
    txtSerie.setEnabled(enable);
    txtParte.setEnabled(enable);
    dateInstalation.setEnabled(enable);
    txtNameEq.setEnabled(enable);
    txtLugar.setEnabled(enable);
    txtZona.setEnabled(enable);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {
    List<?> list = (List<?>) dataBd.get(jPlan.class);
    if (list != null && !list.isEmpty())
      if (list.get(0) instanceof jPlan)
        cmbPlanModel.save((List<jPlan>) list);
  }

  @Override
  public IKeylable getActiveElement()
  {
    if (active != null) {
      active.setName(txtNameEq.getText());
      // otras cosas que se puedan cambiar
    }
    return active;
  }

  @Override
  public void createElement()
  {
    active = new jFullEquipment();
  }

  @Override
  public void querryResponse(final Object content, final Object source)
  {

    if (source instanceof PersistenceManager && content instanceof jFullEquipment) {
      active = (jFullEquipment) content;
      txtNameEq.setText(active.getName());
      txtNameEq.setToolTipText("Ingresada al sistema el  " + sdf.format(active.getInstallDate()));
      cmbPlanModel.setSelectedItem(active.getPlan());
      txtParte.setText(active.getPartNumber());
      txtSerie.setText(active.getSerialNumber());
      txtLugar.setText(active.getLocation().getLugar());
      txtZona.setText(active.getLocation().getZona());
      jReference ref = active.getReference();
      if (ref != null) {
        String doc = ref.getPath() + "/" + ref.getName() + "." + ref.getExtend();
        lblOPEN.setText(doc);
      }
      List<jReport> rttes = active.getMaintenances();
      if (rttes != null && !rttes.isEmpty())
        dateInstalation.setText(sdf.format(rttes.get(0).getFechahora()));
      modelCtte.clearAll();
      List<jComponent> cmps = active.getComponents();
      if (cmps != null) {
        for (jComponent cmp : cmps) {
          modelCtte.save(cmp);
        }
      }

    }

  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jEquipment) {
      jEquipment eq = (jEquipment) object;
      logger.info("Equipo seleccionado Seleccionada " + eq);

      MSGToBdQuerry querry = new MSGToBdQuerry();
      querry.setContent(jEquipment.class);
      querry.setSource(this);
      querry.setIdentifier(eq.getId());
      Factories.getDispatcher().post(querry);
    }
  }

  @Override
  public void elementSaved(Object object)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void elementRemoved(Object object)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }
}
