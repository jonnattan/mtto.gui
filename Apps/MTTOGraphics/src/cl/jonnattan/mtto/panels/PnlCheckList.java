package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.MTTOCmbModel;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.CmbPlanRenderer;
import cl.jonnattan.mtto.util.MTTOText;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class PnlCheckList extends JPanel implements IPnlTuple
{
  private static final long   serialVersionUID = 1L;
  private MTTOText            txtTitle         = null;
  private static Logger       logger           = Logger.getGlobal();
  private jChecklist          active           = null;
  private MTTOText            txtDescripcion   = null;
  private JComboBox<jPlan>    cmbPlan          = null;
  private MTTOCmbModel<jPlan> mdlPlan          = null;

  public PnlCheckList()
  {
    super();
    setBorder(new TitledBorder(null, "Descripci\u00F3n del CheckList",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    initialize();
  }

  private void initialize()
  {
    setOpaque(false);
    setLayout(new BorderLayout(0, 0));

    JPanel pnlArriba = new JPanel();
    pnlArriba.setOpaque(false);
    add(pnlArriba, BorderLayout.NORTH);
    pnlArriba.setLayout(new GridLayout(0, 1, 0, 0));

    JPanel pnlNombre = new JPanel();
    pnlArriba.add(pnlNombre);
    pnlNombre.setOpaque(false);
    pnlNombre.setLayout(new BorderLayout(0, 0));
    JLabel lblName = new JLabel("Nombre      ");
    pnlNombre.add(lblName, BorderLayout.WEST);
    lblName.setFont(CONSTConst.font);
    txtTitle = new MTTOText(50);
    pnlNombre.add(txtTitle);
    txtTitle.setFont(CONSTConst.font);

    JPanel pnlDescripcion = new JPanel();
    pnlArriba.add(pnlDescripcion);
    pnlDescripcion.setOpaque(false);
    pnlDescripcion.setLayout(new BorderLayout(0, 0));
    JLabel lblDsc = new JLabel("Descripci\u00F3n ");
    pnlDescripcion.add(lblDsc, BorderLayout.WEST);
    lblDsc.setFont(CONSTConst.font);
    txtDescripcion = new MTTOText(50);
    pnlDescripcion.add(txtDescripcion, BorderLayout.CENTER);
    txtDescripcion.setFont(CONSTConst.font);

    JPanel pnlEquipo = new JPanel();
    pnlEquipo.setOpaque(false);
    pnlEquipo.setLayout(new BorderLayout(0, 0));
    JLabel lblplan = new JLabel("Plan        ");
    lblplan.setFont(CONSTConst.font);
    pnlEquipo.add(lblplan, BorderLayout.WEST);
    mdlPlan = new MTTOCmbModel<>();
    cmbPlan = new JComboBox<>(mdlPlan);
    cmbPlan.setRenderer(new CmbPlanRenderer<jPlan>());
    pnlEquipo.add(cmbPlan, BorderLayout.CENTER);
    pnlArriba.add(pnlEquipo);
  }

  @Override
  public void dataClean()
  {
    txtTitle.setText("");
    txtDescripcion.setText("");
    cmbPlan.setSelectedIndex(-1);
  }

  @Override
  public void pnlEnable(boolean enable)
  {
    txtTitle.setEnabled(enable);
    txtDescripcion.setEnabled(enable);
    if (!enable)
      cmbPlan.setEnabled(enable);
  }

  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {

  }

  @Override
  public IKeylable getActiveElement()
  {
    if(active != null )
    {
      active.setTitulo( txtTitle.getText() );
      active.setDescripcion( txtDescripcion.getText() );
      active.setIdPlan( mdlPlan.getIdSelect() );
    }
    return active;
  }

  @Override
  public void createElement()
  {
    active = new jChecklist();
    cmbPlan.setEnabled(true);
    dataClean();
  }

  @Override
  public void querryResponse(Object content, Object source)
  {
    if (content instanceof List<?>) {
      List<?> list = (List<?>) content;
      if (!list.isEmpty() && list.get(0) instanceof jPlan) {
        mdlPlan.removeAllElements();
        for (Object dato : list)
          mdlPlan.addElement((jPlan) dato);
      }
    }
  }

  @Override
  public void elementRemoved(Object object)
  {

  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jChecklist) {
      active = (jChecklist) object;
      logger.info("Cargo Seleccionado " + object);
      txtTitle.setText(active.getTitulo());
      txtDescripcion.setText(active.getDescripcion());
      mdlPlan.selectToID(active.getIdPlan());
    }
  }

  @Override
  public void elementSaved(Object object)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }

}
