package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.CorrCmbModel;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOText;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c) 2019
 */

public class PnlPersona extends JPanel implements IPnlTuple
{
  private static String[]              cargos           = { "operador",
      "mecanico", "electrico", "electronico" };
  private static Logger                logger           = Logger.getGlobal();
  private static final long            serialVersionUID = 1L;
  private MTTOText                     txtNombre        = null;
  private JComboBox<Integer>           cmbEdad          = null;
  private CorrCmbModel                 cmbEdadModel     = null;
  private JComboBox<String>            cmbCargo         = null;
  private DefaultComboBoxModel<String> cmbCargoModel    = null;
  private jPerson                      active           = null;
  private JPasswordField               txtPass          = null;
  private JTextField                   txtUser          = null;
  private MTTOText                     txtMail          = null;

  public PnlPersona()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    setOpaque(false);
    setLayout(new BorderLayout(0, 0));
    setBorder(new TitledBorder(
        new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos de Usuario",
        TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

    JPanel pnlCenter = new JPanel();
    pnlCenter.setOpaque(false);
    add(pnlCenter, BorderLayout.CENTER);
    pnlCenter.setLayout(new BorderLayout(0, 0));

    JPanel pnlData = new JPanel();
    pnlCenter.add(pnlData, BorderLayout.NORTH);
    pnlData.setOpaque(false);
    pnlData.setLayout(new GridLayout(4, 1, 0, 0));

    JPanel pnlNombre = new JPanel();
    pnlNombre.setOpaque(false);
    pnlData.add(pnlNombre);
    pnlNombre.setLayout(new BorderLayout(0, 0));
    JLabel lblName = new JLabel("Nombre      ");
    pnlNombre.add(lblName, BorderLayout.WEST);
    lblName.setFont(CONSTConst.font);
    txtNombre = new MTTOText(65, true);
    pnlNombre.add(txtNombre, BorderLayout.CENTER);
    JPanel pnlDesc = new JPanel();
    pnlDesc.setOpaque(false);
    pnlData.add(pnlDesc);
    pnlDesc.setLayout(new BorderLayout(0, 0));

    JLabel lblCargo = new JLabel("Cargo       ");
    lblCargo.setFont(CONSTConst.font);
    pnlDesc.add(lblCargo, BorderLayout.WEST);

    cmbCargoModel = new DefaultComboBoxModel<>(cargos);
    cmbCargo = new JComboBox<String>(cmbCargoModel);
    cmbCargo.setOpaque(false);
    pnlDesc.add(cmbCargo, BorderLayout.CENTER);

    JPanel pnlCredencial = new JPanel();
    pnlCredencial.setOpaque(false);
    pnlData.add(pnlCredencial);
    pnlCredencial.setLayout(new GridLayout(0, 2, 0, 0));

    JPanel pnlPass = new JPanel();
    pnlPass.setOpaque(false);
    pnlPass.setLayout(new BorderLayout(0, 0));
    JLabel lblPassw = new JLabel("Contrase\u00F1a");
    lblPassw.setFont(CONSTConst.font);
    pnlPass.add(lblPassw, BorderLayout.WEST);
    txtPass = new JPasswordField();
    pnlPass.add(txtPass, BorderLayout.CENTER);
    JPanel plnUser = new JPanel();
    plnUser.setOpaque(false);
    plnUser.setLayout(new BorderLayout(0, 0));
    txtUser = new JTextField();
    plnUser.add(txtUser, BorderLayout.CENTER);
    txtUser.setFont(CONSTConst.font);
    txtUser.setColumns(10);
    JLabel lblUsuario = new JLabel("Usuario     ");
    lblUsuario.setFont(CONSTConst.font);
    plnUser.add(lblUsuario, BorderLayout.WEST);

    pnlCredencial.add(plnUser);
    pnlCredencial.add(pnlPass);

    JPanel pnlCorreo = new JPanel();
    pnlCorreo.setOpaque(false);

    pnlCorreo.setLayout(new BorderLayout(0, 0));

    JLabel lblMail = new JLabel("Correo      ");
    lblMail.setFont(CONSTConst.font);
    pnlCorreo.add(lblMail, BorderLayout.WEST);

    txtMail = new MTTOText(65);
    pnlCorreo.add(txtMail, BorderLayout.CENTER);
    txtMail.setColumns(10);

    JPanel pnlMailEdad = new JPanel();
    pnlMailEdad.setOpaque(false);

    pnlMailEdad.setLayout(new GridLayout(0, 2, 0, 0));

    JPanel pnlEdad = new JPanel();
    pnlEdad.setOpaque(false);

    pnlEdad.setLayout(new BorderLayout(0, 0));

    JLabel lblEdad = new JLabel("Edad      ");
    lblEdad.setFont(CONSTConst.font);
    pnlEdad.add(lblEdad, BorderLayout.WEST);

    pnlData.add(pnlMailEdad);
    pnlMailEdad.add(pnlCorreo);
    pnlMailEdad.add(pnlEdad);

    cmbEdadModel = new CorrCmbModel( 15, 60 );
    cmbEdad = new JComboBox<>(cmbEdadModel);
    cmbEdad.setFont(CONSTConst.font);
    pnlEdad.add(cmbEdad, BorderLayout.CENTER);

  }

  @Override
  public void dataClean()
  {
    txtNombre.setText("");
    cmbCargo.setSelectedIndex(0);
    txtPass.setText("");
    txtMail.setText("");
    txtUser.setText("");
    cmbEdad.setSelectedIndex(0);
    active = null;
  }

  @Override
  public void pnlEnable(boolean enable)
  {
    txtNombre.setEnabled(enable);
    cmbCargo.setEnabled(enable);
    txtPass.setEnabled(enable);
    txtUser.setEnabled(enable);
    txtMail.setEnabled(enable);
    cmbEdad.setEnabled(enable);
  }

  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {

  }

  @SuppressWarnings("deprecation")
  @Override
  public IKeylable getActiveElement()
  {
    if (active != null) {
      active.setNombre(txtNombre.getText());
      active.setCargo(cmbCargo.getSelectedItem().toString());
      active.setCorreo(txtMail.getText());
      active.setEdad((Integer) cmbEdadModel.getSelectedItem());
      active.setUser(txtUser.getText());
      active.setPass(txtPass.getText());
    }

    return active;
  }

  @Override
  public void createElement()
  {
    active = new jPerson();
    dataClean();
  }

  @Override
  public void querryResponse(Object content, Object source)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jPerson) {
      active = (jPerson) object;

      txtNombre.setText(active.getNombre());
      cmbCargo.setSelectedItem(active.getCargo());
      txtPass.setText(active.getPass());
      txtUser.setText(active.getUser());
      cmbEdad.setSelectedItem(active.getEdad());
      txtMail.setText(active.getCorreo());

      logger.info("Componente " + active + " Seleccionado");
    }
  }

  @Override
  public void elementSaved(Object object)
  {

  }

  @Override
  public void elementRemoved(Object object)
  {
    // TODO Auto-generated method stub

  }
  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }
}
