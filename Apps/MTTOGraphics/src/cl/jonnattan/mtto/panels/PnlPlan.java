package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOHourDateEditor;
import cl.jonnattan.mtto.util.MTTOText;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class PnlPlan extends JPanel implements IPnlTuple
{
  private static final long  serialVersionUID = 1L;
  private MTTOText           txtVersion       = null;
  private MTTOText           txtName          = null;
  private MTTOHourDateEditor dateUpdate       = null;
  private jPlan              active           = null;
  private static Logger      logger           = Logger.getGlobal();

  public PnlPlan()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    setOpaque(false);
    setLayout(new BorderLayout(0, 0));
    JPanel pnlAttr = new JPanel();
    pnlAttr.setBorder(new TitledBorder(
        new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255),
            new Color(160, 160, 160)),
        "Datos de Planes", TitledBorder.LEADING, TitledBorder.TOP, null,
        new Color(0, 0, 0)));
    pnlAttr.setOpaque(false);
    add(pnlAttr, BorderLayout.CENTER);
    pnlAttr.setLayout(new BorderLayout(0, 0));
    JPanel pnlDatos = new JPanel();
    pnlDatos.setOpaque(false);
    pnlAttr.add(pnlDatos, BorderLayout.NORTH);
    pnlDatos.setLayout(new GridLayout(0, 1, 0, 0));

    JPanel pnlNombre = new JPanel();
    pnlNombre.setOpaque(false);
    pnlDatos.add(pnlNombre);
    pnlNombre.setLayout(new BorderLayout(0, 0));
    JLabel lblName = new JLabel("Nombre Plan ");
    pnlNombre.add(lblName, BorderLayout.WEST);
    lblName.setFont(CONSTConst.font);
    txtName = new MTTOText(30, true);
    txtName.setHorizontalAlignment(SwingConstants.LEFT);
    pnlNombre.add(txtName, BorderLayout.CENTER);
    txtName.setFont(CONSTConst.font);

    JPanel pnlVersion = new JPanel();
    pnlVersion.setOpaque(false);
    pnlDatos.add(pnlVersion);
    pnlVersion.setLayout(new BorderLayout(0, 0));
    JLabel lblCode = new JLabel("Versi\u00F3n     ");
    lblCode.setHorizontalAlignment(SwingConstants.CENTER);
    pnlVersion.add(lblCode, BorderLayout.WEST);
    lblCode.setFont(CONSTConst.font);
    txtVersion = new MTTOText(20, false);
    txtVersion.setHorizontalAlignment(SwingConstants.LEFT);
    pnlVersion.add(txtVersion);
    txtVersion.setFont(CONSTConst.font);

    dateUpdate = new MTTOHourDateEditor();
    dateUpdate.setWithClock(true, false);
    dateUpdate.setDate(new Timestamp( System.currentTimeMillis() ));
    
    JPanel panel = new JPanel();
    pnlDatos.add(panel);
    panel.setLayout(new BorderLayout(0, 0));

    JLabel lblUpdate = new JLabel("Actualizado ");
    lblUpdate.setFont(CONSTConst.font);

    panel.add(lblUpdate, BorderLayout.WEST);
    panel.add(dateUpdate);

  }

  @Override
  public void dataClean()
  {
    txtName.setText("");
    txtVersion.setText("");
  }

  @Override
  public void pnlEnable(boolean enable)
  {
    txtName.setEnabled(enable);
    txtVersion.setEnabled(enable);
  }

  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {
    //
  }

  @Override
  public IKeylable getActiveElement()
  {
    if( active != null )
    {
      active.setName(txtName.getText());
      active.setVersion(txtVersion.getText());
      active.setRevision( new Date() );
    }    
    return active;
  }

  @Override
  public void createElement()
  {
    active = new jPlan();
    dateUpdate.setDate(new Timestamp(System.currentTimeMillis()));
  }

  @Override
  public void querryResponse(Object content, Object source)
  {

  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jPlan) {
      active = (jPlan) object;
      logger.info("jPlan " + active + " Seleccionado");
      txtName.setText(active.getName());
      txtVersion.setText(active.getVersion());
      Date date = active.getRevision();
      if (date != null)
        dateUpdate.setDate(new Timestamp(date.getTime()));
    }
  }

  @Override
  public void elementSaved(Object object)
  {

  }

  @Override
  public void elementRemoved(Object object)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }
}
