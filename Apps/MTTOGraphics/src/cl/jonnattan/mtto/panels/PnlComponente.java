package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.CorrCmbModel;
import cl.jonnattan.mtto.models.MTTOCmbModel;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.CmbEqRenderer;
import cl.jonnattan.mtto.util.MTTOText;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 24-04-2019
 * @version 1.0 Copyright(c) 2019
 */

public class PnlComponente extends JPanel implements IPnlTuple
{
  private static final long        serialVersionUID = 1L;
  private static Logger            logger           = Logger.getGlobal();
  private MTTOText                 txtNombre        = null;
  private MTTOText                 txtDescripcion   = null;
  private MTTOText                 txtParte         = null;
  private MTTOText                 txtSerie         = null;
  private JComboBox<Integer>       cmbCantidad      = null;
  private CorrCmbModel             mdlCantidad      = null;
  private JComboBox<jEquipment>    cmbEq            = null;
  private MTTOCmbModel<jEquipment> mdlEq            = null;
  private jComponent               active           = null;

  public PnlComponente()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    setOpaque(false);
    setLayout(new BorderLayout(0, 0));
    setBorder(
        new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
            "Descripci\u00F3n del Componente", TitledBorder.LEADING,
            TitledBorder.TOP, null, new Color(0, 0, 0)));

    JPanel pnlCenter = new JPanel();
    pnlCenter.setOpaque(false);
    add(pnlCenter, BorderLayout.CENTER);
    pnlCenter.setLayout(new BorderLayout(0, 0));

    JPanel pnlData = new JPanel();
    pnlCenter.add(pnlData, BorderLayout.NORTH);
    pnlData.setOpaque(false);
    pnlData.setLayout(new GridLayout(7, 1, 0, 0));

    JPanel pnlNombre = new JPanel();
    pnlNombre.setOpaque(false);
    pnlData.add(pnlNombre);
    pnlNombre.setLayout(new BorderLayout(0, 0));
    JLabel lblName = new JLabel("Nombre      ");
    pnlNombre.add(lblName, BorderLayout.WEST);
    lblName.setFont(CONSTConst.font);
    txtNombre = new MTTOText(65);
    pnlNombre.add(txtNombre, BorderLayout.CENTER);
    JPanel pnlDesc = new JPanel();
    pnlDesc.setOpaque(false);
    pnlData.add(pnlDesc);
    pnlDesc.setLayout(new BorderLayout(0, 0));

    JLabel lblDescrp = new JLabel("Descripci\u00F3n ");
    lblDescrp.setFont(CONSTConst.font);
    pnlDesc.add(lblDescrp, BorderLayout.WEST);

    txtDescripcion = new MTTOText(50);
    pnlDesc.add(txtDescripcion, BorderLayout.CENTER);

    JPanel pnlEquipo = new JPanel();
    pnlEquipo.setOpaque(false);
    pnlEquipo.setLayout(new BorderLayout(0, 0));
    JLabel lblEq = new JLabel("Equipo      ");
    lblEq.setFont(CONSTConst.font);
    pnlEquipo.add(lblEq, BorderLayout.WEST);
    mdlEq = new MTTOCmbModel<>();
    cmbEq = new JComboBox<>(mdlEq);
    cmbEq.setRenderer(new CmbEqRenderer<jEquipment>());
    pnlEquipo.add(cmbEq, BorderLayout.CENTER);
    pnlData.add(pnlEquipo);

    JPanel pnlParte = new JPanel();
    pnlParte.setOpaque(false);
    pnlData.add(pnlParte);
    pnlParte.setLayout(new BorderLayout(0, 0));

    JLabel lblParte = new JLabel("N\u00FAmero Parte");
    lblParte.setFont(CONSTConst.font);
    pnlParte.add(lblParte, BorderLayout.WEST);

    txtParte = new MTTOText(50);
    pnlParte.add(txtParte, BorderLayout.CENTER);
    txtParte.setColumns(10);

    JPanel pnlSerie = new JPanel();
    pnlSerie.setOpaque(false);
    pnlData.add(pnlSerie);
    pnlSerie.setLayout(new BorderLayout(0, 0));

    JLabel lblSerie = new JLabel("N\u00FAmero Serie");
    lblSerie.setFont(CONSTConst.font);
    pnlSerie.add(lblSerie, BorderLayout.WEST);

    txtSerie = new MTTOText(50);
    pnlSerie.add(txtSerie, BorderLayout.CENTER);
    txtSerie.setColumns(10);

    JPanel pnlCantidad = new JPanel();
    pnlCantidad.setOpaque(false);
    pnlCantidad.setLayout(new BorderLayout(0, 0));
    JLabel lblCantidad = new JLabel("Cantidad    ");
    lblCantidad.setFont(CONSTConst.font);
    pnlCantidad.add(lblCantidad, BorderLayout.WEST);
    mdlCantidad = new CorrCmbModel(30);
    cmbCantidad = new JComboBox<>(mdlCantidad);
    cmbCantidad.setFont(CONSTConst.font);
    pnlCantidad.add(cmbCantidad, BorderLayout.CENTER);
    pnlData.add(pnlCantidad);

  }

  @Override
  public void dataClean()
  {
    txtNombre.setText("");
    txtDescripcion.setText("");
    txtParte.setText("");
    txtSerie.setText("");
    cmbCantidad.setSelectedIndex(0);
    cmbEq.setSelectedIndex(0);
  }

  @Override
  public void pnlEnable(boolean enable)
  {
    txtNombre.setEnabled(enable);
    txtDescripcion.setEnabled(enable);
    txtParte.setEnabled(enable);
    txtSerie.setEnabled(enable);
    cmbCantidad.setEnabled(enable);
    if (!enable)
      cmbEq.setEnabled(enable);
  }

  @Override
  public void fillPanel(final Map<?, ?> dataBd)
  {
    List<?> list = (List<?>) dataBd.get(jEquipment.class);
    if (list != null && !list.isEmpty() && list.get(0) instanceof jEquipment) {
      mdlEq.removeAllElements();
      for (Object o : list)
        mdlEq.addElement((jEquipment) o);
    }
  }

  @Override
  public IKeylable getActiveElement()
  {
    if(active != null)
    {
      active.setCantidad( (Integer) cmbCantidad.getSelectedItem() );
      active.setDescripcion( txtDescripcion.getText() );
      active.setIdEq( mdlEq.getIdSelect() );
      active.setName( txtNombre.getText() );
      active.setPartnumber( txtParte.getText() );
      active.setSerialNumber( txtSerie.getText() );
    }
    
    return active;
  }

  @Override
  public void createElement()
  {
    active = new jComponent();
    cmbEq.setEnabled(true);
    dataClean();
  }

  @Override
  public void querryResponse(Object content, Object source)
  {
    
  }

  @Override
  public void elementSelected(Object object)
  {
    if (object instanceof jComponent) {
      active = (jComponent) object;
      logger.info("Componente " + active + " Seleccionado");
      txtNombre.setText(active.getName());
      txtDescripcion.setText(active.getDescripcion());
      txtParte.setText(active.getPartnumber());
      txtSerie.setText(active.getSerialNumber());
      mdlEq.setSelectedItem(active.getIdEq());
    }
  }

  @Override
  public void elementSaved(Object object)
  {
    
  }

  @Override
  public void elementRemoved(Object object)
  {
    
  }
  @Override
  public void repaintForChangeLook()
  {
    repaint();
  }
}
