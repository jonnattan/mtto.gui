package cl.jonnattan.mtto.panels;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.gui.GUIMain;
import cl.jonnattan.mtto.message.MSGToBdQuerry;
import cl.jonnattan.mtto.message.MSGToBdQuerry.EQuerryAction;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOWait;

/**
 * Panel de Botones
 * 
 * @author Jonnattan Griffiths
 * @since 23/08/2019
 * @version 1.0 Copyright(c) 2019
 */
public class PnlBotones extends JPanel
{
  private static final long serialVersionUID = 1L;
  private JButton           btnUpdate;
  private JButton           btnDelete;
  private JButton           btnCreate;
  private JPanel            panel;
  private JPanel            pnlUser;
  private JLabel            lblUser;

  public PnlBotones()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    setBorder(null);
    btnUpdate = new JButton("Actualizar");
    btnUpdate.setToolTipText("Actualiza el elemento seleccionado");
    btnUpdate.setFont(CONSTConst.font);
    btnUpdate.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent arg0)
      {
        processRequeriment((JButton) arg0.getSource());
      }
    });

    btnDelete = new JButton("Eliminar");
    btnDelete.setToolTipText("Elimina el elemento seleccionado");
    btnDelete.setFont(CONSTConst.font);
    btnDelete.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent arg0)
      {
        processRequeriment((JButton) arg0.getSource());
      }
    });

    btnCreate = new JButton("Crear");
    btnCreate.setToolTipText("Crea un elemento");
    btnCreate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        processRequeriment((JButton) arg0.getSource());
      }
    });
    setLayout(new BorderLayout(0, 0));
    btnCreate.setFont(CONSTConst.font);

    panel = new JPanel();
    panel.add(btnCreate);
    panel.add(btnUpdate);
    panel.add(btnDelete);

    //add(panel, BorderLayout.NORTH);
    panel.setLayout(new GridLayout(1, 0, 0, 0));

    pnlUser = new JPanel();
    add(pnlUser, BorderLayout.SOUTH);
    pnlUser.setLayout(new BorderLayout(0, 0));

    lblUser = new JLabel(GUIMain.persona != null
        ? GUIMain.persona.getCargo() + " " + GUIMain.persona.getNombre()
        : "Usuario no registrado");
    pnlUser.add(lblUser, BorderLayout.SOUTH);
  }

  /**
   * Habilita o desHabilita los botones de la aplicacion.
   * 
   * @param stateBtnSms
   * @param stateBtnChat
   * @param stateBtnMail
   */
  public void changeStatusBtns(final boolean state1, final boolean state2,
      final boolean state3)
  {
    btnUpdate.setEnabled(state1);
    btnDelete.setEnabled(state2);
    btnCreate.setEnabled(state3);
  }

  protected void processRequeriment(final JButton btn)
  {
    if (btn == btnCreate) {
      MTTOWait.getInstance().iniciar(this);
      MSGToBdQuerry message = new MSGToBdQuerry();
      message.seteType(EQuerryAction.CREATION);
      message.setSource(this);
      Factories.getDispatcher().post(message);

    } else if (btn == btnUpdate) {
      MSGToBdQuerry message = new MSGToBdQuerry();
      message.seteType(EQuerryAction.UPDATE);
      message.setSource(this);
      Factories.getDispatcher().post(message);

    } else if (btn == btnDelete) {
      MSGToBdQuerry message = new MSGToBdQuerry();
      message.seteType(EQuerryAction.DELETE);
      message.setSource(this);
      Factories.getDispatcher().post(message);
    }
  }

  /**
   * Explora la carpeta principal
   * 
   * @return
   */
  /*
   * private String getPathFileXML() { String fileName = null; String
   * defaultFolder = Factories.getParameters().getDataSetPath(); JFileChooser
   * fchooser = new JFileChooser(); FileNameExtensionFilter filter = new
   * FileNameExtensionFilter("Archivos de DataSet (DS, ds)", "ds", "DS");
   * fchooser.setFileFilter(filter);
   * fchooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
   * fchooser.setMultiSelectionEnabled(false); fchooser.setCurrentDirectory(new
   * File(defaultFolder)); int res = fchooser.showOpenDialog(null); if (res ==
   * JFileChooser.APPROVE_OPTION) { File file = fchooser.getSelectedFile();
   * fileName = file.getAbsolutePath(); }
   * 
   * if (res == JFileChooser.CANCEL_OPTION) { fileName = null; } return
   * fileName; }
   */
}
