package cl.jonnattan.mtto.abstracs;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;

/**
 * Modelo de tabla para presentar las unidades que participen en un chat.
 * 
 * @author Jonnattan Griffiths
 * @since 02/08/2019
 * @version 1.0 Copyright(c)
 */
public abstract class AMttoTblModel<T extends AbsKeylable> extends
    AbstractTableModel
{
  private static final long serialVersionUID = 1L;
  private List<T>           lista            = null;
  
  public AMttoTblModel()
  {
    super();
    lista = new ArrayList<T>();
  }
  
  @Override
  public boolean isCellEditable(int arg0, int arg1)
  {
    return false;
  }
  
  @Override
  public void addTableModelListener(TableModelListener arg0)
  {
    super.addTableModelListener(arg0);
  }
  
  @Override
  public int getRowCount()
  {
    return lista.size();
  }
  
  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return getValue(getData(rowIndex), columnIndex);
  }
  
  /** Obtiene el valor en la tabla */
  protected abstract Object getValue(final T dato, final int columnIndex);
  
  /** borra toda la lista */
  public void clearAll()
  {
    lista.clear();
    fireTableDataChanged();
  }
  
  /** Retorna el objeto en la posicion de la fila que corresponda */
  public T getData(int rowIndex)
  {
    T object = null;
    try
    {
      object = lista.get(rowIndex);
    }
    catch (IndexOutOfBoundsException ex)
    {
      object = null;
    }
    return object;
  }
  
  /** Retorna todos los objetos de la lista */
  public List<T> getList()
  {
    return lista;
  }
  
  public T findObject(int idRep)
  {
    for (T dato : getList())
      if (dato.getId() == idRep)
        return dato;
    return null;
  }
  
  /**
   * Inseta una fila en la tabla
   * 
   * @param unit
   */
  public void save(T unit)
  {
    if (unit != null)
    {
      try
      {
        int index = lista.indexOf(unit);
        if (index >= 0)
          lista.set(index, unit);
        else
          lista.add(unit);
        fireTableDataChanged();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }
  
  /**
   * Elimina el elemento en esa posicion
   * 
   * @param aData
   */
  public void deleteRow(int index)
  {
    try
    {
      lista.remove(index);
      fireTableDataChanged();
    }
    catch (IndexOutOfBoundsException ex)
    {
      ex.printStackTrace();
    }
  }
  
  /**
   * Elimina el objeto de la lista
   * 
   * @param unit
   */
  public void deleteRow(T unit)
  {
    try
    {
      lista.remove(unit);
      fireTableDataChanged();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
  
}
