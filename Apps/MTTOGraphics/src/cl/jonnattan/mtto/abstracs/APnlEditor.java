package cl.jonnattan.mtto.abstracs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.message.MSGFromBdDeleted;
import cl.jonnattan.mtto.message.MSGFromBdQuerry;
import cl.jonnattan.mtto.message.MSGFromBdSaved;
import cl.jonnattan.mtto.message.MSGInternalSelected;
import cl.jonnattan.mtto.message.MSGLookRefresh;
import cl.jonnattan.mtto.message.MSGStart;
import cl.jonnattan.mtto.message.MSGToBdDelete;
import cl.jonnattan.mtto.message.MSGToBdSave;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOTable;
import cl.jonnattan.mtto.util.MTTOWait;

/**
 * Clase que describe los paneles que seran administradores de la base de datos
 * 
 * @author Jonnattan Griffiths
 * @since 28/12/2019
 * @version 1.0 Copyright(c) - 2019
 */
public abstract class APnlEditor extends JPanel implements IPnlEditor
{
  protected static Logger   logger           = Logger.getGlobal();
  private static final long serialVersionUID = 1L;

  private MTTOTable                             table          = null;
  private JLabel                                lblNumber      = null;
  private int                                   rowIndexSelect = -1;
  private boolean                               inEditionMode  = false;
  private JButton                               btnAgregar     = null;
  private JButton                               btnEditar      = null;
  private JButton                               btnGuardar     = null;
  private JButton                               btnEliminar    = null;
  private JButton                               btnCancelar    = null;
  private JPanel                                pnlBtnes       = null;
  private static Map<Class<?>, List<IKeylable>> datos          = null;

  public APnlEditor(boolean isTabElement)
  {
    super();
    datos = new HashMap<Class<?>, List<IKeylable>>();
    Factories.getDispatcher().subscribe(this);
    if (isTabElement)
      initializeTabElement();
    else
      initializePnl();
  }

  protected void HideButtons()
  {
    btnAgregar.setEnabled(false);
    btnEditar.setEnabled(false);
    btnGuardar.setEnabled(false);
    btnEliminar.setEnabled(false);
    btnCancelar.setEnabled(false);
    pnlBtnes.setVisible( false );
  }

  private void initializeTabElement()
  {
    setOpaque(false);
    setLayout(new GridLayout(0, 2, 0, 0));
    // -----------------------------------------
    JPanel pnlDerecha = new JPanel();
    pnlDerecha.setOpaque(false);
    pnlDerecha.setLayout(new BorderLayout());
    add(pnlDerecha);

    JScrollPane scrollPane = new JScrollPane(getTable());
    scrollPane.setOpaque(false);
    scrollPane.setHorizontalScrollBarPolicy(
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    pnlDerecha.add(scrollPane, BorderLayout.CENTER);

    lblNumber = new JLabel("numero de elementos");
    setNumeroElementos(0);
    pnlDerecha.add(lblNumber, BorderLayout.SOUTH);
    lblNumber.setFont(CONSTConst.font);
    lblNumber.setHorizontalAlignment(SwingConstants.CENTER);

    JPanel pnlCenter = new JPanel();
    pnlCenter.setOpaque(false);
    add(pnlCenter);
    pnlCenter.setLayout(new BorderLayout(0, 0));

    pnlBtnes = new JPanel();
    pnlCenter.add(pnlBtnes, BorderLayout.SOUTH);
    pnlBtnes.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING,
        TitledBorder.TOP, null, null));
    pnlBtnes.setOpaque(false);
    pnlBtnes.setLayout(new GridLayout(0, 5, 0, 0));

    btnAgregar = new JButton("Agregar");
    btnEditar = new JButton("Editar");
    btnGuardar = new JButton("Guardar");
    btnEliminar = new JButton("Eliminar");
    btnCancelar = new JButton("Cancelar");

    btnAgregar.setFont(CONSTConst.font);
    btnAgregar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });
    pnlBtnes.add(btnAgregar);

    btnEditar.setFont(CONSTConst.font);
    pnlBtnes.add(btnEditar);
    btnEditar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnGuardar.setFont(CONSTConst.font);
    pnlBtnes.add(btnGuardar);
    btnGuardar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnEliminar.setFont(CONSTConst.font);
    pnlBtnes.add(btnEliminar);
    btnEliminar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnCancelar.setFont(CONSTConst.font);
    pnlBtnes.add(btnCancelar);

    btnCancelar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });
    setBtnStates(true, false, false, false, false);

    // Se pone la parte de arriba solo si existe
    IPnlTuple pnlEditor = getEditorPnl();
    if (pnlEditor != null && pnlEditor instanceof JPanel) {
      pnlEditor.pnlEnable(false);
      pnlCenter.add((JPanel) pnlEditor, BorderLayout.CENTER);
    }
  }

  private void initializePnl()
  {
    setLayout(new BorderLayout(0, 0));
    // Se pone la parte de arriba solo si existe
    IPnlTuple pnlEditor = getEditorPnl();
    if (pnlEditor != null && pnlEditor instanceof JPanel) {
      pnlEditor.pnlEnable(false);
      add((JPanel) pnlEditor, BorderLayout.NORTH);
    }
    // -----------------------------------------
    JPanel pnlCenter = new JPanel();
    pnlCenter.setOpaque(false);
    pnlCenter.setLayout(new BorderLayout());
    add(pnlCenter, BorderLayout.CENTER);

    JScrollPane scrollPane = new JScrollPane(getTable());
    scrollPane.setOpaque(false);
    scrollPane.setHorizontalScrollBarPolicy(
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    pnlCenter.add(scrollPane, BorderLayout.CENTER);

    lblNumber = new JLabel("numero de elementos");
    setNumeroElementos(0);
    pnlCenter.add(lblNumber, BorderLayout.NORTH);
    lblNumber.setFont(CONSTConst.font);
    lblNumber.setHorizontalAlignment(SwingConstants.CENTER);

    JPanel pnlBtnes = new JPanel();
    pnlBtnes.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING,
        TitledBorder.TOP, null, null));
    pnlBtnes.setOpaque(false);
    add(pnlBtnes, BorderLayout.SOUTH);
    pnlBtnes.setLayout(new GridLayout(0, 5, 0, 0));

    btnAgregar = new JButton("Agregar");
    btnEditar = new JButton("Editar");
    btnGuardar = new JButton("Guardar");
    btnEliminar = new JButton("Eliminar");
    btnCancelar = new JButton("Cancelar");

    btnAgregar.setFont(CONSTConst.font);
    btnAgregar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });
    pnlBtnes.add(btnAgregar);

    btnEditar.setFont(CONSTConst.font);
    pnlBtnes.add(btnEditar);
    btnEditar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnGuardar.setFont(CONSTConst.font);
    pnlBtnes.add(btnGuardar);
    btnGuardar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnEliminar.setFont(CONSTConst.font);
    pnlBtnes.add(btnEliminar);
    btnEliminar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });

    btnCancelar.setFont(CONSTConst.font);
    pnlBtnes.add(btnCancelar);
    btnCancelar.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        analizeAction((JButton) event.getSource());
      }
    });
    setBtnStates(true, false, false, false, false);
  }

  public void setNumeroElementos(final int number)
  {
    String text = String.format("Exiten %03d Elementos en la Tabla", number);
    lblNumber.setText(text);
  }

  /**
   * Analiza el boton presionado y realiza la accion correspondinte
   * 
   * @param source
   */
  protected void analizeAction(final JButton boton)
  {
    if (boton == btnAgregar)
      addElementMode();
    else if (boton == btnEditar)
      modElementMode();
    else if (boton == btnGuardar)
      saveElement();
    else if (boton == btnEliminar)
      deleteSelectElement();
    else if (boton == btnCancelar)
      cancelOperation();
    else
      System.out.print("Operacion No Valida");
  }

  /** Retorna Pnl de edicion */
  protected abstract IPnlTuple getEditorPnl();

  public JTable getTable()
  {
    if (table == null) {
      table = new MTTOTable();
      table.addMouseListener(new MouseAdapter()
      {
        @Override
        public void mouseClicked(MouseEvent mouseEvent)
        {
          if (!mouseEvent.isMetaDown() && !mouseEvent.isAltDown()
              && !mouseEvent.isShiftDown()) {
            TableModel model = table.getModel();
            if (!inEditionMode) {
              rowIndexSelect = table.rowAtPoint(mouseEvent.getPoint());
              if (rowIndexSelect > -1) {
                // se selecciono un elemento, pero no necesariamente es
                // el mismo de la fila q tiene el modelo
                if (model instanceof AMttoTblModel<?>) {
                  // Aca puede haber un problema cuando la lista se ha ordenado
                  // de forma distinta a su orden inicial
                  IKeylable active = ((AMttoTblModel<?>) model)
                      .getData(rowIndexSelect);
                  if (active != null) {
                    setBtnStates(true, true, false, true, false);
                    selectElement(active);
                  }
                  // Aca hay que poner codigo cuando se trata de campos boolean
                  // que necesitan que cambien
                }
              } else {
                logger.warning("No se ha seleccionado nada...");
                setBtnStates(true, false, false, false, false);
                // No hay elemento seleccionado
              }
            } else {
              String msg = "Finalize proceso de edici�n antes de continuar";
              JOptionPane.showMessageDialog(APnlEditor.this, msg,
                  "Sr. Operador", JOptionPane.ERROR_MESSAGE);
              table.getSelectionModel().setSelectionInterval(rowIndexSelect,
                  rowIndexSelect);
            }
          } else {
            logger.finer("Boton deshabilitado...");
          }
        }
      });
    }
    return table;
  }

  @Override
  public boolean isWork()
  {
    return inEditionMode;
  }

  /**
   * Elimina el elemento seleccionado
   */
  public void deleteSelectElement()
  {
    IKeylable active = getEditorPnl().getActiveElement();
    if (active != null) {
      // Se elimina de la tabla
      if (rowIndexSelect >= 0) {
        TableModel model = getTable().getModel();
        ((AMttoTblModel<?>) model).deleteRow(rowIndexSelect);
        // Se envia el Mensajes a la base de datos
        logger.info("Se Elimina fila[" + rowIndexSelect + "]");

        MSGToBdDelete msgDelete = new MSGToBdDelete();
        msgDelete.setContent(active);
        msgDelete.setSource(this);
        Factories.getDispatcher().post(msgDelete);
        // Se normalisa el pnl
      } else {
        logger.warning("Elemento seleccionado es [" + rowIndexSelect + "]");
      }
      getEditorPnl().pnlEnable(false);
      inEditionMode = false;
      rowIndexSelect = -1;
      selectElement(null);
      setBtnStates(true, false, false, false, false);
      table.clearSelection();
    }
  }

  private void modElementMode()
  {
    if (!inEditionMode) {
      logger.info("Se inica modo de edicion de elemento");
      inEditionMode = true;
      getEditorPnl().pnlEnable(true);
      setBtnStates(false, false, true, false, true);
    }
  }

  private void addElementMode()
  {
    inEditionMode = true;
    getEditorPnl().pnlEnable(true);
    selectElement(null);
    logger.info("Se inicia modo para crear elemento");
    getEditorPnl().createElement();
    setBtnStates(false, false, true, false, true);
    table.clearSelection();
  }

  private void cancelOperation()
  {
    if (inEditionMode) {
      logger.info("Operacion cancelada");
      inEditionMode = false;
      rowIndexSelect = -1;
      getEditorPnl().pnlEnable(false);
      selectElement(null);
      setBtnStates(true, false, false, false, false);
      table.clearSelection();
    }
  }

  private void saveElement()
  {
    IKeylable active = getEditorPnl().getActiveElement();
    // Se envia el Mensajes a la base de datos
    MSGToBdSave msgSave = new MSGToBdSave();
    msgSave.setContent(active);
    msgSave.setSource(this);
    Factories.getDispatcher().post(msgSave);

    getEditorPnl().pnlEnable(false);
    getEditorPnl().dataClean();

    inEditionMode = false;
    rowIndexSelect = -1;
    setBtnStates(true, false, false, false, false);
    table.clearSelection();
  }

  /**
   * Cambia el estado de los botones.
   * 
   * @param btnAdd    Boton Agregar
   * @param btnUpd    Boton Modificar
   * @param btnSave   Boton Guardar
   * @param btnDel    Boton Eliminar
   * @param btnCancel Boton Cancelar
   */
  private void setBtnStates(final boolean btnAdd, final boolean btnUpd,
      final boolean btnSave, final boolean btnDel, final boolean btnCancel)
  {
    btnAgregar.setEnabled(btnAdd);
    btnEditar.setEnabled(btnUpd);
    btnGuardar.setEnabled(btnSave);
    btnEliminar.setEnabled(btnDel);
    btnCancelar.setEnabled(btnCancel);
  }

  private void selectElement(IKeylable element)
  {
    MSGInternalSelected msgSelect = new MSGInternalSelected();
    msgSelect.setContent(element);
    msgSelect.setSource(this);
    Factories.getDispatcher().post(msgSelect);
  }

  /**
   * Metodo que indica la tupla que es seleccionada
   * 
   * @param nave
   */
  protected void selectedElement(final IKeylable tuple)
  {
    getEditorPnl().elementSelected(tuple);
  }

  /**
   * Notifica el guardado de un elemento
   * 
   * @param tuple
   */
  private void bdSavedElement(final IKeylable tuple)
  {
    // aviso al panel respectivo
    getEditorPnl().elementSaved(tuple);
    // aviso al editor
    elementSaved(tuple);
    // Se actualiza la memoria interna
    List<IKeylable> list = datos.get(tuple.getClass());
    if (list != null) {
      int index = list.indexOf(tuple);
      if (index > -1)
        list.set(index, tuple);
      else
        list.add(tuple);
      datos.put(tuple.getClass(), list);
    }
  }

  /**
   * Notifica el guardado de un elemento
   * 
   * @param tuple
   */
  protected abstract void elementSaved(final IKeylable tuple);

  /**
   * Notifica la eliminacion de un elemento
   * 
   * @param tuple
   */
  protected abstract void elementRemoved(final IKeylable tuple);

  /**
   * Notifica la eliminacion de un elemento
   * 
   * @param tuple
   */
  private void bdDeletedElement(final IKeylable tuple)
  {
    getEditorPnl().elementRemoved(tuple);
    getEditorPnl().dataClean();

    elementRemoved(tuple);
    List<IKeylable> list = datos.get(tuple.getClass());
    if (list != null) {
      int index = list.indexOf(tuple);
      if (index > -1)
        list.remove(index);
      datos.put(tuple.getClass(), list);
    }
  }

  /**
   * Retorna el resultado de una consulta a la base de datos realizada
   * generalmente por un panel
   * 
   * @param content
   * @param source
   */
  protected void processResponse(final Object content, final Object source)
  {
    if (content != null)
      proccessResponse(content);
    getEditorPnl().querryResponse(content, source);
  }

  /*
   * Se avisa tambien la respuesta de las preguntas
   */
  protected abstract void proccessResponse(final Object content);

  /**
   * Carga los datos relacionados con la base de datos
   * 
   * @param datos
   */
  protected abstract void loadBdData(final Map<?, ?> datos);

  @SuppressWarnings("unchecked")
  @Override
  public void handle(Object source, Object obj)
  {
    if (obj != null && obj instanceof MSGInternalSelected) {
      Object object = ((MSGInternalSelected) obj).getContent();
      if (object != null && object instanceof IKeylable)
        selectedElement((IKeylable) object);
    } else if (obj != null && obj instanceof MSGFromBdSaved) {
      Object object = ((MSGFromBdSaved) obj).getContent();
      if (object != null && object instanceof IKeylable) {
        bdSavedElement((IKeylable) object);
      }
    } else if (obj != null && obj instanceof MSGFromBdDeleted) {
      Object object = ((MSGFromBdDeleted) obj).getContent();
      if (object != null && object instanceof IKeylable) {
        bdDeletedElement((IKeylable) object);
      }
    } else if (obj != null && obj instanceof MSGFromBdQuerry) {
      Object object = ((MSGFromBdQuerry) obj).getContent();
      if (object != null) {
        MTTOWait.getInstance().detener();
        processResponse(object, ((MSGFromBdQuerry) obj).getSource());
      }
    } else if (obj != null && obj instanceof MSGStart) 
    {
      Object object = ((MSGStart) obj).getContent();
      if (object != null && object instanceof List<?>) 
      {
        List<IKeylable> list = (List<IKeylable>) object;
        if (list != null && !list.isEmpty()) {
          Class<?> classe = list.get(0).getClass();
          datos.put(classe, list);
          getEditorPnl().fillPanel(datos);
          loadBdData(datos);
        }
      }
    } else if(obj != null && obj instanceof MSGLookRefresh ) {
      repaint();
      getEditorPnl().repaintForChangeLook();
    }
    else
    {
      //
    }
  }
}
