package cl.jonnattan.mtto.program;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.gui.GUIMain;
import cl.jonnattan.mtto.util.MyLogger;

/**
 * Esta clase configura el log del sistema completo y ejecuta en modo grafico o
 * consola
 * 
 * @author Jonnattan Griffiths
 * @since 05/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class MTTOProgram
{
  private static Logger logger = Logger.getLogger("MAIN");

  public static void main(String[] args)
  {
    String logServer = Factories.getParameters().getIpDebug();
    int    logPort   = Factories.getParameters().getPortDebug();
    String logFile   = Factories.getParameters().getLogPath() + "mtto.log";
    MyLogger.initialize(Level.INFO, logFile, logServer, logPort);

    String system = System.getProperty("os.name");
    
    if (system.startsWith("Windows")) {
      SwingUtilities.invokeLater(new Runnable()
      {
        @Override
        public void run()
        {
          try {
            // boolean finderLook = false;
            // com.sun.java.swing.plaf.windows.WindowsLookAndFeel
            // javax.swing.plaf.metal.MetalLookAndFeel
            // javax.swing.plaf.nimbus.NimbusLookAndFeel
            // com.jtattoo.plaf.acryl.AcrylLookAndFeel
            // com.jtattoo.plaf.aero.AeroLookAndFeel
            // com.jtattoo.plaf.bernstein.BernsteinLookAndFeel
            // com.jtattoo.plaf.fast.FastLookAndFeel
            // com.jtattoo.plaf.aluminium.AluminiumLookAndFeel
            // com.jtattoo.plaf.graphite.GraphiteLookAndFeel
            // com.jtattoo.plaf.hifi.HiFiLookAndFeel
            // com.jtattoo.plaf.luna.LunaLookAndFeel
            // com.jtattoo.plaf.mcwin.McWinLookAndFeel
            // com.jtattoo.plaf.mint.MintLookAndFeel
            // com.jtattoo.plaf.noire.NoireLookAndFeel
            // com.jtattoo.plaf.smart.SmartLookAndFeel
            // com.jtattoo.plaf.texture.TextureLookAndFeel

            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            /*
             * LookAndFeelInfo[] lista = UIManager.getInstalledLookAndFeels(); for (int i =
             * 0; i < lista.length; i++) { logger.info("Soportado " +
             * lista[i].getClassName()); if
             * (lookAndFeel.equalsIgnoreCase(lista[i].getClassName())) finderLook = true; }
             * if (finderLook) { UIManager.setLookAndFeel(lookAndFeel); } else {
             * logger.info("Se inicia por defecto Look And Feels del Sistema Operativo: " +
             * UIManager.getSystemLookAndFeelClassName());
             * UIManager.setLookAndFeel(UIManager .getSystemLookAndFeelClassName()); }
             */

          } catch (Exception ex) {
            ex.printStackTrace();
          }
          logger.info("Inicio sistema...");
          GUIMain gui = new GUIMain();
          gui.setVisible(true);
        }
      });
    }
  }
}
