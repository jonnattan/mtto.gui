package cl.jonnattan.mtto.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.message.MSGExit;
import cl.jonnattan.mtto.message.MSGUserValid;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOText;
import cl.jonnattan.mtto.util.MTTOWait;
import cl.jonnattan.mtto.util.MyBorderSystem;

/**
 * Para loguearse en el sistema
 * 
 * @author Jonnattan Griffiths
 * @since 09/08/2019
 * @version 1.0 Copyright(c)
 */
public class GUIRegister extends JDialog
{
  private static final long serialVersionUID = 1L;
  private final Dimension   SIZE             = new Dimension(250, 120);
  private MTTOText          txtUser          = null;
  private JPasswordField    txtPass          = null;

  public GUIRegister()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    JDialog.setDefaultLookAndFeelDecorated(true);
    getRootPane().setWindowDecorationStyle(JRootPane.NONE);

    setUndecorated(true);
    setModal(true);
    setModalExclusionType(ModalExclusionType.NO_EXCLUDE);
    setAlwaysOnTop(true);
    setSize(new Dimension(262, 104));
    setPreferredSize(SIZE);
    setMaximumSize(SIZE);
    setMinimumSize(SIZE);

    getContentPane().setLayout(new BorderLayout(0, 0));
    JPanel pnlMain = new JPanel();
    pnlMain.setBorder(new MyBorderSystem());
    getContentPane().add(pnlMain, BorderLayout.CENTER);
    pnlMain.setLayout(new GridLayout(3, 1, 0, 0));

    JPanel pnlNorth = new JPanel();
    pnlNorth.setBorder(new EmptyBorder(9, 5, 5, 5));
    pnlMain.add(pnlNorth);
    pnlNorth.setLayout(new BorderLayout(0, 0));

    JLabel lblUser = new JLabel("Usuario   ");
    lblUser.setHorizontalAlignment(SwingConstants.CENTER);
    lblUser.setFont(CONSTConst.font);
    pnlNorth.add(lblUser, BorderLayout.WEST);

    txtUser = new MTTOText(20);
    pnlNorth.add(txtUser, BorderLayout.CENTER);
    txtUser.setColumns(10);

    JPanel pnlCenter = new JPanel();
    pnlCenter.setBorder(new EmptyBorder(0, 5, 0, 5));
    pnlMain.add(pnlCenter);
    pnlCenter.setLayout(new BorderLayout(0, 0));

    JPanel panel = new JPanel();
    pnlCenter.add(panel, BorderLayout.NORTH);
    panel.setLayout(new BorderLayout(0, 0));
    JLabel lblPass = new JLabel("Contrase\u00F1a");
    lblPass.setFont(CONSTConst.font);
    panel.add(lblPass, BorderLayout.WEST);
    txtPass = new JPasswordField(20);
    txtPass.addKeyListener(new KeyListener()
    {
      /** Handle the key typed event from the text field. */
      public void keyTyped(KeyEvent e)
      {
      }

      @Override
      public void keyPressed(KeyEvent e)
      {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
          validaUserPass();
      }

      @Override
      public void keyReleased(KeyEvent e)
      {
      }
    });
    txtPass.setFont(CONSTConst.font);
    panel.add(txtPass, BorderLayout.CENTER);

    JPanel pnlSur = new JPanel();
    pnlSur.setBorder(new EmptyBorder(5, 5, 5, 5));
    pnlMain.add(pnlSur);

    JButton btnAceptar = new JButton("Aceptar");
    btnAceptar.setIcon(new ImageIcon(GUIRegister.class
        .getResource("/com/jtattoo/plaf/icons/small/check_symbol_10x10.png")));
    btnAceptar.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        validaUserPass();
      }
    });
    pnlSur.setLayout(new GridLayout(0, 2, 0, 0));
    btnAceptar.setFont(CONSTConst.font);
    pnlSur.add(btnAceptar);

    JButton btnCancelar = new JButton("Cancelar");
    btnCancelar.setIcon(new ImageIcon(GUIRegister.class
        .getResource("/com/jtattoo/plaf/icons/small/closer_10x10.png")));
    btnCancelar.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        cancel();
      }
    });
    btnCancelar.setFont(CONSTConst.font);
    pnlSur.add(btnCancelar);
    configureLocation();
  }

  /**
   * Genera el mensaje para crear un data set
   */
  protected void validaUserPass()
  {
    @SuppressWarnings("deprecation")
    String pass = txtPass.getText();
    String user = txtUser.getText();

    if (user.length() > 0 && pass.length() > 0) {
      MTTOWait.getInstance().iniciar(this);

      MSGUserValid message = new MSGUserValid();
      message.setContent(user.trim());
      message.setPass(pass.trim());
      message.setSource(this);
      Factories.getDispatcher().post(message);

    } else {
      JOptionPane.showConfirmDialog(this,
          "Debe escribir correctamente Usuario y Contraseña", "Sr. Operador",
          JOptionPane.WARNING_MESSAGE);
    }
  }

  private void configureLocation()
  {
    // Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    Point     point = Factories.getParameters().getInitialPosition();
    Dimension dim   = Factories.getParameters().getInitialDimension();

    int x = point.x + (dim.width / 2 - getSize().width / 2);
    int y = point.y + (dim.height / 2 - getSize().height / 2);
    setLocation(x, y);
  }

  protected void cancel()
  {
    dispose();
    MSGExit message = new MSGExit();
    message.setContent("ADIOS");
    message.setSource(this);
    Factories.getDispatcher().post(message);
  }

  @Override
  public void paint(Graphics g)
  {
    // super.paint(graphic);
    // graphic.setColor(new Color(0, 0, 0, 0));
    // Rectangle bound = graphic.getClipBounds();
    // Shape roundRectangle = new RoundRectangle2D.Float(bound.x, bound.y,
    // bound.width, bound.height, 25, 25);
    // AWTUtilities.setWindowShape(this, roundRectangle);

    Graphics2D g2 = (Graphics2D) g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

    Paint                  oldPaint = g2.getPaint();
    RoundRectangle2D.Float r2d      = new RoundRectangle2D.Float(0, 0,
        getWidth(), getHeight() - 1, 20, 20);
    g2.clip(r2d);
    g2.setPaint(new GradientPaint(0.0f, 0.0f, Color.black.darker(), 0.0f,
        getHeight(), Color.BLUE.darker()));
    g2.fillRect(0, 0, getWidth(), getHeight());

    g2.setStroke(new BasicStroke(4f));
    g2.setPaint(new GradientPaint(0.0f, 0.0f, Color.red, 0.0f, getHeight(),
        Color.cyan));
    g2.drawRoundRect(0, 0, getWidth() - 2, getHeight() - 2, 18, 18);

    g2.setPaint(oldPaint);
    super.paint(g2);
  }
}
