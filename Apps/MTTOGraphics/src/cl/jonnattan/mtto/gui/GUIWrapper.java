package cl.jonnattan.mtto.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.message.MSGLookRefresh;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.IconTray;

/**
 * Clase sin descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 26/08/2019
 * @version 1.0 Copyright(c) 2019
 */
public abstract class GUIWrapper extends JFrame
{
  private static final long serialVersionUID = 1L;
  protected static Logger   logger           = Logger.getGlobal();
  private IconTray          trayIcon         = null;
  private JMenu             mnProgram        = null;
  private JMenuItem         mnAbout          = null;
  private JMenu             mnVistas         = null;
  protected GUIAdmin        guiAdmin         = null;
  private GUIInfo           guiInfo          = null;

  public GUIWrapper()
  {
    super();
    trayIcon = new IconTray();
    if (trayIcon.hiddenFrame())
      trayIcon.setFrame(this);
    initialize();
  }

  private void initialize()
  {
    // ----------------------------------------------------------------------
    // Informacion del sistema operativo
    String javaVersion = System.getProperty("java.specification.version");

    logger.info("Sistema Operativo: " + System.getProperty("os.name") + ", "
        + System.getProperty("os.version") + " "
        + System.getProperty("os.arch"));
    logger.info("Usuario: " + System.getProperty("user.name") + " "
        + System.getProperty("user.language") + "_"
        + System.getProperty("user.country"));
    logger.info("Version Java: " + System.getProperty("java.version")
        + " RunTime: " + System.getProperty("java.runtime.version"));

    double version = Double.parseDouble(javaVersion);
    if (version < 1.7) {
      String msg   = "Se ha detectado una versión de java incompatible con el sistema\n"
          + "Puede que hayan problemas de funcionamiento, contacte a su administrador.\n"
          + "La versi�n requerida de Java es 1.7 o superior.\n�Desea Continuar de todas formas?";
      String title = "Sr. Operador "/* + System.getProperty( "user.name" ) */;
      if (JOptionPane.showConfirmDialog(this, msg, title,
          JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
        closeAll();
        detenerAll();
      }
    }

    setResizable(true);
    setName("framePrincipal");
    setTitle("Módulo de Mantenimiento");
    setIconImage(Factories.getImageProvider().getIconSystem());
    
    setSize(Factories.getParameters().getInitialDimension());
    setPreferredSize(Factories.getParameters().getInitialDimension());
    setMaximumSize(Factories.getParameters().getInitialDimension());
    setMaximumSize(Factories.getParameters().getInitialDimension());

    addWindowListener(new WindowAdapter()
    {
      @Override
      public void windowClosing(WindowEvent e)
      {
        closeAll();
      }

      @Override
      public void windowIconified(WindowEvent e)
      {
        if (trayIcon.getTrayIcon() != null) {
          setVisible(false);
        }
      }

      @Override
      public void windowDeiconified(WindowEvent e)
      {
        setVisible(true);
      }
    });

    setLocation(Factories.getParameters().getInitialPosition());
    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);
    mnProgram = new JMenu("Programa");
    mnProgram.setFont(CONSTConst.font);
    menuBar.add(mnProgram);

    // item de cerrar
    JMenuItem itemExit = new JMenuItem("Salir");
    itemExit.setFont(CONSTConst.font);
    itemExit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        closeAll();
      }
    });

    // Item de administracion
    JMenuItem itemAdmin = new JMenuItem("Administrar");
    itemAdmin.setFont(CONSTConst.font);
    itemAdmin.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        showAdminGui();
      }
    });

    mnProgram.add(itemExit);
    mnProgram.add(itemAdmin);

    mnVistas = new JMenu("Vistas");
    mnVistas.setToolTipText("Elija Look de la APP");
    mnVistas.setFont(CONSTConst.font);
    menuBar.add(mnVistas);

    JMenuItem look1 = new JMenuItem("Vista Luna");
    look1.setFont(CONSTConst.font);
    look1.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLook("com.jtattoo.plaf.luna.LunaLookAndFeel");
      }
    });

    JMenuItem look2 = new JMenuItem("Vista Graphite");
    look2.setFont(CONSTConst.font);
    look2.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLook("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
      }
    });

    JMenuItem look3 = new JMenuItem("Vista Smart");
    look3.setFont(CONSTConst.font);
    look3.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLook("com.jtattoo.plaf.smart.SmartLookAndFeel");
      }
    });

    JMenuItem look4 = new JMenuItem("Vista Estandar");
    look4.setFont(CONSTConst.font);
    look4.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLook("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
      }
    });

    JMenuItem look5 = new JMenuItem("Vista Aluminio");
    look5.setFont(CONSTConst.font);
    look5.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLook("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
      }
    });

    mnVistas.add(look1);
    mnVistas.add(look2);
    mnVistas.add(look3);
    mnVistas.add(look4);
    mnVistas.add(look5);

    mnAbout = new JMenuItem("Acerca de");
    mnAbout.setToolTipText("");
    mnAbout.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showAbout();
      }
    });
    mnAbout.setFont(CONSTConst.font);
    menuBar.add(mnAbout);

  }

  protected void setLook(final String look)
  {
    try {
      UIManager.setLookAndFeel(look);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    repaint();

    MSGLookRefresh message = new MSGLookRefresh();
    message.setContent(look);
    message.setSource(this);
    Factories.getDispatcher().post(message);
  }

  public void closeAll()
  {
    trayIcon.closeTray();
    detenerAll();
  }

  protected abstract void showAdminGui();

  protected abstract void detenerAll();

  public void showAbout()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        if (guiInfo == null)
          guiInfo = new GUIInfo();
        guiInfo.setVisible(true);
      }
    });
  }

  public void winMessageInfo(final String text)
  {
    JOptionPane.showMessageDialog(this, text, getTitle(),
        JOptionPane.INFORMATION_MESSAGE);
    trayMessageInfo(text);
  }

  public void winMessageError(final String text)
  {
    JOptionPane.showMessageDialog(this, text, getTitle(),
        JOptionPane.ERROR_MESSAGE);
    trayMessageError(text);
  }

  public void trayMessageInfo(final String text)
  {
    trayIcon.tryMessageInfo(text);
    logger.info(text);
  }

  public void trayMessageError(final String text)
  {
    trayIcon.tryMessageError(text);
    logger.severe(text);
  }

}
