package cl.jonnattan.mtto.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import cl.jonnattan.mtto.factory.Factories;

/**
 * Clase que define el Acerca De...
 * 
 * @author Jonnattan Griffiths
 * @since 28/01/2019
 * @version 1.0 Copyright(c)
 */
public class GUIInfo extends JDialog
{
  private static final long serialVersionUID = 1L;
  private JEditorPane       editorPane       = null;

  public GUIInfo()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    setSize(new Dimension(426, 198));
    setPreferredSize(getSize());
    setMaximumSize(getSize());
    setMinimumSize(getSize());
    
    Point     p           = Factories.getParameters().getInitialPosition();
    Dimension dimensiones = Factories.getParameters().getInitialDimension();
    int       x           = p.x + (dimensiones.width / 2) - (getWidth() / 2);
    int       y           = p.y + (dimensiones.height / 2) - (getHeight() / 2);
    Point     point       = new Point(x, y);
    setLocation(point);
    
    setAlwaysOnTop(true);
    setResizable(false);
    setTitle("Acerca de...");
    
    JPanel panel = new JPanel();
    panel.setOpaque(false);
    getContentPane().add(panel, BorderLayout.CENTER);
    panel.setLayout(new BorderLayout(0, 0));
    
    editorPane = new JEditorPane();
    editorPane.setContentType("text/html");
    editorPane.setOpaque(false);
    editorPane.setEditable(false);
    
    String text = "<HTML><BODY><B><CENTER> SISTEMA MTTO MAQUINARIAS</CENTER></B> ";
    text += "<FONT size=4><center> ";
    text += "Módulo de Mantenimiento Maquinarias<br>";
    text += "Su óptimo funcionamiento en " + System.getProperty("os.name");
    text += "<br></center> </font> ";
    text += "</BODY></HTML>";
    editorPane.setText(text);    
    panel.add(editorPane);    
    JLabel lblPhoto = new JLabel("");
    panel.add(lblPhoto, BorderLayout.SOUTH);
    lblPhoto.setHorizontalAlignment(SwingConstants.CENTER);    
    ImageIcon im = new ImageIcon(Factories.getImageProvider().getImgSystem());
    lblPhoto.setIcon(im);
  }
}
