package cl.jonnattan.mtto.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mtto.editor.CheckEditor;
import cl.jonnattan.mtto.editor.ComponenteEditor;
import cl.jonnattan.mtto.editor.PersonaEditor;
import cl.jonnattan.mtto.editor.PlanEditor;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPnlEditor;
import cl.jonnattan.mtto.message.MSGToBdQuerry;
import cl.jonnattan.mtto.message.MSGToBdQuerry.EQuerryAction;

/**
 * Clase Principal del SISTEMON
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c)
 */
public class GUIAdmin extends JFrame
{
  private static final long serialVersionUID = 1L;
  private JTabbedPane       tabbed           = null;
  private IPnlEditor[]      pnls             = null;
  private int               indexSelect      = -1;
  private int               length           = 0;
  private final boolean     isAdmin;

  public GUIAdmin()
  {
    super();
    this.isAdmin = GUIMain.persona.getUser().equalsIgnoreCase("admin");
    setIconImage(Factories.getImageProvider().getIconSystem());
    initialize();
  }

  private void initialize()
  {
    setSize(new Dimension(510, 450));
    setMinimumSize(getSize());
    setMaximumSize(getSize());
    setPreferredSize(getSize());
    Point     p           = Factories.getParameters().getInitialPosition();
    Dimension dimensiones = Factories.getParameters().getInitialDimension();
    int       x           = p.x + (dimensiones.width / 2) - (getWidth() / 2);
    int       y           = p.y + (dimensiones.height / 2) - (getHeight() / 2);
    Point     point       = new Point(x, y);
    setLocation(point);

    setTitle("Administrar Opciones de Manteniemiento");
    getContentPane().setLayout(new BorderLayout(0, 0));

    tabbed = new JTabbedPane(JTabbedPane.TOP);
    getContentPane().add(tabbed, BorderLayout.CENTER);

    pnls = getPaneles();

    for (IPnlEditor ipnl : pnls)
      if (ipnl instanceof JPanel)
        tabbed.addTab(ipnl.getTabName(), null, (JPanel) ipnl, null);
    // El cero queda como selectionado
    indexSelect = 0;

    tabbed.addChangeListener(new ChangeListener()
    {
      @Override
      public void stateChanged(ChangeEvent event)
      {
        if (event.getSource() instanceof JTabbedPane) {
          JTabbedPane tabpanel = (JTabbedPane) event.getSource();
          if (pnls[indexSelect].isWork()) {
            String msg = "Finalize Proceso de Edici�n Antes de Continuar";
            JOptionPane.showMessageDialog(GUIAdmin.this, msg, "Sr. Operador",
                JOptionPane.ERROR_MESSAGE);
            tabpanel.setSelectedComponent((JPanel) pnls[indexSelect]);
          } else {
            indexSelect = tabpanel.getSelectedIndex();

            MSGToBdQuerry msgGetList = new MSGToBdQuerry();
            msgGetList.seteType(EQuerryAction.GETLIST);
            switch (indexSelect) {
            case 0:
              msgGetList.setContent(jPlan.class);
              break;
            case 1:
              MSGToBdQuerry msg = new MSGToBdQuerry();
              msg.seteType(EQuerryAction.GETLIST);
              msg.setSource(this);
              msg.setContent(jPlan.class);
              Factories.getDispatcher().post(msg);              
              msgGetList.setContent(jChecklist.class);
              break;
            case 2:              
              msgGetList.setContent(jComponent.class);
              break;
            case 3:
              msgGetList.setContent(jPerson.class);
              break;
            }
            msgGetList.setSource(this);
            Factories.getDispatcher().post(msgGetList);

          }
        }
      }
    });

    pack();

    MSGToBdQuerry msgGetList = new MSGToBdQuerry();
    msgGetList.seteType(EQuerryAction.GETLIST);
    msgGetList.setContent(jPlan.class);
    msgGetList.setSource(this);
    Factories.getDispatcher().post(msgGetList);

  }

  /** Paneles */
  private IPnlEditor[] getPaneles()
  {
    IPnlEditor[] paneles = new IPnlEditor[10];

    paneles[length] = new PlanEditor();
    length++;
    paneles[length] = new CheckEditor();
    length++;
    paneles[length] = new ComponenteEditor();
    length++;
    if (isAdmin) {
      paneles[length] = new PersonaEditor();
      length++;
    }
    return paneles;
  }

}
