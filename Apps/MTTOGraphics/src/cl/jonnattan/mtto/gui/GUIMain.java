package cl.jonnattan.mtto.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mtto.editor.EqEditor;
import cl.jonnattan.mtto.editor.MttoEditor;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPnlEditor;
import cl.jonnattan.mtto.message.MSGExit;
import cl.jonnattan.mtto.message.MSGFromBdQuerry;
import cl.jonnattan.mtto.message.MSGFromBdSaved;
import cl.jonnattan.mtto.message.MSGLookRefresh;
import cl.jonnattan.mtto.message.MSGUserValid;
import cl.jonnattan.mtto.panels.PnlBotones;
import cl.jonnattan.mtto.persistence.PersistenceManager;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.ExecServer;
import cl.jonnattan.mtto.util.MTTOWait;

/**
 * Interfaz grafica principal
 * 
 * @author Jonnattan Griffiths
 * @since 02/08/2019
 * @version 1.0 Copyright(c)
 */
public class GUIMain extends GUIWrapper implements Listener
{
  private static final long  serialVersionUID = 1L;
  private PnlBotones         pnlBtns          = null;
  private Component          pnlSelected      = null;
  private GUIRegister        guiReg           = null;
  private PersistenceManager restManager      = null;
  public static jPerson      persona          = null;
  private int                aviso            = 0;
  private ExecServer         server           = null;

  public GUIMain()
  {
    super();
    initServer();
    Factories.getDispatcher().subscribe(this);
    restManager = new PersistenceManager();
    restManager.start();

    showGuiReg();

  }

  private void initServer()
  {
    if (Factories.getParameters().initServer()) {
      server = new ExecServer("java -jar server.app-0.0.1-SNAPSHOT.jar");
      server.start();
      try {
        while (server.isRunProgram())
          Thread.sleep(100);
        Thread.sleep(5000);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void detenerAll()
  {
    int opcion = JOptionPane.showConfirmDialog(this, "¿Seguro que desea cerrar la aplicación?", "Sr Operador",
        JOptionPane.YES_NO_OPTION);

    if (opcion == JOptionPane.YES_OPTION) {
      MTTOWait.getInstance().iniciar(this);
      Factories.getDispatcher().unsubscribe(this);
      if (server != null)
        server.stop();
      try {
        Thread.sleep(1000);
      } catch (Exception e) {
        e.printStackTrace();
      }
      logger.info("Se cierra la App");
      System.exit(0);
    }
  }

  private void showGuiReg()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        if (guiReg == null)
          guiReg = new GUIRegister();
        guiReg.setVisible(true);
      }
    });
  }

  @Override
  protected void showAdminGui()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        guiAdmin.pack();
        guiAdmin.setVisible(true);
        guiAdmin.setLocation(getLocation());
      }
    });
  }

  private void initialize()
  {
    pnlBtns = new PnlBotones();
    getContentPane().add(pnlBtns, BorderLayout.SOUTH);

    final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    tabbedPane.setFont(CONSTConst.font);
    getContentPane().add(tabbedPane, BorderLayout.CENTER);

    IPnlEditor pnlEquipment = new EqEditor();
    pnlSelected = (Component) pnlEquipment;

    tabbedPane.addTab(pnlEquipment.getTabName(), null, (JPanel) pnlEquipment, null);

    IPnlEditor pnlMantencion = new MttoEditor();
    tabbedPane.addTab(pnlMantencion.getTabName(), null, (JPanel) pnlMantencion, null);

    tabbedPane.addChangeListener(new ChangeListener()
    {
      @Override
      public void stateChanged(ChangeEvent event)
      {
        if (event.getSource() instanceof JTabbedPane) {
          JTabbedPane tabpanel = (JTabbedPane) event.getSource();
          if (((IPnlEditor) pnlSelected).isWork()) {
            if (tabpanel.getSelectedComponent() != pnlSelected) {
              logger.info("El panel esta en edicion anterior");
              String msg = "Finalizar Edición Actual Antes de Continuar";
              JOptionPane.showMessageDialog(GUIMain.this, msg, "Sr. Operador", JOptionPane.ERROR_MESSAGE);
              tabpanel.setSelectedComponent(pnlSelected);
            } else {
              pnlSelected = tabpanel.getSelectedComponent();
            }
          }
        }
      }
    });
  }

  @Override
  public void handle(Object source, Object obj)
  {
    if (obj instanceof MSGUserValid) {
      MSGUserValid message = (MSGUserValid) obj;
      if (message.getPass() == null) {
        MTTOWait.getInstance().detener();
        if (message.getContent() != null && message.getContent() instanceof jPerson) {
          GUIMain.persona = (jPerson) message.getContent();
          // GUI de Administracion
          guiAdmin = new GUIAdmin();
          guiAdmin.setVisible(false);

          if (guiReg != null)
            guiReg.dispose();
          initialize();

        } else {
          JOptionPane.showMessageDialog(guiReg,
              "   No se puede iniciar sistema,\nnombre usuario o contrase�a incorrectos");
        }
      }
    } else if (obj instanceof MSGExit) {
      detenerAll();
    } else if (obj instanceof MSGLookRefresh) {
      repaint();
      guiAdmin.repaint();
    } else if (obj instanceof MSGFromBdSaved)
      MTTOWait.getInstance().detener();
    else if (obj != null && obj instanceof MSGFromBdQuerry) {
      Object object = ((MSGFromBdQuerry) obj).getContent();
      if (object != null) {
        // aca se procesa las alarmas de mantencion
        if (object instanceof List<?> && !((List<?>) object).isEmpty() && ((List<?>) object).get(0) instanceof String) {
          List<?> lista = (List<?>) object;
          aviso++;
          String msgEqs = "Falta Mantenimineto de:\n";
          for (Object o : lista)
            msgEqs += (String) o + "\n";
          trayMessageInfo(msgEqs);
          if (aviso == 1)
            JOptionPane.showMessageDialog(this, msgEqs);
        }
      }
    }
  }

}
