package cl.jonnattan.mtto.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import cl.jonnattan.mantenimiento.datajson.jCheckResp;
import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.message.MSGToBdSave;
import cl.jonnattan.mtto.util.CONSTConst;
import cl.jonnattan.mtto.util.MTTOText;
import cl.jonnattan.mtto.util.MTTOWait;

/**
 * Clase Principal del SISTEMON
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c)
 */
public class GUIMtto extends JFrame
{
  private static final long    serialVersionUID = 1L;
  private final jFullEquipment eq;
  private MTTOText             txtNameEq        = null;
  private MTTOText             txtPlanMtto      = null;
  private JTextArea            txtComment       = null;
  private MTTOText             txtFalla         = null;
  private JPanel               pnlCheckList     = null;
  private JCheckBox            chExt            = null;
  private JButton              btnGuardar       = null;
  private JCheckBox            chPasaMtto       = null;

  public GUIMtto(final jFullEquipment eq)
  {
    super();
    this.eq = eq;

    setIconImage(Factories.getImageProvider().getIconSystem());
    initialize();
  }

  private void initialize()
  {
    setSize(new Dimension(510, 450));
    setMinimumSize(getSize());
    setMaximumSize(getSize());
    setPreferredSize(getSize());

    Point     p           = Factories.getParameters().getInitialPosition();
    Dimension dimensiones = Factories.getParameters().getInitialDimension();
    int       x           = p.x + (dimensiones.width / 2) - (getWidth() / 2);
    int       y           = p.y + (dimensiones.height / 2) - (getHeight() / 2);
    Point     point       = new Point(x, y);
    setLocation(point);

    setTitle("Nuevo mantenimiento");
    getContentPane().setLayout(new BorderLayout(0, 0));

    JPanel pnlInformacion = new JPanel();
    pnlInformacion.setOpaque(false);
    getContentPane().add(pnlInformacion, BorderLayout.NORTH);
    pnlInformacion.setLayout(new BorderLayout(0, 0));

    JPanel pnlDatos = new JPanel();
    pnlDatos.setBorder(new TitledBorder(null, "Datos para Mantención",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    pnlDatos.setOpaque(false);
    pnlInformacion.add(pnlDatos, BorderLayout.NORTH);
    pnlDatos.setLayout(new GridLayout(0, 1, 0, 0));

    JPanel pnlEq = new JPanel();
    pnlEq.setOpaque(false);
    pnlDatos.add(pnlEq);
    pnlEq.setLayout(new BorderLayout(0, 0));

    JLabel lblEq = new JLabel("Nombre Equipo ");
    lblEq.setFont(CONSTConst.font);
    pnlEq.add(lblEq, BorderLayout.WEST);

    txtNameEq = new MTTOText(50, true);
    txtNameEq.setText(this.eq.getName());
    txtNameEq.setEditable(false);
    pnlEq.add(txtNameEq);

    JPanel pnlParPlanManual = new JPanel();
    pnlParPlanManual.setOpaque(false);
    pnlDatos.add(pnlParPlanManual);
    pnlParPlanManual.setLayout(new BorderLayout(0, 0));

    JPanel pnlPlan = new JPanel();
    pnlPlan.setOpaque(false);
    pnlPlan.setLayout(new BorderLayout(0, 0));
    pnlParPlanManual.add(pnlPlan, BorderLayout.EAST);

    JLabel lblPlan = new JLabel("Plan de Mtto.  ");
    lblPlan.setFont(CONSTConst.font);
    pnlPlan.add(lblPlan, BorderLayout.WEST);

    txtPlanMtto = new MTTOText(50, true);
    txtPlanMtto.setEditable(false);
    txtPlanMtto.setText(this.eq.getPlan().getName());
    pnlPlan.add(txtPlanMtto);

    JPanel panel = new JPanel();
    panel.setOpaque(false);
    pnlParPlanManual.add(panel);
    panel.setLayout(new BorderLayout(0, 0));

    JLabel lblNamePerson = new JLabel("Se registra a nombre de:" + GUIMain.persona.getNombre());
    lblNamePerson.setFont(CONSTConst.font);
    panel.add(lblNamePerson);

    JPanel pnlMail = new JPanel();
    pnlMail.setOpaque(false);
    pnlDatos.add(pnlMail);
    pnlMail.setLayout(new BorderLayout(0, 0));

    JLabel lblFalla = new JLabel("Reporte Falla ");
    lblFalla.setFont(CONSTConst.font);
    pnlMail.add(lblFalla, BorderLayout.WEST);

    txtFalla = new MTTOText(50);
    pnlMail.add(txtFalla, BorderLayout.CENTER);

    JPanel pnlExterno = new JPanel();
    pnlExterno.setOpaque(false);
    pnlDatos.add(pnlExterno);
    pnlExterno.setLayout(new BorderLayout(0, 0));

    chExt = new JCheckBox("Requiere revisión externa");
    chExt.setFont(CONSTConst.font);
    pnlExterno.add(chExt, BorderLayout.WEST);

    chPasaMtto = new JCheckBox("Mantenimiento sin novedad");
    chPasaMtto.setSelected(true);
    chPasaMtto.setFont(CONSTConst.font);
    pnlExterno.add(chPasaMtto, BorderLayout.CENTER);

    JPanel pnlComentario = new JPanel();
    pnlComentario.setOpaque(false);

    pnlComentario.setLayout(new BorderLayout(0, 0));

    JLabel lblComentario = new JLabel("Comentario Mtto");
    lblComentario.setFont(CONSTConst.font);
    pnlComentario.add(lblComentario, BorderLayout.WEST);

    txtComment = new JTextArea();
    txtComment.setRows(3);
    txtComment.setLineWrap(true);
    pnlComentario.add(txtComment);

    pnlInformacion.add(pnlComentario, BorderLayout.SOUTH);

    JPanel pnlLista = new JPanel();
    pnlLista.setBorder(new TitledBorder(
        new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255),
            new Color(160, 160, 160)),
        "CheckList", TitledBorder.LEADING, TitledBorder.TOP, null,
        new Color(0, 0, 0)));
    getContentPane().add(pnlLista, BorderLayout.CENTER);
    pnlLista.setOpaque(false);
    pnlLista.setLayout(new BorderLayout(0, 0));

    JPanel pnlChecks = new JPanel();
    pnlChecks.setOpaque(false);
    pnlChecks.setLayout(new BorderLayout(0, 0));
    JScrollPane scrollPane = new JScrollPane(pnlChecks);
    pnlLista.add(scrollPane, BorderLayout.NORTH);

    JPanel panel_1 = new JPanel();
    pnlLista.add(panel_1, BorderLayout.SOUTH);

    btnGuardar = new JButton("Guardar Mantenimiento");
    btnGuardar.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveMtto();
      }
    });
    btnGuardar.setIcon(new ImageIcon(GUIMtto.class.getResource(
        "/com/jtattoo/plaf/mcwin/icons/medium/check_symbol_disabled_18x13.png")));
    panel_1.add(btnGuardar);

    // Checklist
    List<jChecklist> listaChecks = this.eq.getPlan().getItems();
    if (listaChecks != null) {
      pnlCheckList = new JPanel();
      pnlCheckList.setOpaque(false);
      pnlCheckList.setLayout(new GridLayout(listaChecks.size(), 1, 0, 0));
      for (jChecklist cl : listaChecks) {
        JCheckBox ck = new JCheckBox(cl.getDescripcion());
        ck.setName(cl.getId().toString());
        ck.setToolTipText(cl.getTitulo());
        pnlCheckList.add(ck);
      }
      pnlChecks.add(pnlCheckList, BorderLayout.NORTH);
    }

    pack();

  }

  protected void saveMtto()
  {
    if (!txtFalla.getText().isEmpty() && !txtComment.getText().isEmpty()) {
      MTTOWait.getInstance().iniciar(this);
      jReport report = new jReport();
      report.setExterno(chExt.isSelected());
      report.setFalla(txtFalla.getText());
      report.setPersona(GUIMain.persona.getUser());
      report.setComentario(txtComment.getText());
      report.setHayFalla(!chPasaMtto.isSelected());
      report.setFechahora(new Date());
      report.setPasa(chPasaMtto.isSelected());
      report.setIdEq(this.eq.getId());

      List<jCheckResp> list = report.getRespuestas();

      for (Component cmp : pnlCheckList.getComponents()) {
        if (cmp instanceof JCheckBox) {
          JCheckBox  jcb  = (JCheckBox) cmp;
          jCheckResp resp = new jCheckResp();
          resp.setId(Long.parseLong(jcb.getName()));
          resp.setYes(jcb.isSelected());
          list.add(resp);
        }
      }

      MSGToBdSave querry = new MSGToBdSave();
      querry.setContent(report);
      querry.setSource(this);
      Factories.getDispatcher().post(querry);

      btnGuardar.setEnabled(false);
    } else
      JOptionPane.showMessageDialog(this, "Datos insuficientes");
  }

  public void informacionUser()
  {
    JOptionPane.showMessageDialog(this, "Mantenimineto registrado con exito.");
    dispose();
    btnGuardar.setEnabled(true);
  }

}
