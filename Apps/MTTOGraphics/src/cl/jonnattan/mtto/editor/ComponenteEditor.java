package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import javax.swing.table.TableModel;

import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.CmteTblModel;
import cl.jonnattan.mtto.panels.PnlComponente;

/**
 * Clase Para Ingreso, Modificacion y eliminacion de Paises
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c) 2019
 */
public class ComponenteEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private CmteTblModel      tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public ComponenteEditor()
  {
    super(false);
    aditionalConfig();
  }

  protected void aditionalConfig()
  {
    tblModel = new CmteTblModel();
    getTable().setModel(tblModel);
  }

  protected TableModel getTableModel()
  {
    if (tblModel == null)
      tblModel = new CmteTblModel();
    return tblModel;
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (ComponenteEditor.pnlEditor == null)
      ComponenteEditor.pnlEditor = new PnlComponente();
    return ComponenteEditor.pnlEditor;
  }

  @Override
  public String getTabName()
  {
    return "Componentes";
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jComponent> list = (List<jComponent>) datos.get(jComponent.class);
    if (list != null) {
      for (jComponent dato : list)
        tblModel.save(dato);
      setNumeroElementos(list.size());
    }
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jComponent) {
      tblModel.save((jComponent) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jComponent) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(Object content)
  {
    if (content instanceof List<?>) {
      List<?> list = (List<?>) content;
      if (!list.isEmpty() && list.get(0) instanceof jComponent) {
        for (Object dato : list)
          tblModel.save((jComponent) dato);
        setNumeroElementos(list.size());
      }
    }
  }
}
