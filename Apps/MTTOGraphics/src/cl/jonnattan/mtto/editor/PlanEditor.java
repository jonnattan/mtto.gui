package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.PlanTblModel;
import cl.jonnattan.mtto.panels.PnlPlan;

/**
 * Clase para realizar mantencion
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class PlanEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private PlanTblModel      tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public PlanEditor()
  {
    super(false);
    aditionalConfig();
  }

  private void aditionalConfig()
  {
    tblModel = new PlanTblModel();
    getTable().setModel(tblModel);
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (PlanEditor.pnlEditor == null)
      PlanEditor.pnlEditor = new PnlPlan();
    return PlanEditor.pnlEditor;
  }

  @Override
  public String getTabName()
  {
    return "Plan de Mantenimiento";
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jPlan> list = (List<jPlan>) datos.get(jPlan.class);
    if(list != null)
    {
      for (jPlan dato : list)
        tblModel.save(dato);
      setNumeroElementos(list.size());
    }      
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jPlan) {
      tblModel.save((jPlan) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jPlan) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(final Object content)
  {
    if( content instanceof List<?> )
    {
      List<?> list = (List<?>) content;
      if( !list.isEmpty() && list.get(0) instanceof jPlan)
      {
        for (Object dato : list)
          tblModel.save((jPlan) dato);
        setNumeroElementos(list.size());
      }      
    }
  }
}
