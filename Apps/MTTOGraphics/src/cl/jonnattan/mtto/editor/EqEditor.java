package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import javax.swing.table.TableColumn;

import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.EqTblModel;
import cl.jonnattan.mtto.panels.PnlEquipo;
import cl.jonnattan.mtto.util.MTTOTableCellRenderer;

/**
 * Sin Descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since 29-04-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class EqEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private EqTblModel        tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public EqEditor()
  {
    super(true);
    aditionalConfig();
  }

  protected void aditionalConfig()
  {
    tblModel = new EqTblModel();
    getTable().setModel(tblModel);
    // Personalizacion para equipos
    getTable().setDefaultRenderer(String.class, new MTTOTableCellRenderer());
    getTable().setRowHeight(25);

    TableColumn columna = getTable().getColumnModel().getColumn(1);
    columna.setPreferredWidth(70);
    columna.setMaxWidth(70);
    columna.setMinWidth(70);
  }

  @Override
  public String getTabName()
  {
    return "Equipos";
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (EqEditor.pnlEditor == null)
      EqEditor.pnlEditor = new PnlEquipo();
    return EqEditor.pnlEditor;
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jEquipment> list = (List<jEquipment>) datos.get(jEquipment.class);
    if (list != null) {
      if (tblModel != null && tblModel.getList() != null
          && tblModel.getList().isEmpty()) {
        for (jEquipment dato : list)
          tblModel.save(dato);
      }
      setNumeroElementos(list.size());
    }
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jEquipment) {
      tblModel.save((jEquipment) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jEquipment) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(Object content)
  {

  }
}
