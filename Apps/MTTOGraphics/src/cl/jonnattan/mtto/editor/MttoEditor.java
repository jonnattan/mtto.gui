package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import javax.swing.table.TableColumn;

import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.message.MSGToBdDelete;
import cl.jonnattan.mtto.models.MttoTblModel;
import cl.jonnattan.mtto.panels.PnlMantencion;

/**
 * Panel para mantenciones
 * 
 * @author Jonnattan Griffiths
 * @since 28-12-2019
 * @version 1.0 Copyright(c) - 2019
 */

public class MttoEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private MttoTblModel      tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public MttoEditor()
  {
    super(true);
    aditionalConfig();

  }

  protected void aditionalConfig()
  {
    tblModel = new MttoTblModel();
    getTable().setModel(tblModel);

    TableColumn columna = getTable().getColumnModel().getColumn(0);
    columna.setPreferredWidth(70);
    columna.setMaxWidth(70);
    columna.setMinWidth(70);

    HideButtons();
  }

  @Override
  public String getTabName()
  {
    return "Mantenimientos";
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (MttoEditor.pnlEditor == null)
      MttoEditor.pnlEditor = new PnlMantencion();
    return MttoEditor.pnlEditor;
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jReport> list = (List<jReport>) datos.get(jReport.class);
    if (list != null && tblModel.getList().isEmpty()) {
      tblModel.clearAll();
      for (jReport dato : list)
        tblModel.save(dato);
      setNumeroElementos(list.size());
    }
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jReport) {
      tblModel.save((jReport) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jPerson) {
      // Si lo que se elimino es la nave seleccionada
      // entonces se envian todas las personas de la lista
      for (jReport persona : tblModel.getList()) {
        MSGToBdDelete msgDelete = new MSGToBdDelete();
        msgDelete.setContent(persona);
        msgDelete.setSource(this);
        Factories.getDispatcher().post(msgDelete);
      }
    }
    if (tuple instanceof jPerson) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(Object content)
  {
    if (content instanceof jFullEquipment) {
      jFullEquipment eqs = (jFullEquipment) content;
      if (eqs.getMaintenances() != null)
      {
        tblModel.clearAll();
        for (jReport mto : eqs.getMaintenances())
          tblModel.save(mto);
      }       
    }
  }
}
