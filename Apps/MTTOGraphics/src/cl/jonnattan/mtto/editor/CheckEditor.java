package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import javax.swing.table.TableColumn;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.CheckTblModel;
import cl.jonnattan.mtto.panels.PnlCheckList;

/**
 * Clase Para Ingreso, Modificacion y eliminacion de Paises
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class CheckEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private CheckTblModel     tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public CheckEditor()
  {
    super(false);
    aditionalConfig();
  }

  protected void aditionalConfig()
  {
    tblModel = new CheckTblModel();
    getTable().setModel(tblModel);

    TableColumn columna = getTable().getColumnModel().getColumn(0);
    columna.setPreferredWidth(150);
    columna.setMaxWidth(200);
    columna.setMinWidth(100);
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (CheckEditor.pnlEditor == null)
      CheckEditor.pnlEditor = new PnlCheckList();
    return CheckEditor.pnlEditor;
  }

  @Override
  public String getTabName()
  {
    return "CheckList";
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jChecklist> list = (List<jChecklist>) datos.get(jChecklist.class);
    if (list != null) {
      for (jChecklist dato : list)
        tblModel.save(dato);
      setNumeroElementos(list.size());
    }
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jChecklist) {
      tblModel.save((jChecklist) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }

  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jChecklist) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(Object content)
  {
    if (content instanceof List<?>) {
      List<?> list = (List<?>) content;
      if (!list.isEmpty() && list.get(0) instanceof jChecklist) {
        for (Object dato : list)
          tblModel.save((jChecklist) dato);
        setNumeroElementos(list.size());
      }
    }
  }
}
