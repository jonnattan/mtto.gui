package cl.jonnattan.mtto.editor;

import java.util.List;
import java.util.Map;

import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.abstracs.APnlEditor;
import cl.jonnattan.mtto.interfaces.IPnlTuple;
import cl.jonnattan.mtto.models.PersonTblModel;
import cl.jonnattan.mtto.panels.PnlPersona;

/**
 * Clase Para Ingreso, Modificacion y eliminacion de Paises
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class PersonaEditor extends APnlEditor
{
  private static final long serialVersionUID = 1L;
  private PersonTblModel    tblModel         = null;
  private static IPnlTuple  pnlEditor        = null;

  public PersonaEditor()
  {
    super(false);
    aditionalConfig();
  }

  protected void aditionalConfig()
  {
    tblModel = new PersonTblModel();
    getTable().setModel(tblModel);
  }

  @Override
  protected IPnlTuple getEditorPnl()
  {
    if (PersonaEditor.pnlEditor == null)
      PersonaEditor.pnlEditor = new PnlPersona();
    return PersonaEditor.pnlEditor;
  }

  @Override
  public String getTabName()
  {
    return "Personas";
  }

  @Override
  protected void loadBdData(Map<?, ?> datos)
  {
    @SuppressWarnings("unchecked")
    List<jPerson> list = (List<jPerson>) datos.get(jPerson.class);
    if (list != null) {
      for (jPerson dato : list)
        tblModel.save(dato);
      setNumeroElementos(list.size());
    }
  }

  @Override
  protected void elementSaved(IKeylable tuple)
  {
    if (tuple instanceof jPerson) {
      tblModel.save((jPerson) tuple);
      setNumeroElementos(tblModel.getRowCount());
    }

  }

  @Override
  protected void elementRemoved(IKeylable tuple)
  {
    if (tuple instanceof jPerson) {
      setNumeroElementos(tblModel.getRowCount());
    }
  }

  @Override
  protected void proccessResponse(Object content)
  {
    if (content instanceof List<?>) {
      List<?> list = (List<?>) content;
      if (!list.isEmpty() && list.get(0) instanceof jPerson) {
        for (Object dato : list)
          tblModel.save((jPerson) dato);
        setNumeroElementos(list.size());
      }
    }
  }
}
