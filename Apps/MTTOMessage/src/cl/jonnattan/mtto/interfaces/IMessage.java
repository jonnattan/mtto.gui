package cl.jonnattan.mtto.interfaces;
/**
 * Interfaz principal de mensajes que fluyen.
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public interface IMessage
{
  void setContent( Object content );
  Object getContent();
  void setSource ( Object source );
  Object getSource();
}

