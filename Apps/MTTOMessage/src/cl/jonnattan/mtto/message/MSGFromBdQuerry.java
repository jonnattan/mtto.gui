package cl.jonnattan.mtto.message;

import cl.jonnattan.mtto.abstracts.AbstractMessage;

/**
 * Mensaje de inicio de sistema
 * @author Jonnattan Griffiths
 * @since 07/08/2019
 * @version 1.0 Copyright(c) 2019
 */
public class MSGFromBdQuerry extends AbstractMessage
{
  public MSGFromBdQuerry()
  {
    super();
  }

}

