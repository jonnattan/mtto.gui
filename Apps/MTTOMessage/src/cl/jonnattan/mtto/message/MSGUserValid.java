package cl.jonnattan.mtto.message;

import cl.jonnattan.mtto.abstracts.AbstractMessage;

/**
 * Mensaje de creacion de un data set
 * 
 * @author Jonnattan Griffiths
 * @since 07/08/2019
 * @version 1.0 Copyright(c)
 */
public class MSGUserValid extends AbstractMessage
{
  private String pass = null;
  
  public MSGUserValid()
  {
    super();
  }

  public String getPass()
  {
    return pass;
  }

  public void setPass(String pass)
  {
    this.pass = pass;
  }
  
}
