package cl.jonnattan.mtto.message;

import cl.jonnattan.mtto.abstracts.AbstractMessage;

/**
 * Mensaje de preguntas de sistema
 * 
 * @author Jonnattan Griffiths
 * @since 07/08/2019
 * @version 1.0 Copyright(c) 2019
 */
public class MSGToBdQuerry extends AbstractMessage
{
  public enum EQuerryAction
  {
    GETONE, CREATION, REPORT, UPDATE, DELETE, GETLIST, ALRMTTO
  };

  private EQuerryAction eType      = EQuerryAction.GETONE;
  private Long          identifier = null;

  public MSGToBdQuerry()
  {
    super();
  }

  public Long getIdentifier()
  {
    return identifier;
  }

  public void setIdentifier(Long identifier)
  {
    this.identifier = identifier;
  }

  public EQuerryAction geteType()
  {
    return eType;
  }

  public void seteType(EQuerryAction eType)
  {
    this.eType = eType;
  }

}
