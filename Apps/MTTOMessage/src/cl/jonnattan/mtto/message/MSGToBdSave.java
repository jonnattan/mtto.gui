package cl.jonnattan.mtto.message;

import cl.jonnattan.mtto.abstracts.AbstractMessage;

/**
 * Mensaje que se usa para enviar a la bd
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class MSGToBdSave extends AbstractMessage
{
  public MSGToBdSave()
  {
    super();
  }

}

