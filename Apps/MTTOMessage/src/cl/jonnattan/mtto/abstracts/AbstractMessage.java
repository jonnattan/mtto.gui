package cl.jonnattan.mtto.abstracts;

import cl.jonnattan.mtto.interfaces.IMessage;

/**
 * Implementacion de mensajes
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c)
 */
public class AbstractMessage implements IMessage
{
  private Object content = null;
  private Object source  = null;

  public AbstractMessage()
  {
    super();
  }

  @Override
  public void setContent(Object content)
  {
    this.content = content;
  }

  @Override
  public Object getContent()
  {
    return this.content;
  }

  @Override
  public void setSource(Object source)
  {
    this.source = source;
  }

  @Override
  public Object getSource()
  {
    return this.source;
  }

}
