package cl.jonnattan.mtto.abstracts;

import cl.jonnattan.mtto.interfaces.IPersistente;

/**
 * Abstract de datos MySql
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public abstract class APersistente implements IPersistente
{
  private static final long serialVersionUID = 1L;
  public final int SIZEBUFFER = 5000;

  public APersistente()
  {
    super();
  }
}
