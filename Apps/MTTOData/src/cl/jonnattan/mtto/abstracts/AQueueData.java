package cl.jonnattan.mtto.abstracts;

import cl.jonnattan.mtto.interfaces.IQueueData;

/**
 * Implementacion de IData
 * 
 * @author Jonnattan Griffiths
 * @since 14/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class AQueueData implements IQueueData
{
  private Object  source  = null;
  private Object  content = null;

  public AQueueData()
  {
   
  }

  @Override
  public void setSource( Object source )
  {
    this.source = source;
  }

  @Override
  public void setContent( Object data )
  {
    this.content = data;
  }

  @Override
  public Object getSource()
  {
    return this.source;
  }

  @Override
  public Object getContent()
  {
    return this.content;
  }

}
