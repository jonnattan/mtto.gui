package cl.jonnattan.mtto.enums;
/**
 * Estados de conexion al servicio externo que provee datos 
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public enum EServerStates
{
  /** Se inicia el ESAT paro queda en estado desconectado */
  CONNECTED,
  /** Funcionando con una conexion activa */
  DISCONNECT;
}

