package cl.jonnattan.mtto.enums;

public enum EDocumentos
{
  NINGUNO("NINGUNO","NONE"), 
  PASSPORT("PASAPORTE","PS"), 
  CEDULA("CEDULA IDENTIDAD","CI"), 
  IFE("CREDENCIAL ELECTOR","CE");
  
  private String description = "";
  private String code = "";
  
  private EDocumentos(String description, String code)
  {
    this.description = description;
    this.code = code;
  }
  
  public String getCode()
  {
    return code;
  }
  
  @Override
  public String toString()
  {
    return description;
  }
}
