package cl.jonnattan.mtto.enums;

/**
 * Enumerado que define el valor de los mensajes, enviados, recibidos o
 * desconocido
 * 
 * @author Jonnattan Griffiths
 * @since 12/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public enum EMsgType
{
  DESCONOCIDO,
  ENVIADO,
  RECIBIDO;
}
