package cl.jonnattan.mtto.enums;

/**
 * Enumerado de mensajes de sistema
 * 
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public enum EAlarm {
  INFO_ERROR_SISTEMA,
  PROCESA_CONEXION,
  PROCESA_DESCONEXION,
  BD_NO_ENCONTRADA;
  
  public byte value()
  {
    return (byte) ordinal();
  }
  
}
