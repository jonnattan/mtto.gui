package cl.jonnattan.mtto.data.process;

import cl.jonnattan.mtto.abstracts.AQueueData;

/**
 * Encapsula data JMS
 * 
 * @author Jonnattan Griffiths
 * @since 14/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class DataPersistence extends AQueueData
{
  public DataPersistence()
  {
    super();
  }
}
