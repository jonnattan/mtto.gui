package cl.jonnattan.mtto.data.tcp;

import java.nio.ByteBuffer;

import cl.jonnattan.mtto.interfaces.ICodificable;

/**
 * Clase de alerta
 * 
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class EsatState implements ICodificable {
  
  private int states = 0x01;
  
  public EsatState()
  {
    super();
  }
  
  @Override
  public String getClassName()
  {
    return getClass().getName();
  }
  
  @Override
  public ByteBuffer encode()
  {
    ByteBuffer bb = ByteBuffer.allocate((Integer.SIZE / 8));
    bb.putInt(states);
    bb.flip();
    return bb;
  }
  
  @Override
  public void decode(ByteBuffer bBuffer)
  {
    states = bBuffer.getInt();
  }
}
