package cl.jonnattan.mtto.data.tcp;

import java.nio.ByteBuffer;

import cl.jonnattan.mtto.enums.EAlarm;
import cl.jonnattan.mtto.interfaces.ICodificable;
import cl.jonnattan.mtto.util.GLOBALFunction;

/**
 * Clase de alerta
 * 
 * @author Jonnattan Griffiths
 * @since 16/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public class EsatAlarm implements ICodificable {
  private EAlarm type    = EAlarm.INFO_ERROR_SISTEMA;
  private String mensaje = "";
  
  public EsatAlarm()
  {
    super();
  }
  
  public String getMensaje()
  {
    return mensaje;
  }
  
  public void setMensaje(String mensaje)
  {
    this.mensaje = mensaje;
  }
  
  public EAlarm getType()
  {
    return type;
  }
  
  public void setType(EAlarm type)
  {
    this.type = type;
  }
  
  @Override
  public String getClassName()
  {
    return getClass().getName();
  }
  
  @Override
  public ByteBuffer encode()
  {
    byte[] bytes = GLOBALFunction.strToByte(mensaje);
    ByteBuffer bb = ByteBuffer.allocate(bytes.length + 5);
    bb.put(type.value());
    bb.putInt(bytes.length);
    bb.put(bytes);
    bb.flip();
    return bb;
  }
  
  @Override
  public void decode(ByteBuffer bBuffer)
  {
    type = EAlarm.values()[(bBuffer.get() & 0xFF)];
    byte[] bytes = new byte[bBuffer.getInt()];
    mensaje = GLOBALFunction.byteToStr(bytes);
  }
}
