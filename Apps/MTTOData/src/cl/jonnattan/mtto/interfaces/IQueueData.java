package cl.jonnattan.mtto.interfaces;

/**
 * Interfaz para encapsular la data que circula dentro del Motor Cse. La data
 * puede venir del cliente JMS o de los clientes de Sce
 * 
 * @author Jonnattan Griffiths
 * @since 14/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IQueueData {
  /** Thread Fuente del Msg */
  void setSource(Object source);
  /** Obtiene Fuente del MSG */
  Object getSource();
  /** Setea el contenido del MSG */
  void setContent(Object content);
  /** Obtiene el contenido del Msg */
  Object getContent();
}
