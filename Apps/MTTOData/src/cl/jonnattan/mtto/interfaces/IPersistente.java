package cl.jonnattan.mtto.interfaces;

import java.io.Serializable;


/**
 * Clase que describe los objetos que se guardan en la Base de datos
 * 
 * @author Jonnattan Griffiths
 * @since 09/09/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IPersistente extends Serializable
{
  // 
}
