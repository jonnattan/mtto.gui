package cl.jonnattan.mtto.interfaces;


/**
 * Interfaz que define a una clase que se puede codificar y decodificar
 * automaticamente. La codificacion la hace poniendo el nombre de la clase en el
 * principio de los bytes, y el decoficador puede crear la instancia y
 * llenarla.
 * 
 * @author Jonnattan Griffiths
 * @since 22/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface ICodificable extends IDecoder, IEncoder
{
  /**
   * Retorna el Nombre de la Clase
   * @return
   */
  String getClassName();
}
