package cl.jonnattan.mtto.interfaces;

import java.nio.ByteBuffer;

/**
 * Interfaz de decodificación de mensajes
 * @author Jonnattan Griffiths
 * @since 13/08/2019
 * @version 1.0 Copyright(c)   - 2019
 */
public interface IDecoder
{
  void decode( ByteBuffer bBuffer );
}

