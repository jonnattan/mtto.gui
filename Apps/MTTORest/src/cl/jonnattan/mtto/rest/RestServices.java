package cl.jonnattan.mtto.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jLogin;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.factory.Factories;

/**
 * PAra ejecucion de servicios rest
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class RestServices
{
  private final Gson   gson;
  private final String routeServices;

  public RestServices()
  {
    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    routeServices = "http://" + Factories.getParameters().getIpServer() + ":"
        + Factories.getParameters().getPortServer();
  }

  public jPerson loginProcess(String login, String pass)
  {
    jPerson persona = null;

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    Builder builderLogin = client.target(uri).path("login").request();
    // Evaluo so es numero o texto primero
    jLogin user       = new jLogin(login, pass);
    String jsonString = gson.toJson(user);
    try {
      final Invocation invocation = builderLogin
          .buildPost(Entity.json(jsonString));
      final Response   response   = invocation.invoke();
      
      //WebTarget webTarget = client.target(uri).path("equipments");
      //Entity<String> e =  Entity.entity(jsonString, MediaType.APPLICATION_JSON_TYPE); 
      //Response  response  = webTarget.request(MediaType.APPLICATION_JSON_TYPE).post(e);
      
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);
        persona = gson.fromJson(strJson, jPerson.class);
        if (persona != null && persona.getId() != null)
          System.out.println("[RestManager] Respuesta:" + persona);
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return persona;
  }

  public List<jEquipment> getEquipos()
  {
    List<jEquipment> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    // Builder builderLogin = client.target(uri).path("equipments").request();
    // Evaluo so es numero o texto primero
    try {
      //final Invocation invocation = builderLogin.buildGet();
      //final Response   response   = invocation.invoke();
      
      WebTarget webTarget = client.target(uri).path("equipments");
      Response  response  = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
          .get();
      
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        jEquipment[] array = gson.fromJson(strJson, jEquipment[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (jEquipment eq : array)
            list.add(eq);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }

  public jFullEquipment findEquipment(final Long idEq)
  {
    jFullEquipment eq  = null;
    String         uri = routeServices + "/findequipment/" + idEq;
    URL            url;
    try {
      url = new URL(uri);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(HttpMethod.GET);
      connection.setConnectTimeout(10000);
      connection.setRequestProperty("content-type", "text/json; charset=UTF-8");

      if (connection.getResponseCode() == Status.OK.getStatusCode()) {
        if (connection.getInputStream() != null) {
          final InputStream inputStream = connection.getInputStream();
          byte[]            bytes = new byte[inputStream.available()];
          inputStream.read(bytes);
          String            jsonStr     = new String(bytes);
          eq = gson.fromJson(jsonStr, jFullEquipment.class);
        }
      }
      connection.connect();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return eq;
  }

  public List<jResponse> findResponseToMaintance(final Long idReport)
  {
    List<jResponse> list = new ArrayList<>();
    String          uri  = routeServices + "/findresponses/" + idReport;
    URL             url;
    try {
      url = new URL(uri);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(HttpMethod.GET);
      connection.setConnectTimeout(10000);
      connection.setRequestProperty("content-type", "text/json; charset=UTF-8");

      if (connection.getResponseCode() == Status.OK.getStatusCode()) {
        if (connection.getInputStream() != null) {
          final InputStream inputStream = connection.getInputStream();
          byte[]            bytes = new byte[inputStream.available()];
          inputStream.read(bytes);
          String            jsonStr     = new String(bytes);
          jResponse         array[]     = gson.fromJson(jsonStr,
              jResponse[].class);
          if (array != null) {
            System.out.println("[RestManager] Respuesta:" + array);
            for (jResponse dato : array)
              list.add(dato);
          }
        }
      }
      connection.connect();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<jPlan> getPlans()
  {
    List<jPlan> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI uri = UriBuilder.fromUri(routeServices).build();
    // Builder builderLogin = client.target(uri).path("plans").request();
    // Evaluo so es numero o texto primero
    try {
      // final Invocation invocation = builderLogin.buildGet();

      WebTarget webTarget = client.target(uri).path("plans");
      Response  response  = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
          .get();

      // final Response response = invocation.invoke();
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        jPlan[] array = gson.fromJson(strJson, jPlan[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (jPlan eq : array)
            list.add(eq);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }

  public List<jPerson> getPersons()
  {
    List<jPerson> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    Builder builderLogin = client.target(uri).path("persons").request();
    // Evaluo so es numero o texto primero
    try {
      final Invocation invocation = builderLogin.buildGet();
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        jPerson[] array = gson.fromJson(strJson, jPerson[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (jPerson eq : array)
            list.add(eq);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }

  public long save(final IKeylable tuple)
  {
    long         success = -1;
    ClientConfig config  = new ClientConfig();
    Client       client  = ClientBuilder.newClient(config);

    URI     uri         = UriBuilder.fromUri(routeServices).build();
    Builder builderSave = client.target(uri).path(getService(tuple, true))
        .request();
    String  jsonString  = gson.toJson(tuple);
    try {
      final Invocation invocation = builderSave
          .buildPost(Entity.json(jsonString));
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.CREATED.getStatusCode()) {
        String respuesta = response.readEntity(String.class);
        success = Long.parseLong(respuesta);
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return success;
  }

  public boolean delete(final IKeylable tuple)
  {
    boolean      success = false;
    ClientConfig config  = new ClientConfig();
    Client       client  = ClientBuilder.newClient(config);

    URI     uri         = UriBuilder.fromUri(routeServices).build();
    Builder builderSave = client.target(uri)
        .path(getService(tuple, false) + "/" + tuple.getId()).request();

    try {
      final Invocation invocation = builderSave.buildDelete();
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.ACCEPTED.getStatusCode()) {
        success = true;
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return success;
  }

  private String getService(final IKeylable tuple, boolean isSave)
  {
    if (tuple instanceof jPlan)
      return isSave ? "plansave" : "plandelete";
    if (tuple instanceof jPerson)
      return isSave ? "personsave" : "persondelete";
    if (tuple instanceof jReport)
      return isSave ? "reportssave" : "reportdelete";
    if (tuple instanceof jComponent)
      return isSave ? "componentsave" : "componentdelete";
    if (tuple instanceof jChecklist)
      return isSave ? "checklistsave" : "checklistdelete";
    return "";
  }

  public List<String> getPending()
  {
    List<String> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    Builder builderLogin = client.target(uri).path("pending").request();
    // Evaluo so es numero o texto primero
    try {
      final Invocation invocation = builderLogin.buildGet();
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        String[] array = gson.fromJson(strJson, String[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (String str : array)
            list.add(str);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }

  public List<jComponent> getComponents()
  {
    List<jComponent> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    Builder builderLogin = client.target(uri).path("components").request();
    // Evaluo so es numero o texto primero
    try {
      final Invocation invocation = builderLogin.buildGet();
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        jComponent[] array = gson.fromJson(strJson, jComponent[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (jComponent dato : array)
            list.add(dato);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }

  public List<jChecklist> getChecks()
  {
    List<jChecklist> list = new ArrayList<>();

    ClientConfig config = new ClientConfig();
    Client       client = ClientBuilder.newClient(config);

    URI     uri          = UriBuilder.fromUri(routeServices).build();
    Builder builderLogin = client.target(uri).path("checks").request();
    // Evaluo so es numero o texto primero
    try {
      final Invocation invocation = builderLogin.buildGet();
      final Response   response   = invocation.invoke();
      if (response.getStatus() == Status.OK.getStatusCode()) {
        String strJson = response.readEntity(String.class);

        jChecklist[] array = gson.fromJson(strJson, jChecklist[].class);
        if (array != null) {
          System.out.println("[RestManager] Respuesta:" + array);
          for (jChecklist dato : array)
            list.add(dato);
        }
      } else {
        String razonFalla = response.getStatusInfo().getReasonPhrase();
        System.out.println("[RestManager] Texto de Falla:" + razonFalla);
      }
      // response.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    client.close();
    return list;
  }
}
