package cl.jonnattan.mtto.util;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import cl.jonnattan.mtto.factory.Factories;

public class PnlBDConnect extends JPanel
{
  private static final long serialVersionUID = 1L;
  private JTextField        txtUser          = null; ;
  private JPasswordField    txtPass          = null;
  private JTextField        txtHost          = null;
  private JTextField        txtPort          = null;
  
  public PnlBDConnect()
  {
    super();
    setLayout(new GridLayout(0, 1, 0, 0));
    setOpaque(true);
    JPanel pnlName = new JPanel();
    add(pnlName);
    pnlName.setLayout(new BorderLayout(0, 0));
    
    JLabel lblName = new JLabel("Usuario      ");
    lblName.setFont(CONSTConst.font);
    pnlName.add(lblName, BorderLayout.WEST);
    
    txtUser = new JTextField(20);
    txtUser.setText("postgres");
    txtUser.setFont(CONSTConst.font);
    pnlName.add(txtUser);
    
    JPanel pnlPass = new JPanel();
    add(pnlPass);
    pnlPass.setLayout(new BorderLayout(0, 0));
    
    JLabel lblPass = new JLabel("Contrase\u00F1a   ");
    lblPass.setFont(CONSTConst.font);
    pnlPass.add(lblPass, BorderLayout.WEST);
    
    txtPass = new JPasswordField();
    txtPass.setFont(CONSTConst.font);
    pnlPass.add(txtPass);
    
    JPanel pnlHost = new JPanel();
    add(pnlHost);
    pnlHost.setLayout(new BorderLayout(0, 0));
    
    JLabel lblHost = new JLabel("Direcci\u00F3n IP ");
    lblHost.setFont(CONSTConst.font);
    pnlHost.add(lblHost, BorderLayout.WEST);
    
    txtHost = new JTextField(15);
    txtHost.setFont(CONSTConst.font);
    txtHost.setText("localhost");
    pnlHost.add(txtHost);
    txtHost.setColumns(10);
    
    JPanel pnlPort = new JPanel();
    add(pnlPort);
    pnlPort.setLayout(new BorderLayout(0, 0));
    
    JLabel lblPort = new JLabel("Puerto       ");
    lblPort.setFont(CONSTConst.font);
    pnlPort.add(lblPort, BorderLayout.WEST);
    
    txtPort = new JTextField(7);
    txtPort.setText("5432");
    txtPort.setFont(CONSTConst.font);
    pnlPort.add(txtPort);
    txtPort.setColumns(10);
    configureLocation();
  }
  
  private void configureLocation()
  {
    Point point = Factories.getParameters().getInitialPosition();
    Dimension dim = Factories.getParameters().getInitialDimension();
    int x = point.x + (dim.width / 2 - getSize().width / 2);
    int y = point.y + (dim.height / 2 - getSize().height / 2);
    setLocation(x, y);
  }
  
  public String getUser()
  {
    return txtUser.getText();
  }
  
  public String getPort()
  {
    return txtPort.getText();
  }
  
  public String getHost()
  {
    return txtHost.getText();
  }
  
  public String getPass()
  {
    char[] password = txtPass.getPassword();
    return new String(password);
  }
}
