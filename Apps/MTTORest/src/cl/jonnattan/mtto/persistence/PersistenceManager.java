package cl.jonnattan.mtto.persistence;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import cl.jonnattan.base.middleware.Listener;
import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mantenimiento.datajson.abs.IKeylable;
import cl.jonnattan.mtto.factory.Factories;
import cl.jonnattan.mtto.interfaces.IMessage;
import cl.jonnattan.mtto.message.MSGFromBdDeleted;
import cl.jonnattan.mtto.message.MSGFromBdQuerry;
import cl.jonnattan.mtto.message.MSGFromBdSaved;
import cl.jonnattan.mtto.message.MSGStart;
import cl.jonnattan.mtto.message.MSGToBdDelete;
import cl.jonnattan.mtto.message.MSGToBdQuerry;
import cl.jonnattan.mtto.message.MSGToBdQuerry.EQuerryAction;
import cl.jonnattan.mtto.message.MSGToBdSave;
import cl.jonnattan.mtto.message.MSGUserValid;
import cl.jonnattan.mtto.rest.RestServices;
import cl.jonnattan.mtto.util.TimerEq;
import cl.jonnattan.mtto.util.TimerEq.EqAdvisor;

/**
 * Maneja la persistencia del chat y los sms
 * 
 * @author Jonnattan Griffiths
 * @since 06/08/2019
 * @version 1.0 Copyright(c) - 2019
 */
public class PersistenceManager implements Listener, Runnable, EqAdvisor
{
  protected static Logger                     logger = Logger.getGlobal();
  private Thread                              thread = null;
  private final LinkedBlockingQueue<IMessage> queue;
  private final RestServices                  services;

  public PersistenceManager()
  {
    super();
    queue = new LinkedBlockingQueue<IMessage>();
    services = new RestServices();
  }

  public void start()
  {
    Factories.getDispatcher().subscribe(this);
    if (thread == null) {
      thread = new Thread(this);
      thread.setName("Thead Persistencia");
    }
    thread.start();

  }

  public void stop()
  {
    if (thread != null) {
      thread.interrupt();
      thread = null;
      Factories.getDispatcher().unsubscribe(this);
    }
  }

  @Override
  public void handle(Object source, Object obj)
  {
    if (obj instanceof MSGToBdSave || obj instanceof MSGToBdDelete
        || obj instanceof MSGToBdQuerry) {
      IMessage message = (IMessage) obj;
      if (message.getContent() != null) {
        if (!queue.add(message)) {
          logger.severe("No se pudo insertar en la cola de persistencia");
        }
      }
    } else if (obj instanceof MSGUserValid) {
      MSGUserValid message = (MSGUserValid) obj;
      processInitSystem(message);
    }
  }

  /**
   * Procesa el inicio de sistema recolectando y enviando a la app grafica todos
   * los datos que actualmente estna en la BD
   */
  private void processInitSystem(final MSGUserValid msg)
  {
    if (msg.getContent() != null && msg.getContent() instanceof String
        && msg.getSource() != null && msg.getPass() instanceof String) {

      String user = (String) msg.getContent();
      String pass = msg.getPass();
      logger.info("Inicio Conexion con Servidor USER " + user + " Pass" + pass);

      jPerson persona = services.loginProcess(user, pass);
      // Esto es en respuesta al LOGIN
      MSGUserValid message = new MSGUserValid();
      message.setSource(this);
      message.setContent(persona);
      message.setPass(null);
      Factories.getDispatcher().post(message);

      if (persona != null) 
      {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        MSGStart other = new MSGStart();
        other.setSource(this);
        other.setContent(services.getEquipos());
        Factories.getDispatcher().post(other);
        
        MSGStart msgPlanes = new MSGStart();
        msgPlanes.setSource(this);
        msgPlanes.setContent(services.getPlans() );
        Factories.getDispatcher().post(msgPlanes);
        
        
        // Se inicia el timer de consultas
        long timeOut_Seg = Factories.getParameters().getTimerQuestion();
        TimerEq timer = new TimerEq(timeOut_Seg * 1000L);
        timer.addListener(this);
        
      }

    }
  }

  @Override
  public void run()
  {
    while (true) {
      try {
        IMessage message = queue.take();
        if (message instanceof MSGToBdSave)
          processSaveMessage((IKeylable) message.getContent());
        else if (message instanceof MSGToBdDelete)
          processMessageDelete((IKeylable) message.getContent());
        else if (message instanceof MSGToBdQuerry)
          processMessageQuerry((MSGToBdQuerry) message);

      } catch (InterruptedException e) {
        break;
      }
    }
    logger.info("Hilo de Persistencia terminado...");
  }

  /** Preguntas de cosas Directamente a la BD */
  private void processMessageQuerry(final MSGToBdQuerry message)
  {
    if (message.getContent() != null) {
      switch (message.geteType()) {
      case GETONE: {
        if (message.getIdentifier() != null) {
          if (message.getContent() == jEquipment.class) {
            jFullEquipment eq = services.findEquipment(message.getIdentifier());
            if (eq != null) {
              MSGFromBdQuerry msg = new MSGFromBdQuerry();
              msg.setSource(this);
              msg.setContent(eq);
              Factories.getDispatcher().post(msg);
            }
          } else if (message.getContent() == jReport.class) {
            List<jResponse> respuestas = services
                .findResponseToMaintance(message.getIdentifier());
            if (respuestas != null) {
              MSGFromBdQuerry msg = new MSGFromBdQuerry();
              msg.setSource(this);
              msg.setContent(respuestas);
              Factories.getDispatcher().post(msg);
            }
          }
        }
      }
        break;
      case GETLIST: {
        if (message.getContent() == jPlan.class) {
          List<jPlan> planes = services.getPlans();
          if (planes != null) {
            MSGFromBdQuerry msg = new MSGFromBdQuerry();
            msg.setSource(this);
            msg.setContent(planes);
            Factories.getDispatcher().post(msg);
          }
        } else if (message.getContent() == jPerson.class) {
          List<jPerson> list = services.getPersons();
          if (list != null) {
            MSGFromBdQuerry msg = new MSGFromBdQuerry();
            msg.setSource(this);
            msg.setContent(list);
            Factories.getDispatcher().post(msg);
          }
        }
        else if (message.getContent() == jComponent.class) {
          List<jComponent> list = services.getComponents();
          if (list != null) {
            MSGFromBdQuerry msg = new MSGFromBdQuerry();
            msg.setSource(this);
            msg.setContent(list);
            Factories.getDispatcher().post(msg);
          }
        }
        else if (message.getContent() == jChecklist.class) {
          List<jChecklist> list = services.getChecks();
          if (list != null) {
            MSGFromBdQuerry msg = new MSGFromBdQuerry();
            msg.setSource(this);
            msg.setContent(list);
            Factories.getDispatcher().post(msg);
          }
        }
      }
        break;
      case ALRMTTO: {
        List<String> list = services.getPending();
        if (list != null) {
          MSGFromBdQuerry msg = new MSGFromBdQuerry();
          msg.setSource(this);
          msg.setContent(list);
          Factories.getDispatcher().post(msg);
        }
      }
        break;
      default:
        logger.severe("Problemas!!!!!!!!!!!!!!!!");
        break;
      }

    }

  }

  /**
   * Guarda o alcualiza una tupla
   * 
   * @param message
   */
  private void processSaveMessage(final IKeylable tuple)
  {
    if (tuple != null) {

      long id = services.save(tuple);
      if( id != -1L )
      {
        if(tuple instanceof jReport )
          ((jReport)tuple).setId(id);
        if(tuple instanceof jPerson )
          ((jPerson)tuple).setId(id);
        if(tuple instanceof jChecklist)
          ((jChecklist)tuple).setId(id);
        if(tuple instanceof jComponent )
          ((jComponent)tuple).setId(id);
        
        MSGFromBdSaved msgSaved = new MSGFromBdSaved();
        msgSaved.setContent(tuple);
        msgSaved.setSource(this);
        Factories.getDispatcher().post(msgSaved);
      }
      
    }
  }

  /**
   * Elimina una tupla
   * 
   * @param message
   */
  private void processMessageDelete(final IKeylable tuple)
  {
    if (tuple != null) {
      if( services.delete(tuple) )
      {
        MSGFromBdDeleted msgDeleted = new MSGFromBdDeleted();
        msgDeleted.setContent(tuple);
        msgDeleted.setSource(this);
        Factories.getDispatcher().post(msgDeleted);
      }      
    }
  }

  @Override
  public void infoEq()
  {
    MSGToBdQuerry message = new MSGToBdQuerry();
    message.seteType(EQuerryAction.ALRMTTO);
    message.setSource(this);
    message.setContent("Solicitud");
    if (!queue.add(message)) 
      logger.severe("No se pudo insertar en la cola de persistencia");
  }

}
